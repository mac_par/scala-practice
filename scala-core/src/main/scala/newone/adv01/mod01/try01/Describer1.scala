package newone.adv01.mod01.try01

import newone.adv01.mod01.model.{Apple, Fruit}

object Describer1 {
  def describeAnApple(fn: Apple => Description): Description = fn(Apple("Fuji"))

  def describeAFruit(fn: Fruit => Description): Description = fn(Apple("papierowka"))
}
