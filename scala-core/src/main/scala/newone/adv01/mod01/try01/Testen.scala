package newone.adv01.mod01.try01

import newone.adv01.mod01.model.Fruit

object Testen extends App {
  import Describer1._

  private val function: Fruit => Taste = fruit => Taste(s"Such a tasty ${fruit.name}")
  val description1 = describeAnApple(function)
  println(description1)

  println(describeAFruit(function))
}
