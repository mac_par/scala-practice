package newone.adv01.mod01.try04

case class Distance(meter: Int) {
  def larger(item: Distance): Distance = if (this > item) this else item

  def smaller(item: Distance): Distance = if (this < item) this else item

  def >(item: Distance): Boolean = this.meter > item.meter

  def <(item: Distance): Boolean = this.meter < item.meter
}

object Distance {
  def sortDistances(list: List[Distance]): List[Distance] = list match {
    case Nil => Nil
    case head :: rest => insertSort(head, sortDistances(rest))
  }

  private def insertSort(item: Distance, rest: List[Distance]): List[Distance] = rest match {
    case Nil => List(item)
    case head :: _ if item < head => item :: rest
    case head :: tail => head :: insertSort(item, tail)
  }
}
