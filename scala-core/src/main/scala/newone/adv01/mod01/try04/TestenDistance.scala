package newone.adv01.mod01.try04

object TestenDistance extends App {
  val list = (1 to 15).reverse.map(Distance.apply).toList
  println(list)

  println(Distance.sortDistances(list))
}
