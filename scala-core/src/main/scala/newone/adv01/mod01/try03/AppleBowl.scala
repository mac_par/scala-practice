package newone.adv01.mod01.try03

import newone.adv01.mod01.model.Food

class AppleBowl[F <: Food] private(val food: F) extends FoodBowl2 {
  override type FOOD = F
}

object AppleBowl {
  def apply[F <: Food](food: F): AppleBowl[F] = new AppleBowl(food)
}
