package newone.adv01.mod01.try03

import newone.adv01.mod01.model.Food

abstract class FoodBowl2 {
  type FOOD <: Food
  val food: FOOD
  def eat: String = s"Yummy ${food.name}"

}
