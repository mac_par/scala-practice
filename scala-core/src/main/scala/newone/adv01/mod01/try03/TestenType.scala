package newone.adv01.mod01.try03

import newone.adv01.mod01.model.{Apple, Banana}

object TestenType extends App {
  val appleBowl = AppleBowl(Apple("dupa"))
  val hello: String = "hi"

  def say(word: hello.type):Unit = println(word)

  say(hello)

  val bananaBowl = AppleBowl(Banana("banana"))
  val bananaType: bananaBowl.FOOD = bananaBowl.food
}
