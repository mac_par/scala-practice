package newone.adv01.mod01.try03


object To extends App {
  val or: String = "or"
  val to: String = "to"
  def be(word: or.type): To.type = this
  def not(word: to.type): To.type = this
  def be: String = "This is the question!"

  println(To be or not to be)
}
