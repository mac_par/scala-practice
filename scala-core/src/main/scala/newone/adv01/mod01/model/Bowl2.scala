package newone.adv01.mod01.model

case class Bowl2[+T <: Food](content: T) {
  override def toString: String = s"Bowl of ${this.content.name}"

  def contents: T = this.content
}
