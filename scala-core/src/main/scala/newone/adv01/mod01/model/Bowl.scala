package newone.adv01.mod01.model

case class Bowl(food: Food) {
  override def toString: String = s"Bowl of ${food.name}"

  def content: Food = food
}
