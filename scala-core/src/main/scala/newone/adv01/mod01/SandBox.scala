package newone.adv01.mod01

import newone.adv01.mod01.model.{Apple, Bowl, Bowl2, Food, Fruit, Muesli}

object SandBox extends App {
  def consumeIt[T <: Food](bowl: Bowl2[T]) = s"o my god: ${bowl.content.name}"
  val lidlen = Muesli("Lidlen")
  val mac = Apple("mac")

  val bowl1 = Bowl(lidlen)
  val bowl2 = Bowl(mac)
  val cont1: Food = bowl1.content
  val cont2: Food = bowl2.content
  val bowl1a = Bowl2(lidlen)
  val bowl2a = Bowl2[Apple](mac)
  val cont1a: Muesli = bowl1a.content
  val cont2b = bowl2a.content

  println(consumeIt(bowl1a))
  println(consumeIt(bowl2a))
}
