package newone.adv01.mod01.try05

object Colors2 extends Enumeration {
  val RED = Value(1, "Red")
  val GREEN = Value(2, "Green")
  val BLUE = Value(3, "Blue")
  val ALPHA = Value(4, "Alpha")
}
