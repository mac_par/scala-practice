package newone.adv01.mod01.try05

import newone.adv01.mod01.model.{Apple, Banana, Food, Muesli}
import language.existentials

object Fuitens extends App {
  def fruiten01(listen: List[T forSome {type T <: Food}]): Unit = listen.map(_.name).foreach(println)
  def fruiten02(listen: List[_ <: Food]): Unit = listen.map(_.name).foreach(println)


  val listen = List(Apple("dfs"), Banana("dfsdf"), Muesli("sdfsd"))
  fruiten01(listen)
  println
  fruiten02(listen)
}
