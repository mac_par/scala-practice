package newone.adv01.mod01.try05

class Haters extends LogAble with LazyLogging {

}

object Haters extends App {
  val haters = new Haters()
  haters.inform("Marco", "Polo")
}
