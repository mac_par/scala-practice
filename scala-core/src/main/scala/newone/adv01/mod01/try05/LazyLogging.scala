package newone.adv01.mod01.try05

trait LazyLogging {
  def logIt(s1: String, s2: String): Unit = println(s"$s1 hates $s2")
}
