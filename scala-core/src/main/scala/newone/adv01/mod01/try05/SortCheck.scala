package newone.adv01.mod01.try05

object SortCheck extends App {
  def isSorted[T](items: Array[T], fn: (T, T) => Boolean): Boolean = {
    def loop(idx: Int): Boolean = {
      if (items == null) {
        false
      } else if (idx >= items.length - 1) {
        true
      } else if (fn(items(idx - 1), items(idx))) {
        loop(idx + 1)
      } else {
        false
      }
    }

    loop(1)
  }

  val numbers = (1 to 15).toArray
  val nbrFn: (Int, Int) => Boolean = (x, y) => x < y
  val inNumber = List(1, 2, 3, 4, 5, 6, 4, 8, 9, 10).toArray
  val chars = ('a' to 'z').toArray
  val chars2 = List('z', 'b', 'c').toArray
  val charFn: (Char, Char) => Boolean = (x, y) => x < y

  println(isSorted(numbers, nbrFn))
  println(isSorted(inNumber, nbrFn))
  println(isSorted(chars, charFn))
  println(isSorted(chars2, charFn))
}
