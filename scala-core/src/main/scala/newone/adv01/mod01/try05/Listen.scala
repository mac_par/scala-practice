package newone.adv01.mod01.try05

case class Listen(value: Int) {
  def size: Int = value
}
