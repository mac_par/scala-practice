package newone.adv01.mod01.try05

import newone.adv01.mod01.model.Muesli

class MeinMuesli(private val name: String) extends BowlTrait {
  override type FOOD = Muesli
  override val item: Muesli = Muesli(name)
}
