package newone.adv01.mod01.try05

import newone.adv01.mod01.model.Food

trait BowlTrait {
  type FOOD <: Food
  val item: FOOD
  def info: String = s"Yummy bowl of ${item.name}"
}
