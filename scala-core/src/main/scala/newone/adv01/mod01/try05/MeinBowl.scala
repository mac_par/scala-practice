package newone.adv01.mod01.try05

import newone.adv01.mod01.model.Apple

class MeinBowl(private val name: String) extends BowlTrait{
  override type FOOD = Apple
  override val item: FOOD = Apple(name)
}
