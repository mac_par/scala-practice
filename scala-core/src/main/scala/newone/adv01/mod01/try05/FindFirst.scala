package newone.adv01.mod01.try05

import scala.annotation.tailrec

object FindFirst extends App {
  def findFirst[T](array: Array[T], fn: T => Boolean): Int = {
    @tailrec
    def loop(idx: Int): Int = {
      if (idx >= array.length) -1 else if (fn(array(idx))) idx else loop(idx + 1)
    }

    loop(0)
  }

  val array1 = Array('C', 'z', 'a', 'r', 'n', 'e', 'k')
  val key1 = 'e'
  val fn1: Char => Boolean = x => x == key1
  val key2 = 'x'
  val fn2: Char => Boolean = x => x == key2
  val array2 = Array(5, 3, 6, 2, 8, 5, 6, 1)
  val key3 = 4
  val fn3: Int => Boolean = x => x == key3
  val key4 = 2
  val fn4: Int => Boolean = x => x == key4

  println(findFirst(array1, fn1))
  println(findFirst(array1, fn2))
  println(findFirst(array2, fn3))
  println(findFirst(array2, fn4))
}
