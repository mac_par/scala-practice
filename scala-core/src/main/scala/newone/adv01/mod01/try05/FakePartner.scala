package newone.adv01.mod01.try05

class FakePartner(name: String) { outhern =>
  class Faker(name: String) {
    def desc: String = s"${outhern.name} loves $name"
  }
}

object FakePartner extends App {
  val partneren = new FakePartner("Marco")
  val faker = new partneren.Faker("dupa")
  println(faker.desc )
}
