package newone.adv01.mod01.try05

object CompoundIT {
  def compose[A, B, C](f: B => C, g: A=>B): A => C = a => f(g(a))
}
