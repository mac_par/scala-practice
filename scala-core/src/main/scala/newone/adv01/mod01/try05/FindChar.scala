package newone.adv01.mod01.try05

object FindChar extends App {
  def findIt(array: Array[String], key: String): Int = {
    def loo(idx: Int): Int = {
      if (idx >= array.length)
        return -1
      else if (array(idx) == key)
        return idx
      else
        return loo(idx + 1)
    }
    return loo(0)
  }

  val word = Array("g", "i","m", "b", "o")
  val key = "b"
  val key2 = "z"

  println(findIt(word.toArray, key))
  println(findIt(word.toArray, key2))
}
