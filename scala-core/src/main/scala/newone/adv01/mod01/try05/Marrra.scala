package newone.adv01.mod01.try05

import newone.adv01.mod01.model.{Fruit, Muesli}

object Marrra extends App {
  def show(bowl: BowlTrait {type FOOD <: Fruit}): Unit = println(s"Fruten: ${bowl.info}")
  def show2(bowl: BowlTrait {type FOOD <: Muesli}): Unit = println(s"Muslen: ${bowl.info}")

  def display(instance: Dupa[_]): Unit = println(instance)
  val meinMuesli = new MeinMuesli("muelsin")
  val meinApple = new MeinBowl("applen")

  show2(meinMuesli)
  show(meinApple)

  val idiota = Idiota("ja")
  val fucker = Fucker(34)

  display(idiota)
  display(fucker)
}
