package newone.adv01.mod01.try05
import language.reflectiveCalls
import language.existentials

object SizeListen extends App {
//  def maxSizen(listen: List[_ <: {def size: Int}]): Int = listen.map(_.size).max

//  val listen = List(Set(5), List(1,2,3), Listen(5))
//  println(maxSizen(listen))
  val string = "hello"
  val obj: AnyRef = string

  println(obj.isInstanceOf[{def length:Int}])
  println(obj.asInstanceOf[{def length:Int}].length)
  println(obj.asInstanceOf[{def charAt(x:Int):Char}].charAt(1))

}
