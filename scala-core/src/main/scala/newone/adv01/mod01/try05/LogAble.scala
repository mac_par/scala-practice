package newone.adv01.mod01.try05

trait LogAble { this: LazyLogging =>
  def inform(s1: String, s2: String): Unit = this.logIt(s1, s2)
}
