package newone.adv01.mod01.try02

import newone.adv01.mod01.model.Food

case class MixFoodBowl[+T <: Food](item1: T, item2: T) {
  override def toString: String = s"Bowl of ${item1.name} & ${item2.name}"
}
