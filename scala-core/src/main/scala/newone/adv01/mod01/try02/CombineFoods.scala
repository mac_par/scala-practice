package newone.adv01.mod01.try02

import newone.adv01.mod01.model.Food

trait CombineFoods[+T <: Food] {
  val item: T
  def combineWith[S >: T <: Food](another: S): MixFoodBowl[S] = MixFoodBowl(item, another)
}
