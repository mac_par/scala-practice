package newone.adv01.mod01.try02

case class CombineWithInt(item: Int) extends CombineWith[Int] {
  override def combineWith(another: Int): Int = item + another
}
