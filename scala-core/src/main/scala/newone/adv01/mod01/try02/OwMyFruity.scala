package newone.adv01.mod01.try02

import newone.adv01.mod01.model.Food

case class OwMyFruity[+T <: Food](item: T) extends CombineFoods[T] {
}
