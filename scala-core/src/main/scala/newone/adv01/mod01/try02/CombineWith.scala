package newone.adv01.mod01.try02

trait CombineWith[T] {
  val item: T
  def combineWith(another: T): T
}
