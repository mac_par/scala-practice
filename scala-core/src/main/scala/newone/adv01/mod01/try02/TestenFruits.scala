package newone.adv01.mod01.try02

import newone.adv01.mod01.model.{Apple, Muesli}

object TestenFruits extends App {
  val apple = Apple("Mniam")
  val muslit = Muesli("Jah")

  val combino = OwMyFruity(apple)
  println(combino.combineWith(muslit))
}
