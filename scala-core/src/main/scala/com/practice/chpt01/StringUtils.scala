package com.practice.chpt01

object StringUtils extends App {
  implicit class Incrementer(str: String) {
    def increment: String = str map (c => (c + 1).toChar)
    def decrement: String = str map (c => (c - 1).toChar)
    def hide: String = str.replaceAll(".","*")
    def plusOne: Int = str.length + 1
    def asBoolean: Boolean = str match {
      case "0" | "zero" | "" | " " => false
      case _ => true
    }
  }

  println("dupa".decrement)
  println("dupa".hide)
  println("dupa".plusOne)
  println("dupa".asBoolean)
}
