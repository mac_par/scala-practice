package com.practice.chpt01

import scala.collection.immutable
import scala.util.control.Breaks
import scala.util.control.Breaks.{break, breakable}

object ConvertionTries extends App {
  val i = 127
  println(i.isValidByte)

  val s = "Davae"
  val p = s: Object
  println(p.getClass)

  val seq: immutable.Seq[Int] = 1 to 10
  val pi = 3.141592653589793
  println(f"$pi%03.3f")

  for {i <- 1 to 10} {
    breakable {
      if (i > 4) break
    }
    println(i)
  }

  breakable {
    for {i <- 1 to 10} {
      if (i > 4) break
      println(i)
    }
  }

  val outern = new Breaks
  val inner = new Breaks
  for {x <- 0 to 10} {
    outern.breakable {
      for {y <- 0 to 10} {
        inner.breakable{
          if (x == y) inner.break
          if (x > y) outern.break
        }
        println(f"x: $x - y: $y")
      }
    }
  }
}
