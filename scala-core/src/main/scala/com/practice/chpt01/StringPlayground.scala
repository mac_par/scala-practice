package com.practice.chpt01

import CosTam._
object StringPlayground extends App {
  println("dupa".toInteger)
  println("123".toInteger)
}

object CosTam {
  implicit class StringConverter(str: String) {
    def toInteger: Option[Int] = try {
      Some(str.toInt)
    } catch {
      case e: NumberFormatException => None
    }
  }
}
