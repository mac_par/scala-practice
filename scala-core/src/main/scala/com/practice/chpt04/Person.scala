package com.practice.chpt04

class Person(val firstName: String, val lastName: String) {
  println("constructor begins")
  private val HOME = System.getProperty("user.home")
  var age = 0

  override def toString: String = s"$firstName $lastName is $age old"

  def printHome: Unit = println(s"$HOME")

  def printFullName: Unit = println(this)

  printHome
  printFullName
  println("still in constructor")
}
