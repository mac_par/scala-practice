package com.practice.chpt04

class Brain private {
  override def toString: String = "This is the brain"

  println("Brain was created")
}

object Brain {
  private lazy val brain = new Brain

  def getInstance = brain
}
