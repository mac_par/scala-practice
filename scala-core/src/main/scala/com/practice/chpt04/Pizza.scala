package com.practice.chpt04

import com.practice.chpt04.Pizza.{DEFAULT_CRUST_SIZE, DEFAULT_CRUST_TYPE}

//primary constructor
class Pizza (var crustSize: Int, var crustType: String) {
  var field: String = _
  def this(crustSize: Int) {
    this(crustSize, DEFAULT_CRUST_TYPE)
  }

  def this(crustType: String) {
    this(DEFAULT_CRUST_SIZE, crustType)
  }

  def this() {
    this(DEFAULT_CRUST_SIZE, DEFAULT_CRUST_TYPE)
  }

  override def toString: String = s"A $crustSize inch pizza with a $crustType crust"
}

object Pizza {
  val DEFAULT_CRUST_SIZE = 12
  val DEFAULT_CRUST_TYPE = "thin"
}
