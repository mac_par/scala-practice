package kolejne.rewizja.advanced.testing

import com.google.common.cache.{CacheBuilder, CacheLoader}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object FakeWeatherLookup {

  import scala.concurrent.ExecutionContext.Implicits.global

  private val cache = CacheBuilder.newBuilder()
    .build[String, Future[Double]] {
      new CacheLoader[String, Future[Double]] {
        override def load(k: String): Future[Double] = Future(WeatherService.weatherLookup(k))
      }
    }
  def apply(key: String): Future[Double] = cache.get(key)

  def main(args: Array[String]): Unit = {
    val f1 = FakeWeatherLookup("SFO")
    val f2 = FakeWeatherLookup("IPX")
    val f3 = FakeWeatherLookup("ABC")
    Await.ready(f1, 2 second)
    Await.ready(f2, 2 second)
    Await.ready(f3, 2 second)
    println(f1)
    println(f2)
    println(f3)
    val f4 = FakeWeatherLookup("SFO")
    println(f4)
  }
}
