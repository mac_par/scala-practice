package kolejne.rewizja.advanced.testing

object WeatherService {
  def weatherLookup(code: String): Double = {
    Thread.sleep(1000)
    code.toList.map(_.toInt).sum / 10.0
  }
}
