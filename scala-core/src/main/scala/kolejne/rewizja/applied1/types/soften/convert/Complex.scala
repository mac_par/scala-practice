package kolejne.rewizja.applied1.types.soften.convert

import scala.language.implicitConversions

case class Complex(real: Double, imaginary: Double = 0.0) {
  override def toString: String = s"$real ${sign} ${imaginary.abs}"

  private def sign = if (real == real.abs) "+" else '-'

  def +(other: Complex) = Complex(real + other.real, imaginary + other.imaginary)
}

object Complex {
  implicit def intToComplex(value: Int): Complex = Complex(value)
}
