package kolejne.rewizja.applied1.types.soften.convert

object ComplexTest extends App {
  val c1 = Complex(5, -6)
  val c2 = Complex(-3, -6)
  println(c1 + c2)
  println(c1 + 10)
  println(10 + c2)
}
