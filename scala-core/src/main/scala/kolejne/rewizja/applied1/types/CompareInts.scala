package kolejne.rewizja.applied1.types

object CompareInts {
  def main(args: Array[String]): Unit = {
    import IntComparator._
    val list = List(99, 12, 0, 4, 60, 32, 13, 2)
    println(CompareSorter.sortCompare(list))
  }

  implicit object IntComparator extends Compare[Int] {
    override def isSmaller(t1: Int, t2: Int): Boolean = t1 < t2

    override def isLarger(t1: Int, t2: Int): Boolean = isSmaller(t2, t1)
  }
}
