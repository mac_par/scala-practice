package kolejne.rewizja.applied1.types.soften.rules

import kolejne.rewizja.applied1.generiken.food.Fruit
import kolejne.rewizja.applied1.generiken.food.fruit.Apple
import kolejne.rewizja.applied1.types.soften.{Eats, Vegan}

object VeganRules {
  implicit object veganEatsFruit extends Eats[Vegan, Fruit]

  implicit object veganEatsApple extends Eats[Vegan, Apple]
}
