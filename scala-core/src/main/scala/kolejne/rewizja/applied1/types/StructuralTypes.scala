package kolejne.rewizja.applied1.types

import language.reflectiveCalls

object StructuralTypes {

  def main(args: Array[String]): Unit = {
    val string = "sth very important"
    if (string.isInstanceOf[{def length: Int}]) {
      println(string.length)
    }
    println(Person("Sally").LifePartner("Marco").describe())

  }
}
