package kolejne.rewizja.applied1.types.soften.convert

import scala.language.implicitConversions

object TestTimesInt extends App {
  implicit class TimesInt(val i: Int) extends AnyVal {
    def times(fn: => Unit): Unit = {
      var x = 0
      while (x < i) {
        fn
        x += 1
      }
    }
  }

  3 times{
    println("Hi there!")
  }
}
