package kolejne.rewizja.applied1.types.soften

import kolejne.rewizja.applied1.generiken.food.Food
import kolejne.rewizja.applied1.generiken.food.cereal.Muesli
import kolejne.rewizja.applied1.generiken.food.fruit.Apple
import kolejne.rewizja.applied1.generiken.food.meat.Meat
import kolejne.rewizja.applied1.types.soften.rules.{VeganRules, PaleoRules, VegetarianRules}

object EatItTest extends App {
  def feedTo[EATER <: Eater, FOOD <: Food](food: FOOD, eater: EATER)(implicit ev: Eats[EATER, FOOD]): String = ev.feed(food, eater)

  val apple = Apple("Apple")
  val alpen = Muesli("Alpen")
  val beef = Meat("beef")

  val alice = Vegan("Alice")
  val bob = Vegetarian("Bob")
  val charlie = Paleo("Charlie")

  import VeganRules._
  import VegetarianRules._
  import PaleoRules._
  println(feedTo(apple, alice))
  println(feedTo(alpen, bob))
  println(feedTo(beef, charlie))
}
