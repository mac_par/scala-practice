package kolejne.rewizja.applied1.types

import kolejne.rewizja.applied1.types.Jsoner.toJson

import scala.language.implicitConversions

object DoubleJsonTest extends App {

  import JsonImplc._
  println(toJson(2.0))
  println(toJson(List(2.0, 3.0, 4.0)))
  println(toJson("2.0"))
  println(toJson(List("2.0", "tak", "nie")))
  println(toJson(List(true, false)))
  val strToValue = Map( "one" -> 2, "tak" -> 3, "nie" -> 0, "cos" -> 1)
  println(toJson(strToValue))
}

object JsonImplc {
  implicit val doubleToJson: JSONWrite[Double] = (item: Double) => item.toString
  implicit val stringToJson: JSONWrite[String] = (item: String) => s""""$item""""
  implicit val intToJson: JSONWrite[Int] = (item: Int) => item.toString
  implicit val booleanToJson: JSONWrite[Boolean] = item => s""""${item.toString}""""
  implicit val anyValToJson: JSONWrite[AnyVal] = {
    case i: Boolean => booleanToJson.toJsonString(i)
    case i: Double => doubleToJson.toJsonString(i)
    case i: Int => intToJson.toJsonString(i)
  }
}
