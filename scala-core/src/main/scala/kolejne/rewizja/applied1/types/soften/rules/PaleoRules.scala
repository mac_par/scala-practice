package kolejne.rewizja.applied1.types.soften.rules

import kolejne.rewizja.applied1.generiken.food.Fruit
import kolejne.rewizja.applied1.generiken.food.fruit.Apple
import kolejne.rewizja.applied1.generiken.food.meat.Meat
import kolejne.rewizja.applied1.types.soften.{Eats, Paleo}

object PaleoRules {
  implicit object paleoEatsFruit extends Eats[Paleo, Fruit]

  implicit object paleoEatsApple extends Eats[Paleo, Apple]

  implicit object paleoEatsMeat extends Eats[Paleo, Meat]
}
