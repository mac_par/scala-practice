package kolejne.rewizja.applied1.types

object CompareSorter {
  def insertCompare[T: Compare](item: T, list: List[T]) /*(implicit comparator: Compare[T])*/ : List[T] = {
    val cmp = implicitly[Compare[T]]
    list match {
      case Nil => List(item)
      case head :: tail if cmp.isSmaller(item, head) => item :: insertCompare(head, tail)
      case head :: tail => head :: insertCompare(item, tail)
    }
  }

  def sortCompare[T: Compare](list: List[T]): List[T] = list match {
    case Nil => Nil
    case head :: tail => insertCompare(head, sortCompare(tail))
  }
}
