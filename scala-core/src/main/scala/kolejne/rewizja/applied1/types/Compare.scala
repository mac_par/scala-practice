package kolejne.rewizja.applied1.types

trait Compare[T] {
  def isSmaller(t1: T, t2: T): Boolean

  def isLarger(t1: T, t2: T): Boolean
}
