package kolejne.rewizja.applied1.types

case class Person(name: String) { outer =>
  case class LifePartner(name: String) {
    def describe(): String = s"${outer.name} loves ${this.name}"
  }
}
