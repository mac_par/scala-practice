package kolejne.rewizja.applied1.types

trait JSONWrite[T] {
  def toJsonString(item: T): String
}
