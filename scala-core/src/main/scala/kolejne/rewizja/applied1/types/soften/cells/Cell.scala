package kolejne.rewizja.applied1.types.soften.cells

case class Cell[T](item: T) {
  def *[U: Numeric](other: Cell[U])(implicit ev: T =:= U): Cell[U] = {
    val numClass = implicitly[Numeric[U]]
    Cell(numClass.times(item, other.item))
  }
}
