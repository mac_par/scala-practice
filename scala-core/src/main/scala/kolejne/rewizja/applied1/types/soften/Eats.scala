package kolejne.rewizja.applied1.types.soften

import kolejne.rewizja.applied1.generiken.food.Food
import scala.annotation.implicitNotFound

@implicitNotFound(msg = "Illegal Feeding: No Eatsrule from ${EATER} to ${FOOD}")
trait Eats[EATER <: Eater, FOOD <: Food] {
  def feed(food: FOOD, eater: EATER)(implicit ev: Eats[EATER, FOOD]): String = s"${eater.name} eats ${food.name}"
}
