package kolejne.rewizja.applied1.types.soften.rules

import kolejne.rewizja.applied1.generiken.food.Fruit
import kolejne.rewizja.applied1.generiken.food.cereal.{Cereal, Muesli}
import kolejne.rewizja.applied1.generiken.food.fruit.Apple
import kolejne.rewizja.applied1.types.soften.{Eats, Vegetarian}

object VegetarianRules {
  implicit object vegetarianEatsFruit extends Eats[Vegetarian, Fruit]

  implicit object vegetarianEatsApple extends Eats[Vegetarian, Apple]

  implicit object vegetarianEatsCereal extends Eats[Vegetarian, Cereal]

  implicit object vegetarianEatsMuesli extends Eats[Vegetarian, Muesli]
}
