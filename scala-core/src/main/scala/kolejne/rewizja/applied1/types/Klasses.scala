package kolejne.rewizja.applied1.types

import scala.reflect._
import scala.reflect.runtime.universe._

object Klasses extends App {
  val kalss = classTag[String]
  println(kalss)
  println(getTag("tak").runtimeClass)
  println(isA[Int](1))
  println(isA[Int](1.0))
  println(isA[Int]("1.0"))
  println(classTag[Map[String,Int]])
  println(typeTag[Map[String,Int]])


  def getTag[T: ClassTag](t: T) = classTag[T]

  def isA[T: ClassTag](x: Any) = x match {
    case _: T => true
    case _ => false
  }
}
