package kolejne.rewizja.applied1.types

object Jsoner {
  def toJson[T: JSONWrite](item: T)(implicit jsonizer: JSONWrite[T]): String = jsonizer.toJsonString(item)

  def toJson[T: JSONWrite](list: List[T])(implicit jsonizer: JSONWrite[T]): String = list.map(jsonizer.toJsonString).mkString("[", ",", "]")

  def toJson[S: JSONWrite](mapitem: Map[String, S])(implicit valueJsonizer: JSONWrite[S]): String = {
    if (mapitem.isEmpty) {
      return "{}"
    }
    mapitem.map { entry => s""""${entry._1}" : ${valueJsonizer.toJsonString(entry._2)}""" }
      .mkString("{\n", ",\n", "\n}")
  }
}
