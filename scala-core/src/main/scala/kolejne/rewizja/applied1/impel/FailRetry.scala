package kolejne.rewizja.applied1.impel

object FailRetry {
  def main(args: Array[String]): Unit = {
    implicit val params = RetryParams(5)
    val res = RetryObject.retryCall {
      println("hi")
      checkIt()
    }
    println(res)
  }

  var x = 0

  def checkIt(): Int = {
    x += 1
    require(x > 4, "x not big enough")
    x
  }
}
