package kolejne.rewizja.applied1.impel

import scala.util.control.NonFatal

object RetryObject {
  def retryCall[A](fn: => A, currentTry: Int = 0)(implicit params: RetryParams): A = {
    try fn
    catch {
      case NonFatal(_) if currentTry < params.times => retryCall(fn, currentTry + 1)(params)
    }
  }
}
