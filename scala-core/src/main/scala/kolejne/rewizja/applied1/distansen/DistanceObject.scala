package kolejne.rewizja.applied1.distansen

object DistanceObject {
  def insertDistance(distance: Distance, list: List[Distance]): List[Distance] = list match {
    case head::tail if distance < head => distance::insertDistance(head, tail)
    case head::tail => head::insertDistance(distance, tail)
    case Nil => List(distance)
  }
  def sortDistances(list: List[Distance]): List[Distance] = list match {
    case head::tail => insertDistance(head, sortDistances(tail))
    case Nil => Nil
  }

  def main(args: Array[String]): Unit = {
    val l1 = List(Distance(10), Distance(1), Distance(99), Distance(12), Distance(9), Distance(0), Distance(2))
    println(sortDistances(l1))
  }
}
