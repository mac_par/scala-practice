package kolejne.rewizja.applied1.distansen

import kolejne.rewizja.applied1.sorten.Compare

case class Distance(meters: Int) extends Compare[Distance] {
  def larger(other: Distance): Distance = if (other.meters > meters) other else this

  def smaller(other: Distance): Distance = if (other.meters < meters) other else this

  def >(other: Distance): Boolean = meters > other.meters

  def <(other: Distance): Boolean = meters < other.meters
}
