package kolejne.rewizja.applied1.generiken.food.sink

import kolejne.rewizja.applied1.generiken.food.Fruit

object FruitSink extends Sink[Fruit] {
  override def sink(item: Fruit): String = s"Consuming fruit: ${item.name}"
}
