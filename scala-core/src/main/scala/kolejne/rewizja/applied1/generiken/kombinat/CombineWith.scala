package kolejne.rewizja.applied1.generiken.kombinat

trait CombineWith[T] {
  val item: T
  def combineWith(another: T): T
}
