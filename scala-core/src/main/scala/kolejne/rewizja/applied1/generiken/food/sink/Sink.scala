package kolejne.rewizja.applied1.generiken.food.sink

trait Sink[-F] {
  def sink(item: F): String
}
