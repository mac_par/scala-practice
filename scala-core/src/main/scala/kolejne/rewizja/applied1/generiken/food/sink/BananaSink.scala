package kolejne.rewizja.applied1.generiken.food.sink

import kolejne.rewizja.applied1.generiken.food.fruit.Banana

object BananaSink extends Sink[Banana] {
  override def sink(item: Banana): String = s"Peeling and eating ${item.name}"
}
