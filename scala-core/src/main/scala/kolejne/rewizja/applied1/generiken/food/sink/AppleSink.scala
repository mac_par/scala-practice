package kolejne.rewizja.applied1.generiken.food.sink

import kolejne.rewizja.applied1.generiken.food.fruit.Apple

object AppleSink extends Sink[Apple]{
  override def sink(item: Apple): String = s"Coring and eating ${item.name}"
}
