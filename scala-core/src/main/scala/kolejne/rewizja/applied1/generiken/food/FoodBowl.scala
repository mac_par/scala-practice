package kolejne.rewizja.applied1.generiken.food

case class FoodBowl[+F <: Food](food: F) {
  override def toString: String = s"A bowl of ${food.name}"
  def mixWith[M >: F <: Food](other: M): MixedBowl[M] = MixedBowl(food, other)
}
