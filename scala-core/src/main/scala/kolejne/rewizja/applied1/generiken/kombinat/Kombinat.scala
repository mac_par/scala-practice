package kolejne.rewizja.applied1.generiken.kombinat

import kolejne.rewizja.applied1.generiken.food.FoodBowl
import kolejne.rewizja.applied1.generiken.food.cereal.Muesli
import kolejne.rewizja.applied1.generiken.food.fruit.{Apple, Banana}

object Kombinat {
  def main(args: Array[String]): Unit = {
    val combinat = CombineWithInt(10)
    println(combinat.combineWith(5))

    val apple = Apple("Fuji")
    val banana = Banana("Chiquita")
    val alpen = Muesli("Alpen")
    println(FoodBowl(apple).mixWith(banana))
    println(FoodBowl(alpen).mixWith(banana))
  }
}
