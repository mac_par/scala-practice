package kolejne.rewizja.applied1.generiken.food

case class Bowl2[+F <: Food](content: F) {
  override def toString: String = s"A bowl of yummy ${content.name}"

  def contents = content
}
