package kolejne.rewizja.applied1.generiken.kombinat

case class CombineWithInt(item: Int) extends CombineWith[Int] {
  override def combineWith(another: Int): Int = item + another
}
