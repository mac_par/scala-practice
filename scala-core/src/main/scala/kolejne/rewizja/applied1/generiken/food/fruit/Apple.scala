package kolejne.rewizja.applied1.generiken.food.fruit

import kolejne.rewizja.applied1.generiken.food.Fruit

case class Apple(override val name: String) extends Fruit(name) {

}
