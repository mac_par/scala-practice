package kolejne.rewizja.applied1.generiken.food

import kolejne.rewizja.applied1.generiken.food.cereal.Muesli
import kolejne.rewizja.applied1.generiken.food.fruit.{Apple, Banana}
import kolejne.rewizja.applied1.generiken.food.sink.{AppleSink, BananaSink, FruitSink}

object Eaten {
  def main(args: Array[String]): Unit = {
    val fuji = Apple("fuji")
    val karmelen = Banana("karmelen")
    val alpen = Muesli("Alpen")

    val fujiBowl: Bowl2[Apple] = Bowl2(fuji)
    val alpenBowl: Bowl2[Muesli] = Bowl2(alpen)
    println(fujiBowl)
    println(alpenBowl)
    serveFruitBowl(fujiBowl)
    serveFoodBowl(alpenBowl)
    sendFruit(fuji)
    sendFruit(karmelen)
    println(AppleSink.sink(fuji))
    println(BananaSink.sink(karmelen))
    println(FruitSink.sink(karmelen))
    println(FruitSink.sink(fuji))
  }


  def serveFruitBowl(bowl: Bowl2[Fruit]): Unit = {
    println(s"mmmm.... ${bowl.content.name}s were so yummy fruits")
  }

  def serveFoodBowl(bowl: Bowl2[Food]): Unit = {
    println(s"mmmm.... ${bowl.content.name}s were so yummy food")
  }

  def sendFruit[F >: Fruit](item: F) = {
    println(s"Eating ${item}")
  }
}
