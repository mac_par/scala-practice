package kolejne.rewizja.applied1.generiken.feature

import kolejne.rewizja.applied1.generiken.food.Fruit
import kolejne.rewizja.applied1.generiken.food.fruit.Apple

object Describe {
  def main(args: Array[String]): Unit = {
  val juicyFruit: Fruit => Description = fruit => Taste(s"This ${fruit.name} is nice and juicy")
    println(describeAnApple(juicyFruit).describe)
  }

  def describeAnApple(fn: Apple => Description) = fn(Apple("fuji"))
}
