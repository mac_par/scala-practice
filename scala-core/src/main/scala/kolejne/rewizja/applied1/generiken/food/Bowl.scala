package kolejne.rewizja.applied1.generiken.food

case class Bowl(food: Food) {
  override def toString: String = s"A bowl of yummy ${food.name}"
  def contents = food
}
