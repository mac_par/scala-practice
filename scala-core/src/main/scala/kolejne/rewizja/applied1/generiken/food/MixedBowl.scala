package kolejne.rewizja.applied1.generiken.food

case class MixedBowl[+F <: Food](food1: F, food2: F) {
  override def toString: String = s"${food1.name} mixed with ${food2.name}"
}
