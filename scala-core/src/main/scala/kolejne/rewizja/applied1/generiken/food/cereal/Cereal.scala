package kolejne.rewizja.applied1.generiken.food.cereal

import kolejne.rewizja.applied1.generiken.food.Food

abstract class Cereal(override val name: String) extends Food
