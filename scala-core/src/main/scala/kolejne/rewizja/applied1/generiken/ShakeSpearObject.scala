package kolejne.rewizja.applied1.generiken

object ShakeSpearObject {
  val or = "or"
  val to = "to"

  object To {
    def be(word: or.type): this.type = this

    def not(word: to.type): this.type = this

    def be: String = "this is the question"
  }

  def main(args: Array[String]): Unit = {
    println(To be or not to be)
  }
}
