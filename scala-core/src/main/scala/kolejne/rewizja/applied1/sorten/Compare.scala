package kolejne.rewizja.applied1.sorten

trait Compare[T] {
  def >(other: T): Boolean

  def <(other: T): Boolean
}
