package kolejne.rewizja.applied1.sorten

import kolejne.rewizja.applied1.distansen.Distance

object GenSorten {
  def insertGen[T <: Compare[T]](item: T, list: List[T]): List[T] = list match {
    case head :: tail if item < head => item :: insertGen(head, tail)
    case head :: tail => head :: insertGen(item, tail)
    case Nil => List(item)
  }

  def sortGen[T <: Compare[T]](list: List[T]): List[T] = list match {
    case head :: tail => insertGen(head, sortGen(tail))
    case Nil => Nil
  }

  def main(args: Array[String]): Unit = {
    val l1 = List(Distance(10), Distance(1), Distance(99), Distance(12), Distance(9), Distance(0), Distance(2))
    println(sortGen(l1))
  }
}
