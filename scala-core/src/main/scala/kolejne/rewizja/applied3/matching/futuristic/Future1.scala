package kolejne.rewizja.applied3.matching.futuristic
import scala.concurrent._
object Future1 {
  def main(args: Array[String]): Unit = {
    import ExecutionContext.Implicits.global
    val f1 = Future{
      Thread.sleep(1000)
      10
    }

    val fa: Future[Any] = Future(10)
    val fi = fa.collect{
      case i: Int => i
    }
    val fii = fi.filter(_>11)

    val f2 = f1.map(_ *10)
    println(s"f1: ${f1.value} - ${f1.isCompleted}")
    println(s"f2: ${f2.value} - ${f2.isCompleted}")
    Thread.sleep(2500)
    println(s"f1: ${f1.value} - ${f1.isCompleted}")
    println(s"f2: ${f2.value} - ${f2.isCompleted}")
    println(fii.fallbackTo(Future.successful(0)))
  }
}
