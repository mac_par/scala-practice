package kolejne.rewizja.applied3.matching.jazda

case class Person(name: String, var weightInPounds: Double) {
  import Person.poundToKgRatio
  def weightInKilo: Double = weightInPounds * poundToKgRatio

  def weightInKilo_=(newWeight: Double): Unit = {
    weightInPounds = newWeight / poundToKgRatio
  }

}

object Person {
  private val poundToKgRatio = 0.453592

  def main(args: Array[String]): Unit = {
    val person = Person("Marco", 70)
    println(person.weightInKilo) //31.75
    person.weightInKilo = 34.194
    println(person.weightInPounds) //75
  }
}
