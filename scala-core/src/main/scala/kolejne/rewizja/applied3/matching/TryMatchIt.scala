package kolejne.rewizja.applied3.matching

import kolejne.rewizja.applied3.matching.model.Person

import scala.util.{Failure, Success, Try}

object TryMatchIt {
  val MaxLimit = 10
  val minLimit = 1

  def main(args: Array[String]): Unit = {
    println(opposite2("sane"))
    println(opposite2("dupa"))

    println(isALimit(10))
    println(isALimit(3))
    println(isALimit(1))

    println(matchesVector(Vector(1, 2, 3, 4)))
    println(matchesVector((2 to 5).toVector))
    println(matchesVector(Vector.empty[Int]))

    val brad = Person("Brad", None, None)
    val harry = Person("Harry", Some("0-700****"), None)
    val sally = Person("Sally", Some("1234"), None)
    println(whoIsThat(brad))
    println(whoIsThat(sally))
    println(whoIsThat(harry))
    val Person(name, hasPhone, hasAddress) = harry
    println(s"$name, $hasPhone, $hasAddress")

    for {Person(name, Some(phoneNumber), _) <- List(harry, sally, brad) } println(s"$name can be colled $phoneNumber")
  }

  def opposite2(s: String): String = s match {
    case "hot" => "cold"
    case "full" => "empty"
    case "cool" => "square"
    case "happy" => "sad"
    case worden@("sane" | "edible" | "secure") => s"in$worden"
    case anything => s"not $anything"
  }

  def isALimit(x: Int): Boolean = x match {
    case MaxLimit => true
    case `minLimit` => true
    //    case minLimit => true
    case _ => false
  }

  def matchesVector(vec: Vector[Int]): String = vec match {
    case Vector(1, 2, rest@_*) => s" 1,2 with $rest"
    case Vector(a, b, rest@_*) => s"$a, $b with $rest"
    case any => s" any vector: $any"
  }

  def matchTry(t: Try[_]): String = t match {
    case Success(x) => s"Success -> $x"
    case Failure(e) => s"Failure -> $e"
  }

  def whoIsThat(p: Person): String = p match {
    case Person("Harry", phone, _) => s"It is Harry, you can call it under: $phone"
    case sally @ Person("Sally", phone, address) => s"It is Sally with $phone, $address"
    case _ => "Have no idea"
  }
}
