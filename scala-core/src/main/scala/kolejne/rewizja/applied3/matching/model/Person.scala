package kolejne.rewizja.applied3.matching.model

case class Person(name: String, phone: Option[String], address: Option[Address])
