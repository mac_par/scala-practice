package kolejne.rewizja.applied3.matching.jazda

trait HeightAndWeight {
  var height: Double
  var weight: Double
}
