package kolejne.rewizja.applied3.matching.model

case class Address(street: String, city: String, postCode: Option[String])
