package kolejne.rewizja.applied3.matching.model

import scala.util.Try

object Coords {
  def unapply(value: String): Option[(Double, Double)] = Try {
    val values = value.split(',').map(_.trim.toDouble)
    (values(0), values(1))
  }.toOption

  def main(args: Array[String]): Unit = {
    val cos = "-121.432, 34.23"
    val cos2 = "-121.432, costam"

    println(getCoords(cos))
    println(getCoords(cos2))
  }

  def getCoords(inpt: String): Option[(Double, Double)] = inpt match {
    case Coords(x, y) => Some(x, y)
    case _ => None
  }
}
