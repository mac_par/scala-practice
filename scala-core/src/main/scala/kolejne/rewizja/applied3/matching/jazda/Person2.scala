package kolejne.rewizja.applied3.matching.jazda

class Person2(nm: String, wt: Double) extends HeightAndWeight {

  import Person2.poundToKgRatio

  def name: String = nm

  private[this] var wtLbs = wt
  private[this] var hgt: Double = _

  def weightInPounds: Double = wtLbs

  def weightInPounds_=(newWeight: Double): Unit = {
    wtLbs = newWeight
  }

  def weightInKilo: Double = wtLbs * poundToKgRatio

  def weightInKilo_=(newWeight: Double): Unit = {
    wtLbs = newWeight / poundToKgRatio
  }

  def height: Double = hgt

  def height_=(newHeight: Double): Unit = {
    require(newHeight > 0, "You fucker, height must be > 0")
    hgt = newHeight
  }

  def weight: Double = weightInKilo

  def weight_=(newWeight: Double): Unit = {
    require(newWeight > 0, "You fucker, the weight must be  > 0")
    weightInKilo = newWeight
  }
}

object Person2 {
  private val poundToKgRatio = 0.453592

  def main(args: Array[String]): Unit = {
    val person = new Person2("Marco", 70)
    person.name
    println(person.weightInKilo) //31.75
    person.weight = 34.194
    println(person.weightInPounds) //75
    person.weightInPounds = 76
    println(person.weight) //31.75
    println(person.height) //0
    person.height = 0
  }
}
