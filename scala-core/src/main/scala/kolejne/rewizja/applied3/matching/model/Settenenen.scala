package kolejne.rewizja.applied3.matching.model

object Settenenen {
  def main(args: Array[String]): Unit = {
    val set = Set('c', 'o', 's', 't', 'a', 'm')
    println(set('a'))
    println(set('b'))
    println(set + 'd')
    println(set)
    println("cos tam dalej".count(set))

    val vector = Vector.range(0, 21)
    val vecView = vector.view
    val vecTransformation = vecView.map(calc)
    println(vecTransformation(3))
    println(vecTransformation(4))
    println(vecTransformation(5))

  }

  def calc(x: Int): Int = x * x
}
