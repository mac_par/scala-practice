package nowe.tutorial.adv.part01.task01

abstract class Car(val make: String, val model: String, val year: Int) {
  val isVintage: Boolean
}
