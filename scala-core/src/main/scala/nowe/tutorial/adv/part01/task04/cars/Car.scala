package nowe.tutorial.adv.part01.task04.cars

class Car(val model:String, override val name: String, override val age: Int) extends Vehicle(name, age) {
  override def toString: String = s"${model} - ${super.toString}"
}


object Car extends App {
  val car = new Car("Hara", "Jara", 1966)
  println(car)
}