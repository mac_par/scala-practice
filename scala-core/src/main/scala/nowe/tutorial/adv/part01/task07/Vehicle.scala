package nowe.tutorial.adv.part01.task07

abstract class Vehicle(val name: String, val description: Vector[String]) {
//  def name:String
//  def description: Vector[String]
  override def toString: String = s"Vehicle($name)"

  def fullDescription: String = (name +: description).mkString("|")
}
