package nowe.tutorial.adv.part01.task07

abstract class VehicleStorage(val name: String, val vehicles: Vector[Vehicle] = Vector.empty[Vehicle]) {
  override def toString: String = s"VehicleStorage: $name with $vehicles"
  def count: Int = vehicles.size
}
