package nowe.tutorial.adv.part01.task08

abstract class Car(override val name: String, val carries: String) extends RollingStock(name) {
  def pulled(): String = s"$name pulls $carries"
}

object Car extends App {
  val par1 = new PassengerCar
  val par2 = new PassengerCar
  val par3 = new PassengerCar

  val cargoCar1 = new CargoCar
  val cargoCar2 = new CargoCar

  val steam = new SteamEngine
  val desiel = new DieselEngine

  steam.add(par1)
  steam.add(par2)

  desiel.add(par3)
  desiel.add(cargoCar1)
  desiel.add(cargoCar2)

  println(steam)
  println(steam.pull())
  println(desiel)
  println(desiel.pull())
}
