package nowe.tutorial.adv.part01.task07

case class Car(override val name: String, override val description: Vector[String] = Vector.empty) extends Vehicle(name, description)

object Car extends App {
  val car = Car("cos", Vector("dupa", "jajca","koszyki"))
  println(car.fullDescription)
  println(car)
}
