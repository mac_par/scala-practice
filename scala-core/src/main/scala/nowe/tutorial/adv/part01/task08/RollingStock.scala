package nowe.tutorial.adv.part01.task08

abstract class RollingStock(val name: String) {
  override def toString: String = name
}
