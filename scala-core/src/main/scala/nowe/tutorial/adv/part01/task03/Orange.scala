package nowe.tutorial.adv.part01.task03

class Orange(val name: String) extends Fruit {
}

object Orange extends App {
  val orange = new Orange("JAffa")
}
