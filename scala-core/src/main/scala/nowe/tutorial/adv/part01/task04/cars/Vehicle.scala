package nowe.tutorial.adv.part01.task04.cars

abstract class Vehicle(val name: String, val age: Int) {
  override def toString: String = s"${name} ${age}"
}
