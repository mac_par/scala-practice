package nowe.tutorial.adv.part01

import java.util.Objects

class Person(var name: String, var age: Int) {
  def isAdult: Boolean = age >= 21

  override def hashCode(): Int = age.hashCode()

  override def equals(obj: Any): Boolean = obj match {
    case p: Person => p.name == name && p.age == age
    case _ => false
  }
}

object Person extends App {
  val mark = new Person("mark", 18)
  val mark2 = new Person("mark", 18)
  val tom = new Person("tom", 48)

  println(mark.eq(mark))
  println(mark.eq(mark2))
  println(mark.eq(tom))
  println(mark == mark)
  println(mark == mark2)
  println(mark == tom)
}
