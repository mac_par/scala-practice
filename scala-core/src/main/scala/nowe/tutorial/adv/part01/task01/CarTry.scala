package nowe.tutorial.adv.part01.task01

object CarTry extends App {
  val caren = new Car("cos", "tam", 1998) {
    override val isVintage: Boolean = year < 2000
  }

  println(caren.isVintage)
}
