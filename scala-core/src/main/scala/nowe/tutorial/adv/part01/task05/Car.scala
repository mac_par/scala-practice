package nowe.tutorial.adv.part01.task05

case class Car(marka: String, name: String, year: Int) {
  lazy val isShortLived:Boolean = year > 1987
}

object Car extends App {
  val ford = Car("marka", "name", 1966)
  println(ford)
  ford.year
  println(ford == Car("marka", "name", 1965))
  println(ford == Car("marka", "name", 1966))
}
