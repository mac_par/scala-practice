package nowe.tutorial.adv.part01.task07

case class ParkingLot(override val name: String, override val vehicles: Vector[Vehicle]) extends VehicleStorage(name,vehicles) {
  override def toString: String = s"$name with ${describe}"
  private[this] def describe: String = vehicles.mkString("|")
}

object ParkingLot extends App {
  val car1 = Car("cos", Vector("cos1", "cos2"))
  val car2 = Car("tak", Vector("tak1", "tak2"))
  val parkingLot = ParkingLot("podrzędny parking", Vector(car1, car2))
  println(parkingLot)
  println(parkingLot.count)
}
