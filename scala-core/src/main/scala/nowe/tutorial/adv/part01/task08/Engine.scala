package nowe.tutorial.adv.part01.task08

import scala.collection.mutable.ListBuffer
abstract class Engine(override val name: String, val maxCars: Int, val cars: ListBuffer[Car] = ListBuffer.empty[Car])
  extends RollingStock(name) {
  def pull(): String = s"$name pulls $definition"
  def add(car: Car): Unit = if (cars.size == maxCars) throw new IllegalStateException("too many") else cars += car
  private [this] def definition: String = cars.mkString("|")
}
