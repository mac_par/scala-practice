package nowe.tutorial.adv.part01.task02

object Demo extends App {
  val d = new Demo
  println("Demo was created")
  println("a")
  d.a
  println("b")
  d.b
  println("c")
  d.c
  println("a")
  d.a
  println("c")
  d.c
}
class Demo {
  val a: Int = {
    println("val a")
    10
  }

  def b: Int = {
  println("def b")
    20
  }

  lazy val c: Int = {
    println("lazy val c")
    30
  }
}
