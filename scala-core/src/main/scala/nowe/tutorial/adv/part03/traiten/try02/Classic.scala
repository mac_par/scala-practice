package nowe.tutorial.adv.part03.traiten.try02

trait Classic extends Car {
  def vintage: Int

  override def describe: String = s"vintage ${vintage} ${super.describe}"
}
