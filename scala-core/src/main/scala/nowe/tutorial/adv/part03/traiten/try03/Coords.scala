package nowe.tutorial.adv.part03.traiten.try03

class Coords(override val coordsX: Double, override val coordsY: Double) extends CoordsT {

}

object Coords extends App {
  val coords = new Coords(3.0, 4.0)
  println(coords.distToOrigin)

  val coord = new {
    val coordsX: Double = 3.0
    val coordsY: Double = 4.0
  } with CoordsT
  println(coord.distToOrigin)
}
