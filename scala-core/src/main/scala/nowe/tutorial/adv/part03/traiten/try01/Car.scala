package nowe.tutorial.adv.part03.traiten.try01

trait Car {
  def color: String
  def describe: String = s"${color} car"
}
