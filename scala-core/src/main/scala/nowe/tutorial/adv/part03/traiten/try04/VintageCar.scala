package nowe.tutorial.adv.part03.traiten.try04

class VintageCar(val make: String, val model: String, val year: Int) extends CompareAge[VintageCar] {
  override def older(item: VintageCar): VintageCar = if (this.year < item.year) this else item

  override def toString: String = model
}

object VintageCar extends App {
  def getOlder[T <: CompareAge[T]](item1: T, item2: T): T = {
    item1 older item2
  }
  println(getOlder(new VintageCar("Ford", "Mustang", 1965),new VintageCar("Ford", "Model T", 1922)))
}
