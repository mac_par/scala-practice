package nowe.tutorial.adv.part03.traiten.try02

trait Convertable extends Car {
  def poweredTop: Boolean

  override def describe: String = {
    val top = if (poweredTop) "powered convertible"
    else "convertible"
    s"${top} ${super.describe}"
  }
}
