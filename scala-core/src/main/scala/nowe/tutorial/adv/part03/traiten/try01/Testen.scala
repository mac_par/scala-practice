package nowe.tutorial.adv.part03.traiten.try01

object Testen extends App {
  val car = new Car {
    override def color: String = "gowniany"
  }

  println(car.describe)

  val actual = new ActualCar("khaki", "gowno")
  println(actual.describe)
  actual("dupa").andThen(println)
}
