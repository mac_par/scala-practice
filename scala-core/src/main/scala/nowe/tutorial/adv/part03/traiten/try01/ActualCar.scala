package nowe.tutorial.adv.part03.traiten.try01

class ActualCar(val color: String, val name: String) extends Car with Function1[String,String] {
  override def describe: String = s"${super.describe} called ${name}"

  override def apply(v1: String): String = s"${v1} ${color}"
}
