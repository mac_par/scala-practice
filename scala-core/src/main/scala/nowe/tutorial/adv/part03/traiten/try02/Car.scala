package nowe.tutorial.adv.part03.traiten.try02

abstract class Car {
  def color: String
  def describe: String = s"${color}"

  override def toString: String = s"${describe} car"
}
