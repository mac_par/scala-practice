package nowe.tutorial.adv.part03.traiten.try03

trait CoordsT {
  val coordsX: Double
  val coordsY: Double

  override def toString: String = s"Coords(${coordsX}, ${coordsY})"
  lazy val distToOrigin: Double = math.sqrt(math.pow(coordsX, 2) + math.pow(coordsY, 2))
}
