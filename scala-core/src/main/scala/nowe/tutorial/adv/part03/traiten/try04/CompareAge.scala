package nowe.tutorial.adv.part03.traiten.try04

trait CompareAge[T] {
  def older(item: T): T
}
