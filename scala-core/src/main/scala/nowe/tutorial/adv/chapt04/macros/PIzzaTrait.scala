package nowe.tutorial.adv.chapt04.macros

trait PizzaTrait {
  var numToppings: Int
  var size: Int = 14
  val maxNumToppings = 10
}

class Pizza extends PizzaTrait {
  override val maxNumToppings: Int = 11
  override var numToppings: Int = 0
  size = 16
}