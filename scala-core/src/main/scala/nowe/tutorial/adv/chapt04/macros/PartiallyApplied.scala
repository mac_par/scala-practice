package nowe.tutorial.adv.chapt04.macros

object PartiallyApplied extends App {
  def wrap(prefix: String, html: String, suffix: String): String = prefix + html + suffix

  val sum = (a: Int, b: Int, c: Int) => a + b + c
  val divPart = wrap("<div>", _, "</div>")
  val f = sum(1, 2, _)
  val divide = (x: Int, i: Int) => i match {
    case value if value != 0 => x / i
    case _ => "You cannot divide $x by zero! Think next time you idiot!"
  }
  val divide2: PartialFunction[Int,Int] = {
    case d:Int  if (d != 0) => 42/d
  }

  println(f(3))
  println(divPart("Hi"))
  println(divPart("Hello"))
  println(divPart("There"))

  println(divide(42, 3))
  println(divide(42, 4))
  println(divide(42, 0))
  println(List(0, 1,2) collect(divide2))
}
