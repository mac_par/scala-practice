package nowe.tutorial.adv.chapt04.macros

trait Employee

class StoreEmployee extends Employee

class CorporateEmployee extends Employee {
  def sellIt(s: String): Boolean = {
    println(s"Sold: $s"); true
  }
}

class GastronomyEmployee extends StoreEmployee

trait PizzaDeliverable {
  this: StoreEmployee =>

}

class PizzaDeliveryBoy extends StoreEmployee with PizzaDeliverable

class AnotherPizzaEmployee extends GastronomyEmployee with PizzaDeliverable

trait SztokEgzczendz {
  this: {def sellIt(s: String): Boolean} =>
  def earnedIt: Boolean = true
}


class Strange extends CorporateEmployee with SztokEgzczendz