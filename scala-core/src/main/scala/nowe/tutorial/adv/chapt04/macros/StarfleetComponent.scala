package nowe.tutorial.adv.chapt04.macros

class StarFleetComponent

trait StarFleetTrait extends StarFleetComponent

class Starship extends StarFleetComponent with StarFleetTrait

/*

Wont compile cause different base classes
class RomulusShip
class Warbird extends RomulusShip with StarFleetTrait
*/
