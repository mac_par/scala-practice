package nowe.tutorial.adv.chapt04.macros

object Tries extends App {
  val doubleIt: (Double, Double) => Double = (x, y) => x * y
  val votingAge = 18

  val canVote = (i: Int) => i >= votingAge

  def strange(f: => Unit, b: => Boolean): Unit = if (b) f

  def plusOne(i: Int): Unit = i + 1

  def executeXTimes(callback: () => Unit, times: Int): Unit = {
    if (times > 0) {
      callback()
      executeXTimes(callback, times - 1)
    }
  }

  println(doubleIt(2, 4))
  val doIt: Any => Unit = println _
  val doubleItOnceMoreTime: (Double, Double) => Double = doubleIt(_, _)
  doIt("Now!")
  println(doubleItOnceMoreTime(3, 4))

  strange(println("dupa"), true)
  println(plusOne(1))

  executeXTimes(() => println("Hi there!"), 5)
  println(canVote(15))
  println(canVote(18))
  println(canVote(21))
}
