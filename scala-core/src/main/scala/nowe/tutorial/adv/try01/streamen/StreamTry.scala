package nowe.tutorial.adv.try01.streamen

import scala.#::

object StreamTry extends App {
  val fib: Stream[BigInt] = BigInt(0) #:: BigInt(1) #:: fib.zip(fib.tail).map{
    case (x,y) => x + y
  }

  println(fib.take(15).toList)

  val factorial: Stream[BigInt] = BigInt(1) #:: factorial.zip(Stream.from(2)).map {
    case (x,y) => x * y
  }

  println(factorial.take(15).toList)
}
