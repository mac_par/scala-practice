package nowe.tutorial.adv.try01

class Person2(val name: String) extends WeightAndHeight {
  override protected[this] var ht: Double = _
  override protected[this] var wt: Double = _

  def height: Double = ht

  def height_=(newHeght: Double): Unit = ht = newHeght

  def weight: Double = wt

  def weight_=(newWeight: Double): Unit = wt = newWeight
}
