package nowe.tutorial.adv.try01

class Person(val name: String, private var _age: Int) {
  def age:Int = _age
  def age_=(newAge: Int):Unit = _age = newAge
}
