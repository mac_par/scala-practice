package nowe.tutorial.adv.try01.caching

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

object WeatherTry extends App {
  val f1 = FakeWeatherLookup("SFO")
  val f2 = FakeWeatherLookup("SFO")
  Await.result(f1, 10.seconds)
  Await.result(f2, 10.seconds)
}
