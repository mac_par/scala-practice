package nowe.tutorial.adv.try01

object Tryien01 extends App {
  val person = new Person("fred", 48)
  println(s"${person.name} - ${person.age}")
  person.age = 55;
  println(s"${person.name} - ${person.age}")

  val person2 = new Person2("Fred")
  person2.weight = 150.0
  person2.height = 123.2
  println(s"${person2.name} - H: ${person2.height}, ${person2.weight}")
}
