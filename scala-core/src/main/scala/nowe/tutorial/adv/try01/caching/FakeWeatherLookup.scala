package nowe.tutorial.adv.try01.caching

import com.google.common.cache.{CacheBuilder, CacheLoader}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


object FakeWeatherLookup {
  private val cache = CacheBuilder.newBuilder().build {
    new CacheLoader[String, Future[Double]] {
      override def load(k: String): Future[Double] = Future(fakeWeatherLookup(k))
    }
  }

  def fakeWeatherLookup(wxCode: String) = {
    Thread.sleep(1000)
    wxCode.toList.map(_.toInt).sum / 10.0
  }

  def apply(wxCode: String): Future[Double] = cache.get(wxCode)
}
