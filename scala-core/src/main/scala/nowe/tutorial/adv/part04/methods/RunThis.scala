package nowe.tutorial.adv.part04.methods

object RunThis extends App {
  echo("Hello World")
  echo(MAGIC_NUM)
  echo(Margin.TOP)
  val mm = MutablMap("name" -> "al")
  mm += ("password" -> "passwd123")
  for {(k,v) <- mm} println(s"k: $k, v: $v")
}
