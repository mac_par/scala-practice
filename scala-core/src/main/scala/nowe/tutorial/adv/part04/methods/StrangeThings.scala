package nowe.tutorial.adv.part04.methods

class StrangeThings extends Human with Mother with Father with Child {
  override def hello: String = "Stranger"

  def helloMother: String = super[Mother].hello

  def helloFather: String = super[Father].hello

  def helloChild: String = super[Child].hello
}


object StrangeThings extends App {
  val strangeThing = new StrangeThings
  println(s"Stranger says: ${strangeThing.hello}")
  println(s"Mother says: ${strangeThing.helloMother}")
  println(s"Father says: ${strangeThing.helloFather}")
  println(s"Child says: ${strangeThing.helloChild}")
}
