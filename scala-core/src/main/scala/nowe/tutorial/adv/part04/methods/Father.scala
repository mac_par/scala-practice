package nowe.tutorial.adv.part04.methods

trait Father extends Human {
  override def hello: String = "Father"
}
