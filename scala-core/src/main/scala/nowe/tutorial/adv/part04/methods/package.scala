package nowe.tutorial.adv.part04

package object methods {
  val MAGIC_NUM = 12

  def echo(a: Any): Unit = println(a)

  object Margin extends Enumeration {
    type Margin = Value
    val TOP, BOTTOM, LEFT, RIGHT = Value
  }

  type MutableMap[K, V] = scala.collection.mutable.Map[K, V]
  val MutablMap = scala.collection.mutable.Map
}
