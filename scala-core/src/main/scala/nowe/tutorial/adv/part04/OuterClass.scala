package nowe.tutorial.adv.part04

class OuterClass {
  class InnerClass {
    var x = 1
  }
}
object OuterClass extends App {
  val v1 = new OuterClass
  val v2 = new OuterClass
  val ic1 = new v1.InnerClass
  val ic2 = new v2.InnerClass

  ic1.x = 15
  ic2.x = 10

  println(s"${ic1.x}")
  println(s"${ic2.x}")
}