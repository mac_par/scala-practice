package nowe.tutorial.adv.part04

object MoreFun extends App {
  def printIt(string: String*): Unit = string.foreach(println)

  def printSequence(seq: Seq[String]): Unit = for (i <- seq) println(i)

  printIt("dupa", "cos", "tak", "tade")
  printSequence(Seq("dupa", "cos", "tak", "tade"))
  printIt(Seq("dupa", "cos", "tak", "tade"): _*)
}
