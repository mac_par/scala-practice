package nowe.tutorial.adv.part04.try03

class ShippingContainer[T] private (private val items: Seq[T]) {
  private[this] val maxCount = 10
  private def isFull: Boolean = items.length == maxCount
  override def toString: String = s"${ShippingContainer.containerColor} container"
}

object ShippingContainer {
    def apply[T](items: T*): ShippingContainer[T] = new ShippingContainer(items)
    private def containerColor: String = "green"
    def isFull(container: ShippingContainer[_]): Boolean = container.isFull
    def maxItems(container: ShippingContainer[_]): Int = container.items.length
}
