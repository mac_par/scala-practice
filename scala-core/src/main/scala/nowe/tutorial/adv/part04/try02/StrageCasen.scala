package nowe.tutorial.adv.part04.try02

object StrageCasen extends App {
  def info(per: Person): Unit = {
    import per._
    println(s"${name}")
    println(s"${surname}")
    println(s"${age}")
  }

  val person = Person("Johnny", "Cage", 45)
  info(person)
}
