package nowe.tutorial.adv.part04.methods.animals

trait Animal {
  def walk:Unit = println("Animal is walking")
}

class FourLeggedAnimal extends Animal {
  override def walk: Unit = println("Four legged animal walks")
}

class Dog extends FourLeggedAnimal /*with Animal*/ {
  override def walk: Unit = {
    super.walk
    super[FourLeggedAnimal].walk
// without it an animal wont walk, it has to be with Animal as above
    //    super[Animal].walk
  }
}

object Dog extends App {
  val dog = new Dog
  dog.walk
}