package nowe.tutorial.adv.part04.methods

trait Child extends Human {
  override def hello: String = "Child"
}
