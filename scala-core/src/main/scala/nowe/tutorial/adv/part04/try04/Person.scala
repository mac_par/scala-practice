package nowe.tutorial.adv.part04.try04

class Person(private var _name: String) {
  def name: String = _name

  def name_=(newName: String): Unit = _name = newName
}
