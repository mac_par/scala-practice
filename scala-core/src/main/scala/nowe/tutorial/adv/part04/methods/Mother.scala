package nowe.tutorial.adv.part04.methods

trait Mother extends Human {
  override def hello: String = "Mother"
}
