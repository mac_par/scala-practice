package nowe.tutorial.adv.part04

class Person {
  private var _name: String = ""
  private var _surname: String = ""
  private var _age: Int = 0
  private var _sex: String = ""

  def name: String = _name

  def name_=(name: String): this.type = {
    _name = name
    this
  }

  def surname: String = _surname

  def surname_=(surname: String): this.type = {
    _surname = surname
    this
  }

  def age: Int = _age

  def age_=(age: Int): this.type = {
    _age = age
    this
  }

  def sex: String = _sex

  def sex_=(sex: String): this.type = {
    _sex = sex
    this
  }
}

object Person extends App {
  val testPerson = new Person
  testPerson.name = "Marco"
  testPerson.surname = "Polo"

  println(s"${testPerson.name}, ${testPerson.surname}")

  val person = new Person
  person
    .name_=("Marco")
    .surname_=("Polo")
    .age_=(56)
    .sex_=("Of course")

  println(s"${person.name}, ${person.surname}, ${person.age}, ${person.sex}")
}
