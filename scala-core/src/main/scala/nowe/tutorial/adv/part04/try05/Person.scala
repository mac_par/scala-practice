package nowe.tutorial.adv.part04.try05

class Person(var name: String, var address: Address) {
  override def toString: String = if (address == null) name else s"$name $address"
}


class Employee(name:String, address: Address, var age:Int) extends Person(name, address)

case class Address(city: String, state: String)