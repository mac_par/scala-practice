package nowe.tutorial.adv.part02

object InstanceCos extends App {
  val str: String = ""
  val str2 = null

  println(str.isInstanceOf[String])
  println(str2.isInstanceOf[String])
}
