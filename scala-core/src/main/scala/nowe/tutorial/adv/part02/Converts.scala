package nowe.tutorial.adv.part02

object Converts extends App {
  implicit def dimus(i: Int) {
    def times(fn: => Unit): Unit = {
      for (_ <- 1 to i) fn
    }
  }
//  5 times println("Hello there")
}
