package nowe.tutorial.adv.part02

object WorkingWithOption extends App {
  val mapen = Map(1 -> "one", 2 -> "two", 3 -> "three")
  def fourthLetter(i: Int): Option[Char] = for {
    word <- mapen.get(i)
    letter <- word.drop(3).headOption
  } yield letter

  println(fourthLetter(1))
  println(fourthLetter(2))
  println(fourthLetter(4))
  println(fourthLetter(3))
}
