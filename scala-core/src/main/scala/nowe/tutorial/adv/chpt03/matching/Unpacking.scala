package nowe.tutorial.adv.chpt03.matching

object Unpacking extends App {
  val numberMap = Map(1 -> "One", 2 -> "Two", 3 -> "Three")

  for {(k,v) <- numberMap} {
    println(s"$k : $v")
  }

  numberMap.map {
    case (1, cos) => s"Matko of $cos"
    case (k,v) => s"[$k,$v]"
  }.foreach(println)
}
