package nowe.tutorial.adv.chpt03.listen

case class Person(name: String, age: Int)

object Person {
  implicit object PersonOrdering extends Ordering[Person] {
    override def compare(x: Person, y: Person): Int = x.age - y.age
  }
}
