package nowe.tutorial.adv.chpt03.listen

object ListenSorten extends App {
  val persons = List(Person("Harry", 35), Person("Sally", 30), Person("Rajesh", 33))
  println(persons.sortBy(_.age))
  println(persons.sortWith((p1, p2) => p1.age < p2.age))

  println(persons.sorted)
 }
