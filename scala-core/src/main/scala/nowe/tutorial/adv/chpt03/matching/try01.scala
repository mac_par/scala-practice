package nowe.tutorial.adv.chpt03.matching

object try01 extends App {
  def opposites(value: String): String = value match {
    case "dupa" => "blada"
    case "stara" => "klacz"
    case cos => s"nie znalazło: $cos"
  }

  def nextTry(value: String): String = value match {
    case "dupa" => "blada"
    case "stara" => "klacz"
    case word@("sane" | "crack" | "cono") => s"in${word}"
    case cos => s"nie znalazło: $cos"
  }

  val MaxLimit = 10
  val minLimit = 1

  def valueLimit(value: Int): Int = value match {
    case MaxLimit => 1
    case `minLimit` => 0
    case _ => -1
  }

  println(opposites("dupa"))
  println(opposites("cos"))

  println(nextTry("cos"))
  println(nextTry("sane"))
  println(nextTry("crack"))

  println(valueLimit(10))
  println(valueLimit(1))
  println(valueLimit(0))
}
