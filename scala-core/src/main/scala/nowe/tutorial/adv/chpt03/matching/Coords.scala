package nowe.tutorial.adv.chpt03.matching

import scala.util.Try

class Coords private(private val x: Double, private val y: Double)

object Coords {
  def apply(x: Double, y: Double): Coords = new Coords(x, y)

  def unapply(arg: Coords): Option[(Double, Double)] = if (arg != null) Some(arg.x -> arg.y) else None

  def unapply(arg: String): Option[(Double, Double)] = Try {
    val values = arg.split(",").map(_.trim.toDouble)
    values(0) -> values(1)
  }.toOption
}
