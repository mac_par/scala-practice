package nowe.tutorial.adv.chpt03.matching

import scala.util.{Failure, Success, Try}

object TryItORNot extends App {
  def matchIt(tryIt: Try[_]): String = tryIt match {
    case Success(value) => s"value: ${value}"
    case Failure(exception) => s"except: ${exception.getMessage}"
  }

  println(Try(4/2))
  println(Try(4/0))
}
