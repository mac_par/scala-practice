package nowe.tutorial.adv.chpt03.matching

object ListMatcher extends App {
  def listen(list: List[Int]): String = list match {
    case 1 :: 2 :: cos => s"1,2, $cos"
    case a :: b :: _ => s"$a $b and something else"
    case a :: Nil => s"only: $a"
    case Nil => "empty list"
  }

  println(listen(List(1,2,3)))
  println(listen(List(1,2)))
  println(listen(List(1,3,4)))
  println(listen(List(4)))
  println(listen(Nil))
}
