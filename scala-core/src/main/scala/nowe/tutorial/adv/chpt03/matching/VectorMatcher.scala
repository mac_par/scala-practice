package nowe.tutorial.adv.chpt03.matching

object VectorMatcher extends App {
  def maher(vec: Vector[Int]): String = vec match {
    case 1 +: 2 +: rest => s"1,2 i $rest"
    case Vector(a, b, words @ _*) => s"$a and $b and $words"
    case Vector(a) => s"only a single $a"
    case Vector() => s"empty vector"
  }

  println(maher(Vector(1,2,3)))
  println(maher(Vector(1,4,3)))
  println(maher(Vector(1,4,3, 4)))
  println(maher(Vector(1)))
  println(maher(Vector(1,3)))
  println(maher(Vector()))
}
