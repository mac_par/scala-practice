package nowe.tutorial.adv.chpt03.sealius

sealed class AccountClass

case object InvestmentAccount extends AccountClass

case object MortgageAccount extends AccountClass

object AccountClass extends App {
  def isValid(account: AccountClass): Boolean = account match {
    case MortgageAccount => true
    case InvestmentAccount => false
  }

  println(isValid(InvestmentAccount))
}
