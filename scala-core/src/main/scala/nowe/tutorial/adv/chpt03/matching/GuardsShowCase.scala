package nowe.tutorial.adv.chpt03.matching

object GuardsShowCase extends App {
  def numben(x: Int): String = x match {
    case 0 => "zer"
    case n if n > 0 && n < 100 => "smallish positive"
    case n if n > 0 => "large positive"
    case n if n < 0 && n > -100 => "smallish negative"
    case _ => "large negative"
  }

  println(numben(-99))
  println(numben(99))
  println(numben(0))
  println(numben(101))
  println(numben(-101))
}
