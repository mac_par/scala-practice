package nowe.tutorial.adv.chpt03.matching

object TestCoords extends App {
  val coords = Coords(1.2, 2.5)
  coords match {
    case Coords(val1, val2) => println(s"Coords: $val1 -> $val2")
    case _ => "dupa"
  }

  "15.4, 25.3" match {
    case Coords(val1, val2) => println(s"Coords: $val1 -> $val2")
    case _ => "dupa"
  }
}
