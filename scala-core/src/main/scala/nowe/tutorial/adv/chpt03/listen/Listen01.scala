package nowe.tutorial.adv.chpt03.listen


object Listen01 extends App {
  val listen1 = 1 :: 3:: Nil
  println(listen1)
  println(List.fill(10)(1))
  println(List.tabulate(10)(x => x))
  println(List.range(0, 10, 2))

  val listen2 = listen1 ::: List(0)
  println(listen2)

  val listen3 = (0 to 6).toList
  println(listen3(3))
  println(listen3.take(3))
  println(listen3.takeRight(3))
  println(listen3.drop(3))
  println("drop right: "+ listen3.dropRight(3))
  println("drop " + listen3.drop(30))
  println(listen3.drop(30).headOption)
  println(listen3.drop(3).headOption)
  println(listen3.drop(3).lastOption)
  println(listen3.updated(3, 200))
}
