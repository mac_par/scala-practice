package approach.second.p1.cars

class Car(
           override val name: String,
           val make: String,
           val model: String,
           override val age: Int
         ) extends Vehicle(name, age) {
  override def toString: String = s"a $make $model, named ${super.toString}"
}
