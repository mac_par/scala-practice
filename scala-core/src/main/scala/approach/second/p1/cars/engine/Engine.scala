package approach.second.p1.cars.engine

import scala.collection.mutable

abstract class Engine extends RollingStock() {
  val maxCars: Int
  val cars: mutable.ListBuffer[Car] = mutable.ListBuffer.empty[Car]

  def pull: String = cars.mkString(",")

  def add(car: Car): Unit = {
    car +: cars
  }
}
