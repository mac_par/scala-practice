package approach.second.p1.cars.engine

class PassengerCar extends Car {
  override val carries: String = "people"
  override val name: String = "Passenger car"
}
