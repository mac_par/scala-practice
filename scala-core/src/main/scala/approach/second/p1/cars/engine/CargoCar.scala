package approach.second.p1.cars.engine

class CargoCar extends Car {
  override val carries: String = "cargo"
  override val name: String = "Cargo car"
}
