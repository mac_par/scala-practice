package approach.second.p1.cars

import java.time.LocalDateTime

case class Carren(make: String, model: String, year: Int) {
  lazy val isVintage: Boolean = LocalDateTime.now().getYear - year > 20
}
