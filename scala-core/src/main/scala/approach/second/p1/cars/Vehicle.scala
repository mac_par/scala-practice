package approach.second.p1.cars

abstract class Vehicle(val name: String, val age: Int) {
  override def toString: String = s"$name, $age years old"
}
