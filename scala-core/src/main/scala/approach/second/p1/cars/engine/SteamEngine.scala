package approach.second.p1.cars.engine

class SteamEngine extends Engine {
  override val maxCars: Int = 3
  override val name: String = "Steam engine"
}
