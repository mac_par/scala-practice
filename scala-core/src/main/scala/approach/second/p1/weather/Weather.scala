package approach.second.p1.weather

case class Weather(temp: Double, precip: Double, pressure: Double)

trait WeatherService {
  def getWeather(icaoCode: String): Weather

  def operational(): Boolean
}

