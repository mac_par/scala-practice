package approach.second.p1

class Person(name: String, age: Int) {

  import Person.adultAge

  def isAdult: Boolean = age >= adultAge
}

object Person {
  val adultAge = 18

  def main(args: Array[String]): Unit = {
    val person = new Person("dsfs", 14)
  }
}
