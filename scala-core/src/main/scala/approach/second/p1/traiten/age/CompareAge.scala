package approach.second.p1.traiten.age

trait CompareAge[T] {
  def older(item: T): T
}
