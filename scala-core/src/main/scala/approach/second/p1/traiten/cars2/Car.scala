package approach.second.p1.traiten.cars2

abstract class Car {
  def color: String

  def describe: String = s"$color"

  override def toString: String = s"$describe car"
}


