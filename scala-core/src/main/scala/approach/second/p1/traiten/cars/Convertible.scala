package approach.second.p1.traiten.cars

import approach.second.p1.traiten.Car

trait Convertible extends Car {
  def poweredTop: Boolean

  override def toString: String = {
    val top = if (poweredTop) "powered convertible" else "convertible"
    s"$top ${super.describe}"
  }
}
