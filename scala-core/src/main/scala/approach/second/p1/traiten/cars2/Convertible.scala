package approach.second.p1.traiten.cars2

trait Convertible extends Car {
  override def toString: String = s"convertible ${super.describe}"
}
