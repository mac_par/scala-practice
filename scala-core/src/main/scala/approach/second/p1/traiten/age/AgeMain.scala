package approach.second.p1.traiten.age

object AgeMain {
  def main(args: Array[String]): Unit = {
    import approach.second.p1.traiten.Logging._
    info(getOlder(Person("dfsfs", 18), Person("sdgfgdf", 32)).toString)
  }

  protected def getOlder[T <: CompareAge[T]](item1: T, item2: T): T = item1 older item2
}
