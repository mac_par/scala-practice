package approach.second.p1.traiten

trait Logging {
  def error(msg: String): Unit = println(s"Error: $msg")
  def info(msg: String): Unit = println(s"Info: $msg")
}

object Logging extends Logging
