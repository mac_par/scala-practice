package approach.second.p1.traiten.cars2

trait PoweredConvertible extends Convertible {
  override def describe: String = s"powered ${super.describe}"
}
