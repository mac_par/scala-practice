package approach.second.p1.traiten

trait Car {
  def color: String

  def describe: String = s"$color car"

  override def toString: String = s"$describe  car"
}
