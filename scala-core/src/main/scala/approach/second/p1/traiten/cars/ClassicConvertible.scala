package approach.second.p1.traiten.cars

import approach.second.p1.traiten.Car

class ClassicConvertible(
                          val color: String,
                          val vintage: Int,
                          val poweredTop: Boolean
                        ) extends Car with Classic with Convertible {

}


object ClassicConvertible {
  def main(args: Array[String]): Unit = {
    val mustang = new ClassicConvertible("red", 1965, false)
    println(mustang)
  }
}