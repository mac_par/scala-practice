package approach.second.p1.traiten.cars2

trait HardtopConvertible extends Convertible {
  override def describe: String = s"hard-top ${super.describe}"
}
