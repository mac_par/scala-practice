package approach.second.p1.traiten.cars

import approach.second.p1.traiten.Car

trait Classic extends Car {
  def vintage: Int

  override def toString: String = s"vintage $vintage ${super.describe}"
}
