package approach.second.p1.traiten.cars2

trait Classic extends Car {
  def vintage: Int

  override def toString: String = s"vintage $vintage ${super.describe}"
}
