package approach.second.p1.traiten.age

case class Person(name: String, age: Int) extends CompareAge[Person] {
  override def older(item: Person): Person = if (item.age > age) item else this
}
