package com.practice.testy

import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
class TestingSuiteDemo extends AnyFunSuite with Matchers{
  val nums: List[Int] = (1 to 20).toList

  test("Filtering a list") {
    val filtered = nums.filter{_ > 15}
    assert(filtered === (16 to 20).toSeq)
  }

  test("Summing a list") {
    nums.sum should be (210)
  }

  test("Try something else") (pending)
}
