package com.practice.testy

import nowe.tutorial.adv.part04.try02.Person
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class TestingSpecDemo extends AnyFunSpec with Matchers {
  describe("sth should") {
    it("do sth") {
      pending
    }
    it("should have sth") {
      val nums = (1 to 20).toList
      val threes = nums.filter(_ % 3 == 0)

      threes should have size (6)
      threes should contain allOf(3, 6, 12, 15)
      threes should not contain (10)
      threes should be(List(3, 6, 9, 12, 15, 18))
      threes should be(sorted)
      threes should not be >(19)
      atLeast(3, threes) should be > 10
    }
    it("Harry Porter") {
      val person = Person("Harry", "Porter", 16)

      person should have(
        'name("Harry"),
        'surname("Porter"),
        'age(16)
      )
    }
    it("ekseptionen1") {
      an [IllegalArgumentException] should be thrownBy {
        require(1 == 2, "tak")
      }
      val ex = intercept[ArithmeticException] (1/0)
      ex.getMessage should be ("/ by zero")
    }
  }
}
