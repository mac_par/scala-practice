package com.practice.chpt04

import org.scalatest.funspec.AnyFunSpec

class PizzaTest extends AnyFunSpec {
  describe("testing auxiliary constructors") {
    it("trying it out") {
      val p1 = new Pizza()
      println(p1)
      val p2 = new Pizza(15, "shitty")
      println(p2)
      val p3 = new Pizza(16)
      println(p3)
      val p4 = new Pizza("pseudo")
      println(p4)
    }
  }
}
