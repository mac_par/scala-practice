package com.practice.chpt04

import nowe.tutorial.adv.part04.try04.{Person => Perek}

import org.scalatest.funspec.AnyFunSpec

class PersonTest extends AnyFunSpec {
  describe("checking class") {
    it("printing out") {
      val person = new Person("Marco", "Polo")
      person.age_$eq(15)
      println(person)
      person.age = 10
      println(person)
    }

    it("printing out2") {
      val person = new Perek("Marco")
      person.name = "Mieszko"
      assert("Mieszko" == person.name)
    }
  }
}
