package approach.second.p1.weather

import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class WeatherServiceTest extends AnyFunSpec with Matchers with MockFactory {
  def lookupWeather(service: WeatherService, icaoCode: String): Option[Weather] = {
    if (!service.operational()) {
      None
    } else {
      Some(service.getWeather(icaoCode))
    }
  }

  describe("weather service - mock") {
    val mocken = mock[WeatherService]
    it("should work - mock") {
      val icadoCode = "PDX"
      (mocken.operational _: () => Boolean).expects().returns(true)
      (mocken.getWeather _).expects(icadoCode).returns(Weather(55.0, 0.0, 1012.0))

      val weather = lookupWeather(mocken, icadoCode)
      weather should be (defined)
      val tolerance = 1e-6
      weather.get.precip should be (0.0 +- tolerance)
      weather.get.pressure should be (1012.0 +- tolerance)
      weather.get.temp should be (55.0 +- tolerance)
    }
    val stubben = stub[WeatherService]
    it("should work - stub") {
      val icadoCode = "PDX"
      (stubben.operational _: () => Boolean).when().returns(true)
      (stubben.getWeather _).when(icadoCode).returns(Weather(55.0, 0.0, 1012.0))

      val weather = lookupWeather(stubben, icadoCode)
      weather should be (defined)
      val tolerance = 1e-6
      weather.get.precip should be (0.0 +- tolerance)
      weather.get.pressure should be (1012.0 +- tolerance)
      weather.get.temp should be (55.0 +- tolerance)
      (stubben.operational _: () => Boolean).verify().once()
      (stubben.getWeather _).verify(icadoCode).once()
    }
    it ("mocken 2") {
      val msg = "it is raining"
      (mocken.operational _: () => Boolean).expects().throwing(new IllegalStateException(msg))
      val ex = intercept[IllegalStateException] {
        mocken.operational()
      }

      ex.getMessage should be (msg)
    }
    it ("mocken 2a - fail") {
      val msg = "it is raining"
      (mocken.operational _: () => Boolean).expects().returns(true)
      val ex = intercept[IllegalStateException] {
        mocken.operational()
      }

      ex.getMessage should be (msg)
    }
  }
}
