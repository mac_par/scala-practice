package com.hands

object TestTest extends App {
  val array = Array.ofDim[Int](5)
  array(3) = 3
  println(array.mkString(", "))

  hello2(Some("Alicante"))
  hello2(None)
  println(size(Some("Alicante")))
  println(size(None))
  def hello2(name: Option[String]): Unit = {
    for (s <- name) println(s)
  }

  def size(name: Option[String]): Int = {
    name.map(_.length).getOrElse(-1)
  }

  val cos = for {i<- 1 to 10 by 2} yield i
  println(cos)
}
