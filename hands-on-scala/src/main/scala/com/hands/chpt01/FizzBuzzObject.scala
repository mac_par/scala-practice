package com.hands.chpt01

object FizzBuzzObject extends App {

  def isDivisible(value: Int, divisor: Int, result: String): Boolean = value % divisor match {
    case 0 => true
    case _ => false
  }

  def isDivisible2(value: Int, divisor: Int, result: String): Boolean = value % divisor == 0

  def flexibleBuzz(f: String => Unit): Unit = {
    for {i <- Range(1, 20)} {
        var result: String = ""
        if (isDivisible(i, 3, "Fizz")) {
          result = "Fizz"
        }
        if (isDivisible(i, 5, "Buzz")) {
          result += "Buzz"
        }
        if (result == "") {
          result = i.toString
        }
        f(result)
    }
  }

  flexibleBuzz(println)
  val box = new Box(1)
  box.print("Hi")
  box.update(_ * 5)
  box.print("Hi")
}
