package com.hands.chpt01

class Box(var x: Int) {
  def update(f: Int => Int) = x = f(x)

  def print(msg: String): Unit = println(s"$msg - $x")
}
