package com.hands.chpt01

object Printing extends App {
  def printMessages(items: Array[Msg]): Unit = {
    def printFrag(parent: Option[Int], indent: String): Unit = {
      for {item <- items if item.parent == parent} {
        println(s"${indent}#${item.id} - ${item.txt}")
        printFrag(Some(item.id), indent + " ")
      }
    }

    printFrag(None, "")
  }

  printMessages(Array(Msg(0, None, "Hello"), Msg(1, Some(0), "World"), Msg(2, None, "I am cow"), Msg(3, Some(2), "Hear me moo"),
    Msg(4, Some(2), "Here I stood"), Msg(5, Some(2), "I am Cow"), Msg(6, Some(5), "here me moo, moo")))
}
