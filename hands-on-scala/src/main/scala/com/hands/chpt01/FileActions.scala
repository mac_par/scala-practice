package com.hands.chpt01

import java.io._
import scala.util.{Success, Try}

object FileActions extends App {
  withFileWriter("File.txt") { writer =>
    writer.write("Hello\n"); writer.write("World!")
  }
  val result = withFileReader("File.txt") { reader =>
    reader.readLine() + "\n" + reader.readLine()
  }

  def withFileWriter[T](file: String)(f: Writer => T) = {
    Try(new BufferedWriter(new FileWriter(file))) match {
      case Success(writer) => try {
        f(writer)
      } finally {
        writer.close()
      }
    }
  }

  def withFileReader[T](str: String)(f: BufferedReader => T): T = {
    Try(new BufferedReader(new FileReader(str))) match {
      case Success(reader) => try {
        f(reader)
      } finally {
        reader.close()
      }
    }
  }

  assert(result == "Hello\nWorld!")
}
