package com.hands.chpt01

class Foo(x: Int) {
  def printIt(msg: String) = println(x + msg)
}
