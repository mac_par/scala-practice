package com.hands.chpt01

object Loops extends App {
  def myLoop(start: Int, end: Int)(callback: Int => Unit) = {
    for {i <- Range(start,end)} {
      callback(i)
    }
  }


  myLoop(1, 20)(i => println(s"i has value: ${i}"))
  val foo = new Foo(5)
  foo.printIt("Hi")
}
