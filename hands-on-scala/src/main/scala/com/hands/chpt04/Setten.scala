package com.hands.chpt04

object Setten extends App {
  val set1 = Set(1,2,3,4,5)
  println(set1 - 2)
  println(set1 + 22)
  println(set1 -- Set(3,4))
  println(set1 ++ Set(3,4,6,7,8))
}
