package com.hands.chpt04

import scala.collection.mutable

object StdDevApp extends App {
  def stdDev(a: Array[Double]): Double = {
    val mean = a.sum / a.length
    val squareErrors = a.map(_ - mean).map(x => x * x)
    math.sqrt(squareErrors.sum / a.length)
  }

  val b: mutable.ArrayBuilder[Int] = Array.newBuilder[Int]
  b += 1
  b += 2
  println(b.result().mkString(","))


  val withHello = Array.fill(5)("hello")
  println(withHello.mkString(" "))

  val tab = Array.tabulate(5)(n=> s"Hi $n")
  println(tab.mkString(" "))

  val mixing = withHello ++ tab
  println(mixing.mkString(" "))

  val distinct = Array(11,2,3,4,5,1,2,1,3,4,2,1,11).distinct
  println(distinct.mkString(" "))

  val values = (1 to 10).toArray
  println(values.find(x => x > 4 && x %2 == 0))
  println(values.exists(_ < 0))
}
