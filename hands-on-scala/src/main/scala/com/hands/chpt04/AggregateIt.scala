package com.hands.chpt04

object AggregateIt extends App {
  val array = (1 to 10).toArray
  println(array.foldLeft(0)(_ + _))
  println(array.grouped(3).mkString(", "))
  println(array.groupBy(_ % 2))
  println(array.to[Vector])
}
