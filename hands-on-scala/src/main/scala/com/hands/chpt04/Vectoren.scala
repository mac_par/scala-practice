package com.hands.chpt04

object Vectoren extends App {
  val v1 = Vector.empty[Int]

  val v2 = v1 :+ 1
  println(v2)
  val v3 = 2 +: v2
  println(v3)
  val v4 = v3 :+ 4
  println(v4)
  println(s"head ${v4.head} - tail: ${v4.tail}")
}
