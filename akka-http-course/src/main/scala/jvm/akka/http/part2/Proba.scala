package jvm.akka.http.part2

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.IncomingConnection
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpHeader, HttpMethods, HttpProtocols, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.stream.Materializer
import akka.stream.scaladsl.Sink

import scala.concurrent.{ExecutionContext, Future}

object Proba {
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("system")
    implicit val materializer = Materializer

    val binding = Http().bind("localhost", 8080)
    implicit val executor = ExecutionContext.global

    val requestHandler: HttpRequest => Future[HttpResponse] = {
      case HttpRequest(HttpMethods.GET, path,_,body,_) => println(s"Get: Otrzymano: $body")
        Future(HttpResponse(status = StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`,
          """
            |{
            |   "id": 12345,
            |   "mowa": 12365,
            |   "kwestia":"to zloto"
            |}
            |""".stripMargin)))
      case HttpRequest(HttpMethods.POST, Uri.Path("/home"),_,body,_) =>println(s"Post: Otrzymano - $body")
        Future(HttpResponse(status = StatusCodes.Created))
      case _ => Future(HttpResponse(StatusCodes.BadRequest, entity = HttpEntity(ContentTypes.`text/html(UTF-8)`, "dupa nie request")))
    }
    val sink = Sink.foreach[IncomingConnection] { connection =>
      connection.handleWithAsyncHandler(requestHandler)
    }

    binding.runWith(sink)
  }
}
