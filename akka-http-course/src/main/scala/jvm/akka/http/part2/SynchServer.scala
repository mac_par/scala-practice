package jvm.akka.http.part2

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.IncomingConnection
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success}

class SynchServer(host: String = "localhost", port: Int = 8080) {
  val log = LoggerFactory.getLogger(SynchServer.getClass)
  implicit val system = ActorSystem("SynchServer")
  implicit val materializer = ActorMaterializer
  val serverSource = Http().bind(host, port)

  def run: Unit = {
    val connectionSink = Sink.foreach[IncomingConnection] { connection =>
      log.info(s"Connection received from: ${connection.remoteAddress}")
      connection.handleWithSyncHandler(requestHandler)
    }

    val serverBinding = serverSource.to(connectionSink).run()
    import system.dispatcher
    serverBinding.onComplete {
      case Success(binding) => log.info("Successful binding")
      case Failure(exception) => log.error("Sth wen wrong", exception)
    }

  }

  val requestHandler: HttpRequest => HttpResponse = {
    case HttpRequest(HttpMethods.GET, uri, headers, entity, protocol) => HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`,
      """
        |{"message":"Hi from Akka Http"}
        |""".stripMargin))
    case request: HttpRequest => request.discardEntityBytes()
      HttpResponse(StatusCodes.NotFound, entity = HttpEntity(ContentTypes.`text/html(UTF-8)`,
        """
          |<html>
          |<head>
          | <title>Jakis tam tytul</title>
          |</head>
          |<body>
          |<h1>Hi There</h1><p>Your request was not processed</p>
          |</body>
          |</html>
          |""".stripMargin))
  }

}

object SynchServer {
  def main(args: Array[String]): Unit = {
    val server = new SynchServer
    server.run
  }
}
