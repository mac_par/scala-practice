package jvm.akka.http.part2

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.IncomingConnection
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpProtocols, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import org.slf4j.LoggerFactory

import scala.concurrent.Future

class AsynchHttpServer(host: String = "localhost", port: Int = 8080) {
  val log = LoggerFactory.getLogger(AsynchHttpServer.getClass)

  implicit val system = ActorSystem("AsynchLowApi")
  implicit val materializer = ActorMaterializer
  val connectionBinding = Http().bind(host, port)
  import system.dispatcher
  val requestHandler: HttpRequest => Future[HttpResponse] = {
    case HttpRequest(HttpMethods.GET, Uri.Path("/home"), _,entity, protocol) => Future(HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`,
      """
        |{"message": "Hi Traveller from /home"}
        |""".stripMargin)))
    case HttpRequest(HttpMethods.GET,_, _,entity, protocol) => Future(HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`,
      """
           |{"message": "Welcome from Akka"}
           |""".stripMargin)))

    case request: HttpRequest =>
      request.discardEntityBytes()
      Future(HttpResponse(StatusCodes.NotFound, protocol = HttpProtocols.`HTTP/1.1`, entity = HttpEntity(ContentTypes.`text/html(UTF-8)`,
        """
          |<html>
          |<head>
          | <title>Sth went wrong</title>
          |</head>
          |<body>
          |<p>This type of request is not supported</p>
          |</body>
          |</html>
          |""".stripMargin)))
  }


  def run: Unit = {
    val sink = Sink.foreach[IncomingConnection]{ connection =>
      log.info("Connection from {}", connection.remoteAddress)
      connection.handleWithAsyncHandler(requestHandler, 4)
    }

    connectionBinding.to(sink).run()
  }
}

object AsynchHttpServer {
  def main(args: Array[String]): Unit = {
    val server = new AsynchHttpServer
    server.run
  }
}
