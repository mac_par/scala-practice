package jvm.akka.http.part2

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.IncomingConnection
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink

import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpLowApi extends App {
  implicit val system = ActorSystem("LowApiSystem")
  implicit val materializer = ActorMaterializer

  import system.dispatcher

  val serverSource = Http().bind("localhost", 8090)
  val connectionSink = Sink.foreach[IncomingConnection] { connection =>
    println(s"Received connection from: ${connection.remoteAddress}")
  }

  private val serverBinding = serverSource.to(connectionSink).run()

  serverBinding.onComplete {
    case Success(binding) => println("Server binding successful")
      binding.terminate(2 seconds)
    case Failure(exception) => println(s"Shit have hit the fan: $exception")
  }
}
