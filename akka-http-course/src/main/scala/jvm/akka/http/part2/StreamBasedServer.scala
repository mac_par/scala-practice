package jvm.akka.http.part2

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpProtocols, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.stream.Materializer
import akka.stream.scaladsl.Flow

class StreamBasedServer(host: String = "localhost", port: Int = 8080) {
  implicit val system = ActorSystem("StreamBasedSystem")
  implicit val materializer = Materializer



  val streamHandler: Flow[HttpRequest, HttpResponse, _] = Flow[HttpRequest].map{
    case HttpRequest(HttpMethods.GET, Uri.Path("/home"), _,entity, protocol) => HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`,
      """
        |{"message": "Hi Traveller from /home"}
        |""".stripMargin))
    case HttpRequest(HttpMethods.GET,_, _,entity, protocol) => HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`application/json`,
      """
        |{"message": "Welcome from Akka"}
        |""".stripMargin))

    case request: HttpRequest =>
      request.discardEntityBytes()
      HttpResponse(StatusCodes.NotFound, protocol = HttpProtocols.`HTTP/1.1`, entity = HttpEntity(ContentTypes.`text/html(UTF-8)`,
        """
          |<html>
          |<head>
          | <title>Sth went wrong</title>
          |</head>
          |<body>
          |<p>This type of request is not supported</p>
          |</body>
          |</html>
          |""".stripMargin))
  }


  def run: Unit = {
    val binding = Http().bind(host, port)
    binding.runForeach{connection =>
      connection.handleWith(streamHandler)
    }
  }
}

object StreamBasedServer extends App {
  val server = new StreamBasedServer
  server.run
}

