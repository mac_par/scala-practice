package jvm.akka.http.part1

import akka.actor.ActorSystem
import akka.stream.{Materializer, OverflowStrategy}
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.util.{Failure, Success}

object AkkaStreamRecap extends App {
  val system = ActorSystem("akkaStreamSystem")
  val materializer = Materializer(system)

  val source = Source(1 to 1000)
  val sink = Sink.foreach[Int](i => println(s"Yeah $i"))
  val flow = Flow[Int].map(_ + 1)
  val runnableGraph = source.via(flow).to(sink)
  runnableGraph.run()(materializer)

  val sinkFold = Sink.fold[Int, Int](0) {
    _ + _
  }
  val sumSink = Source(1 to 1000).via(flow).runWith(sinkFold)(materializer)
  sumSink.onComplete {
    case Success(value) => println(s"Sum: $value")
    case Failure(exception) => println("Cos poszlo nie tak")
  }(system.dispatcher)

  val bufferedFlow = Flow[Int].buffer(10, OverflowStrategy.dropHead)
  Source(1 to 10000).async.via(bufferedFlow).async.runForeach{item =>
    Thread.sleep(100)
    println(s"Wartość $item")
  }(materializer)

  Thread.sleep(5540)
  system.terminate()
}
