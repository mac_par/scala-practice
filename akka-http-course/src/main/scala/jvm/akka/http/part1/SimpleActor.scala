package jvm.akka.http.part1

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor.{Actor, ActorLogging, OneForOneStrategy, Props, Stash, SupervisorStrategy}

class SimpleActor extends Actor with Stash with ActorLogging {

  override def receive: Receive = {
    case message: String => log.info(s"I received: $message")
    case number: Int => log.info(s"I received number: $number")
    case SimpleActor.CHANGE => context.become(anotherHandler)
    case SimpleActor.PROCESS_NOW => {
      unstashAll()
      context.become(processThenAll)
    }
    case SimpleActor.QUESTION => "Marco Polo wielkim polakiem byl"
    case _ => stash()
  }

  def anotherHandler: Receive = {
    case message => s"Action from other Receiver: $message"
  }

  def processThenAll: Receive = {
    case message => s"Processing [$message]"
  }

  override def aroundPreStart(): Unit = log.info("I am about starting")

  override def preStart(): Unit = log.info("I thinking about ")

  override def unhandled(message: Any): Unit = log.info("could not process {}", message)

  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case _: RuntimeException => Restart
    case _ => Stop
  }
}

object SimpleActor {
  implicit val props = Props[SimpleActor]
  val CHANGE = "change"
  val PROCESS_NOW = "processNow"
  val QUESTION = "la question"
}
