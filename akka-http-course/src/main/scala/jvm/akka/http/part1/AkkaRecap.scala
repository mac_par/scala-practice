package jvm.akka.http.part1

import akka.actor.{ActorSystem, PoisonPill}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object AkkaRecap extends App {
  val system = ActorSystem("AkkaRecap")

  //implicits
  implicit val timeout: Timeout = Timeout(3500 milli)

  import akka.pattern.ask

  //create actor
  val actor = system.actorOf(SimpleActor.props, "simpleActor")
  system.scheduler.scheduleAtFixedRate(200 milli, 350 milli) {
    () => actor ! "It's your birthday"
  }(system.dispatcher)
  actor ! "hello"
  actor ! 12L
  actor ! 666
  actor ! 13.0
  val anotherActor = system.actorOf(SimpleActor.props, "anotherActor")
  actor ! true
  val future: Future[Any] = actor ? SimpleActor.QUESTION
  future.onComplete {
    println(_)

  }(ExecutionContext.global)
  actor ! SimpleActor.CHANGE
  actor ! SimpleActor.PROCESS_NOW

  SECONDS.sleep(3)
  actor ! PoisonPill
  system.terminate()
}
