package jvm.akka.http.part3

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.stream.Materializer
import spray.json._

case class Guitar(make: String, model: String)

class GuitarExample extends Actor with ActorLogging {
  import GuitarDB._

  //niemutowalne
  val guitars: scala.collection.mutable.Map[Int, Guitar] = scala.collection.mutable.Map()
  var counter:Int = 0
  override def receive: Receive = {
    case FindAllGuitars => log.info("Retrieving all guitars")
      sender() ! guitars.values.toList
    case FindGuitar(idx) => log.info("Find Guitar nbr {}", idx)
      sender() ! guitars.get(idx)
    case CreateGuitar(guitar) => log.info(s"Adding guitar $guitar with id: $counter")
      guitars.put(counter, guitar)
      sender() ! GuitarCreated(counter)
      counter += 1
  }
}

trait GuitarStoreJsonProtocol extends DefaultJsonProtocol {
  implicit val guitarFormat = jsonFormat2(Guitar)
}

object GuitarExample extends App  with GuitarStoreJsonProtocol {
  implicit val actorSystem = ActorSystem("GuitarDB")
  implicit val materializer = Materializer

  val simpleGuitar = Guitar("Fender", "Not gibson")
  val jsonString = simpleGuitar.toJson.prettyPrint
  println(jsonString)
  val guitar = jsonString.parseJson.convertTo[Guitar]
}

object GuitarDB {
  case class CreateGuitar(guitar: Guitar)
  case class GuitarCreated(id: Int)
  case class FindGuitar(id: Int)
  case object FindAllGuitars
}