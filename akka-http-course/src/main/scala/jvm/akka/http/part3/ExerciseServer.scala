package jvm.akka.http.part3

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Location
import akka.stream.Materializer
import akka.stream.scaladsl.Flow

class ExerciseServer(host: String = "localhost", port: Int = 8388) {
  implicit val system = ActorSystem("Server")
  implicit val materializer = Materializer

  val streamRequestHandler: Flow[HttpRequest, HttpResponse, _] = Flow[HttpRequest].map {
    case HttpRequest(HttpMethods.GET, Uri.Path("/about"), _, _, _) => HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`text/html(UTF-8)`,
      """
        |<html>
        |<head>
        |<title>Some title</title>
        |</head>
        |<body>
        |<h1>Loch Nesl></h1>
        |</body>
        |</html>
        |""".stripMargin))
    case HttpRequest(HttpMethods.GET, Uri.Path("/"), _, _, _) => HttpResponse(StatusCodes.OK, entity = HttpEntity(ContentTypes.`text/html(UTF-8)`, "front door"))
    case HttpRequest(HttpMethods.GET, Uri.Path("/search"), _, _, _) => HttpResponse(StatusCodes.Found, headers = scala.collection.immutable.Seq(Location("https://www.youtube.com")))
    case req: HttpRequest =>
      req.discardEntityBytes()
      HttpResponse(StatusCodes.NotFound)
  }

  def run: Unit = {
    val binding = Http().bind(host, port)
    binding.runForeach{connection => connection.handleWith(streamRequestHandler)}
  }
}

object ExerciseServer {
  def main(args: Array[String]): Unit = {
      val server = new ExerciseServer
    server.run
  }
}
