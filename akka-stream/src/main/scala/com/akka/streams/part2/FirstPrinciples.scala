package com.akka.streams.part2

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}

object FirstPrinciples extends App {
  implicit val system = ActorSystem("principles")

  val source = Source(1 to 10)
  val flow = Flow[Int].map{
    _ + 1
  }
  val sink = Sink.foreach[Int](println)
  val source2 = source.via(flow)
  val graph = source2.to(sink)
  graph.run()

  val illegalSource = Source.single[String](null)
  val illegalGraph = illegalSource.to(Sink.foreach(println))
  illegalGraph.run()

  system.terminate()
}
