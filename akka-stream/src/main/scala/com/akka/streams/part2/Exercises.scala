package com.akka.streams.part2

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, RunnableGraph, Sink, Source}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

object Exercises extends App {
  implicit val executor: ExecutionContext = ExecutionContext.global
  implicit val actorSystem: ActorSystem = ActorSystem("actoren")

  // return the last element
  Source(1 to 12).runWith(Sink.last[Int])

  //compute total word count
  val fileHandler = scala.io.Source.fromFile(getClass.getClassLoader.getResource("book.txt").toURI)
  val lines = fileHandler.getLines().toList;
  fileHandler.close()


  val flow = Flow[String].map(str => str.split("\\s+")).map[Int](_.length)
  val val1 = Source(lines).via(flow).runReduce(_+_)

  val1.onComplete{
    case Success(value) => println(s"Words: $value")
  }

  val result: (NotUsed, Future[Int]) = Flow[String].map(str => str.split("\\s+").toList).runWith(Source(lines), Sink.fold[Int,List[String]](0){ (cnt, list)=> cnt + list.size})
  result._2.onComplete{
    case Success(value) => println(s"Result: $value")
  }

  val result2 = Source(lines).map(_.split("\\s+").toList).runFold[Int](0){(cnt,list)=> cnt + list.size}
  result2.onComplete{
    case Success(value) => println(s"Result2: $value")
  }

  val flow2 = Flow[String].map(_.split("\\s+").toList)
  val flow3 = Flow[List[String]].fold[Int](0){(cnt, list)=> cnt + list.size}
  val result3 = Source(lines).viaMat(flow2)(Keep.right).viaMat(flow3)(Keep.right).runWith(Sink.head)
  result3.onComplete{
    case Success(value) => println(s"Result3: $value")
  }

  actorSystem.terminate()
}
