package com.akka.streams.part2

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Keep, RunnableGraph, Sink, Source}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


object MaterializingStreams extends App {
  implicit val executor: ExecutionContext = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("streams")
  val simpleGraph = Source(1 to 10).to(Sink.foreach(println))

  val materializedValue = simpleGraph.run()
  println(materializedValue)

  val graph2 = Source(1 to 10)
  val materializedValue2: Future[Int] = graph2.runWith(Sink.reduce(_ + _))

  materializedValue2.onComplete {
    case Success(value) => println(s"Some value: ${value}")
    case Failure(exception) => println(s"dupa: ${exception.getMessage}")
  }

  //choosing materialized values
  val simpleSource = Source(1 to 10)
  val simpleFlow = Flow[Int].map(_ + 1)
  val simpleSink = Sink.foreach(println)
  val returned: RunnableGraph[Future[Done]] = simpleSource.viaMat(simpleFlow)(Keep.right).toMat(simpleSink)(Keep.right)
  returned.run().onComplete {
    case Success(value) => println("done")
    case Failure(exception) => println("failed")
  }

  val sum: Future[Int] = Source(1 to 10).runReduce(_ + _)
  sum.onComplete {
    case Success(value) => println(s"Counted sum: $value")
    case Failure(exception) => println(s"Failed due to: ${exception.getMessage}")
  }

  Sink.foreach(println).runWith(Source(List(42)))

  Flow[Int].map(_ + 1).runWith(Source(12 to 22), Sink.foreach(println))

  system.terminate()
}
