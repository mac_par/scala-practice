package com.akka.streams.part2

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}

import scala.concurrent.{ExecutionContext, Future}

object ManySources extends App {
  implicit val executor:ExecutionContext = ExecutionContext.global
  implicit val system = ActorSystem("many_sources")

  val finiteSource = Source.single(1)
  val infiniteSource = Source(Stream.from(1))
  val emptySource = Source.empty[String]
  val futureSource = Source.future(Future(1))

  val ignoreSink = Sink.ignore
  val foldSink = Sink.fold[Int, Int](0){_ +_}

  val mapSource = Source(1 to 10).map(x => x * 2)
  mapSource.runForeach(println)

  val names = Source(List("Marco", "Genowefa", "Mietek", "Gucio", "Barbara"))
  names.filter(_.length > 5).take(3).runForeach(println)

  system.terminate()
}
