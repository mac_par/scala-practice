package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Balance, Broadcast, Concat, Flow, GraphDSL, Merge, Sink, Source}
import akka.stream.{FlowShape, SinkShape, SourceShape}

object OpenGraphBasic01 extends App {
  def createCustomFlow[A,B](sink: Sink[A, _], source: Source[B,_]): Flow[A,B,_] = {
    Flow.fromGraph(
      GraphDSL.create() {implicit builder =>
        val sinkShape = builder.add(sink)
        val sourceShape = builder.add(source)
        FlowShape(sinkShape.in, sourceShape.out)
      }
    )
  }

  implicit val system = ActorSystem("open-graph")

  val firstSource = Source(1 to 50)
  val secondSource = Source(75 to 1000)


  val source = Source.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val concat = builder.add(Concat[Int]())

      firstSource ~> concat
      secondSource ~> concat
      SourceShape(concat.out)
    }
  )

  val sink1 = Sink.foreach[Int] { x => println(s"Jakis sink1: $x") }
  val sink2 = Sink.foreach[Int] { x => println(s"Jakis sink2: $x") }

  val customSink = Sink.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val broadcast = builder.add(Broadcast[Int](2))
      broadcast ~> sink1
      broadcast ~> sink2

      SinkShape(broadcast.in)
    }
  )

  val customFlow = Flow.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      val incrementFlowShape = builder.add(Flow[Int].map(_ + 1))
      val multiplyFlowShape = builder.add(Flow[Int].map(_ * 10))
      incrementFlowShape ~> multiplyFlowShape
      FlowShape(incrementFlowShape.in, multiplyFlowShape.out)
    }
  )

  source.via(customFlow.async).to(customSink).run()

  system.terminate()
}
