package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Sink, Source}
import akka.stream.{FlowShape, SinkShape}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Success

object MaterializedValuesGraph extends App {



  implicit val system = ActorSystem("cos-tam")
  implicit val executor = ExecutionContext.global

  val wordSource = Source(List("Akka", "Is", "Awsome", "and", "other", "english", "words"))
  val printer = Sink.foreach[String](println)
  val counter = Sink.fold[Int, String](0) { (a, _) => a + 1 }
  //print all strings in lower case
  //counts short strings < 5

  val compositeSink: Sink[String, Future[Int]] = Sink.fromGraph(
    GraphDSL.create(printer, counter)((printerMat, counterMat) => counterMat) {
      implicit builder =>
        (printerSink, counterSink) =>
          import GraphDSL.Implicits._
          val broadcast = builder.add(Broadcast[String](2))
          val printFlow = builder.add(Flow[String].filter(str => str == str.toLowerCase))
          val countFlow = builder.add(Flow[String].filter(str => str.length < 5))

          broadcast ~> printFlow ~> printerSink
          broadcast ~> countFlow ~> counterSink
          SinkShape(broadcast.in)
    }
  )

  val result: Future[Int] = wordSource.toMat(compositeSink)(Keep.right).run()
  result.onComplete {
    case Success(value) => println(value)
  }
  Await.result(result, 1 second)
  system.terminate()
}
