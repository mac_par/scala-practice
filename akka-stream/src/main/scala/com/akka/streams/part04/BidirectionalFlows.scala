package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.{BidiShape, ClosedShape}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}

object BidirectionalFlows extends App {
  implicit val system = ActorSystem("fdsf")
  val slide = 3

  val biDirectionalFlow = GraphDSL.create() { implicit builder =>

    val encryptionFlowShape = builder.add(Flow[String].map(encrypt(slide)))
    val decryptFlowShape = builder.add(Flow[String].map(decrypt(slide)))
    BidiShape.fromFlows(encryptionFlowShape, decryptFlowShape)
  }

  val items = List("Cos", "tam", "dalej", "gowno", "dupa")
  val sourceEncrypt = Source(items)
  val sourceDecrypt = Source(items.map(encrypt(slide)))

  val graphen = RunnableGraph.fromGraph(GraphDSL.create(){ implicit builder =>
    val encryptedSourceShape = builder.add(sourceEncrypt)
    val decryptSourceShape = builder.add(sourceDecrypt)
    val bidiShape = builder.add(biDirectionalFlow)
    val encryptSink = builder.add(Sink.foreach[String](str => println(s"Encrypted: $str")))
    val decryptSink = builder.add(Sink.foreach[String](str => println(s"Decrypted: $str")))

    import GraphDSL.Implicits._
    encryptedSourceShape ~> bidiShape.in1
    bidiShape.out1 ~> encryptSink
    decryptSourceShape ~> bidiShape.in2
    bidiShape.out2 ~> decryptSink
    ClosedShape
  })

  graphen.run
  system.terminate()


  def encrypt(n: Int)(string: String): String = string.map { c => (c + n).toChar }

  def decrypt(n: Int)(string: String): String = string.map { c => (c - n).toChar }
}
