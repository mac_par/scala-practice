package com.akka.streams.part04

import java.util.Date

case class Transaction(id: String, source: String, recipient: String, amount: Int, date: Date)
