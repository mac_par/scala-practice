package com.akka.streams.part04

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source}

object GraphBasis02 extends App {
  implicit val actorSystem: ActorSystem = ActorSystem("graph-tries-02")

  val source = Source(1 to 1000)
  val sink1 = Sink.foreach[Int]{
    x=> println(s"Sink1: $x")
  }

  val sink2 = Sink.foreach[Int]{
    x=> println(s"Sink2: $x")
  }

  val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>
    val broadcast = builder.add(Broadcast[Int](2))
    import GraphDSL.Implicits._
    source ~> broadcast ~>  sink1
              broadcast ~>  sink2
    ClosedShape

  })

  graph.run()

  actorSystem.terminate()
}
