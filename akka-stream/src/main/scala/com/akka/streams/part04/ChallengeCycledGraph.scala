package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, MergePreferred, RunnableGraph, Sink, Source, Zip, ZipWith}

import scala.annotation.tailrec

object ChallengeCycledGraph extends App {
  implicit val system = ActorSystem("dsf")

  val graph = RunnableGraph.fromGraph(GraphDSL.create() {implicit builder => {
    import GraphDSL.Implicits._
    val source1Shape = builder.add(Source(List(1)))
    val source2Shape = builder.add(Source(List(1)))
    val zipShape = builder.add(Zip[Int,Int]())
    val mergedShape = builder.add(MergePreferred[(Int,Int)](1))
    val flowConvertShape = builder.add(Flow[(Int,Int)].map{x => (x._1 + x._2, x._2)})
    val broadcastShape = builder.add(Broadcast[(Int,Int)](2))
    val outputSink = builder.add(Sink.foreach[(Int,Int)](x => println(x._1)))
    val feederShape = builder.add(Flow[(Int, Int)].map(x => x))

    source1Shape.out ~> zipShape.in0
    source2Shape.out ~> zipShape.in1
    zipShape.out ~> mergedShape ~> flowConvertShape ~> broadcastShape
    broadcastShape ~> feederShape ~> mergedShape.preferred
    broadcastShape ~> outputSink
    ClosedShape
  }})

  graph.run()
  system.terminate()

  println(fib(8))

  @tailrec
  private def fib(n: Int, acc1: Int = 1, acc2: Int = 1, seq: List[Int] = List(1)): List[Int] = {
    if (n <= 1) seq else fib(n -1, acc2, acc1 + acc2, seq :+ acc2)
  }
}
