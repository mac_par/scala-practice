package com.akka.streams.part04

import java.util.Date

import akka.actor.ActorSystem
import akka.stream.{ClosedShape, FanOutShape, FanOutShape2}
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, RunnableGraph, Sink, Source}

object MoreAbsorbingGraph extends App {
  implicit val system = ActorSystem("fdfsd")
  //txn > 10K
  // - same txn
  // - suspicious transaction
  val transationSource = Source(List(
    Transaction("cos1", "Paul", "Peter", 100, new Date),
    Transaction("cos2", "Martin", "Escobar", 1000, new Date),
    Transaction("cos3", "Martina", "Trapito", 700, new Date)
  ))

  val bankProcessing = Sink.foreach[Transaction](println)
  val suspicious = Sink.foreach[String](x => println(s"Suspicious transaction: [$x]"))

  val processingStaticGraph = GraphDSL.create() {implicit builder =>
    import GraphDSL.Implicits._
    val broadcast = builder.add(Broadcast[Transaction](2))
    val transactionFilter = builder.add(Flow[Transaction].filter(t => t.amount >= 1000).map(_.id))
    broadcast.out(0) ~> transactionFilter.in
    new FanOutShape2(broadcast.in, transactionFilter.out, broadcast.out(1))
  }

  val runnableGraph = RunnableGraph.fromGraph(
    GraphDSL.create() {implicit builder =>
      import GraphDSL.Implicits._
      val feeder = builder.add(processingStaticGraph)
      transationSource ~> feeder.in
      feeder.out0 ~> suspicious
      feeder.out1 ~> bankProcessing
      ClosedShape
    }
  )

  runnableGraph.run()

  system.terminate()
}
