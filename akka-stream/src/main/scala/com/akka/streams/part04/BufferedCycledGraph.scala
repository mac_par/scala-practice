package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.{ClosedShape, OverflowStrategy}
import akka.stream.scaladsl.{Flow, GraphDSL, Merge, MergePreferred, RunnableGraph, Source}

object BufferedCycledGraph extends App {
  implicit val system = ActorSystem("dsf")

  val graph = RunnableGraph.fromGraph(GraphDSL.create() {implicit builder =>
    import GraphDSL.Implicits._
    val sourceShape = builder.add(Source(1 to 100))
    val flowShape = builder.add(Flow[Int].buffer(25, OverflowStrategy.dropHead).map(x => {println(s"Accelerated: $x"); Thread.sleep(100) ;x}))
    val mergeShape = builder.add(Merge[Int](2))
    sourceShape ~> mergeShape ~> flowShape
    mergeShape <~ flowShape
    ClosedShape
  })

  graph.run()

  system.terminate()
}
