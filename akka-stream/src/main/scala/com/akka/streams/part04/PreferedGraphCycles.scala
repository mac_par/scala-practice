package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Flow, GraphDSL, MergePreferred, RunnableGraph, Source}

object PreferedGraphCycles extends App {
  implicit val system = ActorSystem("dfsf")

  val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder =>
    import GraphDSL.Implicits._
    val sourceShape = builder.add(Source(1 to 100))
    val flowShape = builder.add(Flow[Int].map(x => {
      println(s"Accelerated: $x")
      x + 1
    }))
    val merger = builder.add(MergePreferred[Int](1))

    sourceShape ~> merger ~> flowShape
    merger.preferred <~ flowShape
    ClosedShape
  })

  graph.run()

  system.terminate()
}
