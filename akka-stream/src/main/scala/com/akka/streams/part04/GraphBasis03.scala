package com.akka.streams.part04

import scala.concurrent.duration._
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Balance, Flow, GraphDSL, Merge, RunnableGraph, Sink, Source}

object GraphBasis03 extends App {
  implicit val actorSystem = ActorSystem("graph-basis-03")

  val source = Source(1 to 1000)
  val fastSource = source.throttle(5, 1 second)

  val slowSource = source.throttle(2, 1 second)

  val sink1 = Sink.foreach[Int]{x => println(s"Sink1: $x")}
  val sink2 = Sink.foreach[Int]{x => println(s"Sink2: $x")}


  val graph = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder: GraphDSL.Builder[NotUsed] =>

    val merge = builder.add(Merge[Int](2))
    val balancer = builder.add(Balance[Int](2))
    import GraphDSL.Implicits._

    source ~> merge
    source ~> merge
    merge ~> balancer
    balancer ~> sink1
    balancer ~> sink2

    ClosedShape
  })

  graph.run()
  actorSystem.terminate()
}
