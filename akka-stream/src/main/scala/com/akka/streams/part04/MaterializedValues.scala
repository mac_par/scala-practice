package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.SinkShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Sink, Source}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object MaterializedValues extends App {
  implicit val system = ActorSystem("fddfs")
  implicit val executionContext = ExecutionContext.global

  val source = Source(List("Cos", "tam", "dalej", "ale","co", "tam"))
  val printer = Sink.foreach(println)
  val counter = Sink.fold[Int,String](0){(cnt,_)=> cnt + 1}

  val compositeSink = Sink.fromGraph(GraphDSL.create(printer, counter)((m1, m2) => m2) {implicit builder=> (printerSink, counterSink) =>
  import GraphDSL.Implicits._

    val broadcast = builder.add(Broadcast[String](2))
    broadcast ~> printerSink
    broadcast ~> counterSink
    SinkShape(broadcast.in)
  })

  val eventualInt: Future[Int] = source.toMat(compositeSink)(Keep.right).run()
  eventualInt.onComplete{
    case Success(value) => println(value)
    case Failure(exception) => println(exception.getMessage)
  }
  system.terminate()
}
