package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.{ClosedShape, FlowShape, UniformFanInShape}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source, ZipWith}

import scala.math.max

object MoreMoreGraphs extends App {
  implicit val system = ActorSystem("graphs")

  //Max out of 3 sources
  val static3InGraph = GraphDSL.create() {implicit builder =>
    import GraphDSL.Implicits._
    val flow1 = builder.add(ZipWith[Int, Int, Int]((a,b) => scala.math.max(a,b)))
    val flow2 = builder.add(ZipWith[Int, Int, Int]((a,b) => scala.math.max(a,b)))

    flow1.out ~> flow2.in0
    UniformFanInShape(flow2.out, flow1.in0, flow1.in1, flow2.in1)
  }

  val source1 = Source(1 to 10)
  val source2 = Source(1 to 10).map(_ => 5)
  val source3 = Source((1 to 10).reverse)

  val sink = Sink.foreach[Int](x => println(s"Max: $x"))

  val runnableGraph = RunnableGraph.fromGraph(
    GraphDSL.create() {implicit builder =>
      import GraphDSL.Implicits._
      val flow = builder.add(static3InGraph)
      source1 ~> flow.in(0)
      source2 ~> flow.in(1)
      source3 ~> flow.in(2)
      flow.out ~> sink
      ClosedShape
    }
  )

  runnableGraph.run()
  system.terminate()
}
