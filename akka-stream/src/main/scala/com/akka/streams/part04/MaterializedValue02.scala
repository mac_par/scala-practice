package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.FlowShape
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Sink, Source}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Success

object MaterializedValue02 extends App {
  def convertFlow[A, B](flow: Flow[A, B, _]): Flow[A, B, Future[Int]] = {
    Flow.fromGraph(
      GraphDSL.create(Sink.fold[Int, B](0) { (counter, _) => counter + 1 }, flow)((mat1, mat2) => mat1) {
        implicit builder =>
          (counterShape, originalFlow) =>
            val broadcast = builder.add(Broadcast[B](2))
            import GraphDSL.Implicits._
            originalFlow ~> broadcast ~> counterShape

            FlowShape(originalFlow.in, broadcast.out(1))
      }
    )
  }

  implicit val executionContext = ExecutionContext.global
  implicit val system = ActorSystem("df")

  val source = Source(1 to 10)
  val flow = Flow[Int].map(_.toString)
  val customFlow = convertFlow(flow)
  val sink = Sink.foreach(println)

  val result: Future[Int] = source.viaMat(customFlow)(Keep.right).to(sink).run()
  result.onComplete{
    case Success(value) => println(s"Success: $value")
  }
  system.terminate()
}
