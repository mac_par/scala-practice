package com.akka.streams.part04

import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Flow, GraphDSL, Merge, RunnableGraph, Source}

object GraphCycles extends App {
  implicit val system = ActorSystem("dfsf")

  val graph = GraphDSL.create() {implicit builder =>
    import GraphDSL.Implicits._
    val sourceShape = builder.add(Source(1 to 100))
    val mergeShape = builder.add(Merge[Int](2))
    val flowShape = builder.add(Flow[Int].map(x => {println(s"Accelerating: $x"); x + 1}))

    sourceShape ~> mergeShape ~> flowShape
    mergeShape <~ flowShape
    ClosedShape
  }

  RunnableGraph.fromGraph(graph).run()

  Thread.sleep(5000)
  system.terminate()
}
