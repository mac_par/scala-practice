package com.akka.streams.part03

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}

object OrderingSession extends App {
  implicit val system = ActorSystem("order-it")

  Source(1 to 4)
      .map(x => {println(s"Flow A: $x"); x}).async
      .map(x => {println(s"Flow B: $x"); x}).async
      .map(x => {println(s"Flow C: $x"); x}).async
      .to(Sink.ignore).run()

  Thread.sleep(5000)
  system.terminate()
}
