package com.akka.streams.part03

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.sun.org.slf4j.internal.{Logger, LoggerFactory}

object AsyncGraphen extends App {
  implicit val system: ActorSystem = ActorSystem("async-graph")

  val log = system.log
  val simpleSource = Source(1 to 1000)
  val simpleFlow = Flow[Int].map { x => Thread.sleep(1000); x + 1 }
  val simpleFlow2 = Flow[Int].map { x => Thread.sleep(1000); x * 10 }
  val simpleSink = Sink.foreach[Int](println)

  //async - each part runs on separate actor
  simpleSource.via(simpleFlow.async)
    .via(simpleFlow2.async)
    .to(simpleSink).run()

  Thread.sleep(10000)
  system.terminate()
}
