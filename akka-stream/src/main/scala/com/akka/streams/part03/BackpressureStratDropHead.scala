package com.akka.streams.part03

import akka.actor.ActorSystem
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}

object BackpressureStratDropHead extends App {
  implicit val system: ActorSystem = ActorSystem("backpressure")

  val fastSource = Source(1 to 1000)
  val flow = Flow[Int].map { x =>
    println(s"Incoming: $x")
    x + 1
  }.buffer(5, OverflowStrategy.dropBuffer)
  val slowConsumer = Sink.foreach[Int](elem => {
    Thread.sleep(1000)
    println(s"Received: $elem")
  })

  fastSource.async.via(flow).async.to(slowConsumer).run()


  Thread.sleep(100000)
  system.terminate()
}
