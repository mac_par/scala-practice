package com.akka.streams.part03

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}

object  OperationFusion extends App {
  implicit val system = ActorSystem("operation-confusion")

  val simpleSource = Source(1 to 1000)
  val simpleFlow = Flow[Int].map{ x=> Thread.sleep(1000); x+1}
  val simpleFlow2 = Flow[Int].map{ x=> Thread.sleep(1000); x*10}
  val simpleSink = Sink.foreach(println)

  //runs on the same actor
  simpleSource.via(simpleFlow).via(simpleFlow2).to(simpleSink).run()

  system.terminate()
}
