package com.akka.streams.zero

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}

object ObjectTry1 extends App {
  implicit val actorSystem = ActorSystem("stramen")
  Source.single("Hi there").to(Sink.foreach(println)).run()
}
