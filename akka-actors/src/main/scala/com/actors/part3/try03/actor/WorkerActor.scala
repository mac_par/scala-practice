package com.actors.part3.try03.actor

import akka.actor.{Actor, ActorLogging}

import scala.util.Random

class WorkerActor extends Actor with ActorLogging{
  import com.actors.part3.try03.message._
  override def receive: Receive = {
    case LongWork => log.info("Performing long lasting action")
      Thread.sleep(500)
      sender() ! WorkResult(42)
    case ShortWork => log.info("Performing few short actions")
      val random = new Random()
      for {_ <- 1 to 10} {
        Thread.sleep(random.nextInt(50))
        sender() ! WorkResult(1)
      }
  }
}
