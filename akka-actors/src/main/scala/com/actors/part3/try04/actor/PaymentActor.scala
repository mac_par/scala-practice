package com.actors.part3.try04.actor

import akka.actor.{Actor, ActorLogging}
import com.actors.part3.try04.message
import com.actors.part3.try04.message.{AuthorizeCard, PaymentAccepted}

class PaymentActor extends Actor with ActorLogging {
  override def receive: Receive = {
    case AuthorizeCard(creditCard) => sender() ! (if (creditCard.startsWith("0")) message.PaymentDenied else PaymentAccepted)
  }
}
