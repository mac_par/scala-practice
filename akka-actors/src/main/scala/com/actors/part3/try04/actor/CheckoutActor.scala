package com.actors.part3.try04.actor

import akka.actor.{Actor, ActorLogging, Props}
import com.actors.part3.try04.message.{AuthorizeCard, Checkout, DispatchOrder, OrderConfirmed, PaymentAccepted, PaymentDenied}

class CheckoutActor extends Actor with ActorLogging {
  import CheckoutActor._
  override def preStart(): Unit = {
    log.info("creating actors")
    context.actorOf(Props[PaymentActor], paymentActor)
    context.actorOf(Props[FulfillmentActor], fulfillmentActor)
  }

  override def receive: Receive = awaitingCheckout

  def awaitingCheckout: Receive = {
    case Checkout(item, creditCard) =>
      context.become(pendingPayment(item))
      context.actorSelection(paymentActor) ! AuthorizeCard(creditCard)
  }

  def pendingPayment(item: String): Receive = {
    case PaymentAccepted =>
      context.become(pendingFulfillment(item))
      context.actorSelection(fulfillmentActor) ! DispatchOrder(item)
    case PaymentDenied => throw new RuntimeException("I cannot handle it!");
  }

  def pendingFulfillment(item: String): Receive = {
    case OrderConfirmed => context.become(awaitingCheckout)
  }
}


object CheckoutActor {
  val paymentActor = "PaymentActor"
  val fulfillmentActor = "fulfillmentActor"
}