package com.actors.part3.try04.message

case class Checkout(item: String, creditCard: String)
