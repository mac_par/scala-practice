package com.actors.part3.try04.actor

import akka.actor.{Actor, ActorLogging}
import com.actors.part3.try04.message.{DispatchOrder, OrderConfirmed}

class FulfillmentActor extends Actor with ActorLogging {
  override def receive: Receive = orderHandler(0)

  def orderHandler(counter: Int): Receive = {
    case DispatchOrder(item) => sender() ! OrderConfirmed
      log.info("Order {} for item {} has been dispatched", counter, item)
      context.become(orderHandler(counter +1))
  }
}
