package com.actors.part3.try05.actor

import akka.actor.{Actor, ActorLogging}
import com.actors.part3.try05.message.{Inc, Read}

class Counter extends Actor with ActorLogging {
  override def receive: Receive = counting(0)

  def counting(counter: Int): Receive = {
    case Inc => context.become(counting(counter +1))
    case Read => sender() ! counter
  }
}
