package com.actors.part3.try02.message

import akka.actor.ActorRef

case class Register(slaveRef: ActorRef)
