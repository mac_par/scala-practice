package com.actors.part3.try02.message

import akka.actor.ActorRef

case class WorkCompleted(count: Int, sender: ActorRef)
