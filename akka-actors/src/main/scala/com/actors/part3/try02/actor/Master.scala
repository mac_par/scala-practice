package com.actors.part3.try02.actor

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.actors.part3.try02.message._

class Master extends Actor with ActorLogging {
  override def receive: Receive = {
    case Register(slaveRef) => sender() ! RegistrationAck
      context.become(online(slaveRef, 0))
    case _ => log.info("Unsupported action")
  }

  def online(slaveRef: ActorRef, totalWordCount: Int): Receive = {
    case Work(text) => slaveRef ! SlaveWork(text, sender())
    case WorkCompleted(count, senderRef) => val newTotalCount = totalWordCount + count
      senderRef ! Report(newTotalCount)
      context.become(online(slaveRef, newTotalCount))
  }
}
