package com.actors.part3.try01.actor

import akka.actor.{Actor, ActorLogging}

import scala.util.Random

class LabTestActor extends Actor with ActorLogging {
  val random = new Random()

  override def receive: Receive = {
    case "greeting" => if (random.nextBoolean()) "Hi!" else "Hello"
    case "favoriteTech" => sender() ! "Scala"
      sender() ! "Akka"
    case msg: String => sender() ! msg.toUpperCase
  }
}
