package com.actors.part3.try01.actor

import akka.actor.{Actor, ActorLogging}

class SimpleBasicActor extends Actor with ActorLogging {
  override def receive: Receive = {
    case message => sender() ! message
  }
}
