package com.actors.part2.try2

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.pattern._
import akka.util.Timeout

import scala.util.{Failure, Success}

object ActorCapabilites2 extends App {
  implicit val executor = scala.concurrent.ExecutionContext.global
  val actorSystem = ActorSystem("aktor-costam")
  val actor = actorSystem.actorOf(Props[SimpleActor], "Janko")
  implicit val timeout = Timeout(2, TimeUnit.SECONDS)
  actor ! "hello actor"
  actor ! SecretMessage("Marco Polo")
  actor ! Myself("jakis tam tekst")
  actor ask SayHi("Amy") onComplete{
    case Success(value) => println(s"Response: $value")
    case Failure(exception) => println(exception.getMessage)
  }
  val alice = actorSystem.actorOf(Props[SimpleActor], "Alice")
  alice ! Rply(actor)
  alice ! WirelessPhone("cos tam", actor)
  wait(1500)
  actorSystem.stop(actor)
  actorSystem.terminate() onComplete {
    case Success(value) => println("koniec")
    case Failure(exception) => println("jeszcze nie :/")
  }
}
