package com.actors.part2.try2

import akka.actor.ActorRef

case class WirelessPhone(content: String, recipent: ActorRef)
