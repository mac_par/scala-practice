package com.actors.part2.try2

import akka.actor.{Actor, ActorLogging}

class SimpleActor extends Actor with ActorLogging {
  override def receive: Receive = {
    case "Hi!" => sender() ! "Fuck ya!"
    case msg: String => log.info(s"Wiadomosc: $msg")
    case SecretMessage(name) => log.info("Tajna wiadomość shoguna dla {}", name)
    case Myself(content) => log.info("Otrzymano Myself")
      self ! content
    case SayHi(to) => sender() ! s"Hi $to"
    case Rply(actor) => actor ! "Hi!"
    case WirelessPhone(content, actor) => actor forward content //zachowuje wysylajacego
    case _ => println("cos nieznanego")
  }
}
