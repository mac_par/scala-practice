package com.actors.part2.try7.actor

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.actors.part2.try7.actor.Voter.CastedVote

class VoteAggregator extends Actor with ActorLogging {

  import VoteAggregator._

  override def receive: Receive = {
    case AggregateVotes(actors) =>
      actors.foreach(actor => actor ! VoteInquiry)
      context.become(awaitingStatuses(sender(), actors, Map()))
  }

  def awaitingStatuses(requester: ActorRef, actors: Set[ActorRef], currentResults: Map[String, Long]): Receive = {
    case CastedVote(None) => sender() ! VoteInquiry
    case CastedVote(Some(candidate)) =>
      val newActors = actors - sender()
      val votes = currentResults.getOrElse(candidate, 0L)
      val newResults = currentResults.+(candidate -> (votes + 1L))
      if (newActors.isEmpty) {
        log.info("Results: {}", newResults)
        requester ! VotingResult(newResults)
      } else {
        context.become(awaitingStatuses(requester, newActors, newResults))
      }
  }
}

object VoteAggregator {

  case object VoteInquiry

  case class AggregateVotes(voters: Set[ActorRef])

  case class VotingResult(result: Map[String, Long])

}
