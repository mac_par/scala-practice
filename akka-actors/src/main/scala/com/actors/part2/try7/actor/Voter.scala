package com.actors.part2.try7.actor

import akka.actor.{Actor, ActorLogging}

class Voter extends Actor with ActorLogging {

  import VoteAggregator._
  import Voter._

  override def receive: Receive = {
    case Vote(candidate) => context.become(voted(candidate))
    case VoteInquiry => sender() ! CastedVote(None)
    case _ => log.info("Unknown action")
  }

  def voted(candidate: String): Receive = {
    case Vote(_) => log.info("Voter has already cast his/her vote")
    case VoteInquiry => sender() ! CastedVote(Some(candidate))
    case _ => log.info("Unknown action")
  }
}

object Voter {

  case class CastedVote(candidate: Option[String])

  case class Vote(candidate: String)

}
