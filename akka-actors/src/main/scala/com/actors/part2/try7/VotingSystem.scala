package com.actors.part2.try7

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem, Props}
import com.actors.part2.try7.actor.VoteAggregator.{AggregateVotes, VotingResult}
import com.actors.part2.try7.actor.Voter.Vote
import com.actors.part2.try7.actor.{VoteAggregator, Voter}
import akka.pattern._
import akka.util.Timeout

import scala.util.{Failure, Success}

object VotingSystem extends App {
  implicit val executor = scala.concurrent.ExecutionContext.global
  implicit val timeout = Timeout(5, TimeUnit.SECONDS)
  val system = ActorSystem("VotingSystem")
  val votingSystem = system.actorOf(Props[VoteAggregator])
  val voter1 = system.actorOf(Props[Voter], "Voter1")
  val voter2 = system.actorOf(Props[Voter], "Voter2")
  val voter3 = system.actorOf(Props[Voter], "Voter3")
  val voter4 = system.actorOf(Props[Voter], "Voter4")
  val voter5 = system.actorOf(Props[Voter], "Voter5")
  val voter6 = system.actorOf(Props[Voter], "Voter6")

  voter1 ! Vote("Marco Polo")
  voter2 ! Vote("Janusz")
  voter3 ! Vote("Marco Polo")
  voter4 ! Vote("Duda")
  voter5 ! Vote("Janusz")
  voter6 ! Vote("Marco Polo")

  val actors = Set(voter1, voter2, voter3, voter4, voter5, voter6)
  val results = votingSystem ? AggregateVotes(actors)
  results.onComplete {
    case Success(value) => val results = value.asInstanceOf[VotingResult].result
      println(results)
    case Failure(exception) => println("System padl")
  }
  Thread.sleep(5000)
  actors.foreach{system.stop}
  system.terminate()

}
