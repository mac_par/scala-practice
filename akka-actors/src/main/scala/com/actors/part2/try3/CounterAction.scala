package com.actors.part2.try3

import akka.actor.{ActorRef, ActorSystem}
import com.actors.part2.try3.actor.CounterActor
import com.actors.part2.try3.event.{Decrement, Increment, PrintValue}

import scala.collection.immutable

object CounterAction extends App {
  val system = ActorSystem("CounterSystem")
  val actors: immutable.Seq[ActorRef] = for {value <- 1 to 10} yield system.actorOf(CounterActor.props, s"Actor-$value")
  val actorsList = actors.toList
  val size = actors.size
  val iterations = 100000
  for {x<-(1 to iterations)} {
    val actor1 =actorsList(x % size)
    val actor2 =actorsList((x+1) % size)
    new Thread(() => actor1 ! Increment).start()
    new Thread(() => {
      Thread.sleep(1000)
      actor2 ! Decrement
    }).start();
  }

  val checker = system.actorOf(CounterActor.props, "Checker")
  println(checker ! PrintValue)
  Thread.sleep(10000)
  println(checker ! PrintValue)

  for {actor <- actors} system.stop(actor)
  system.terminate()
}
