package com.actors.part2.try3.actor

import akka.actor.{Actor, ActorLogging, Props}
import com.actors.part2.try3.event.{Decrement, Increment, PrintValue}

class CounterActor extends Actor with ActorLogging {
  var counter: Long = 0

  override def receive: Receive = {
    case Increment => log.info("Increment {}", counter); counter += 1;
    case Decrement => log.info("Decrement {}", counter); counter -= 1;
    case PrintValue => log.info("Current state: {}", counter)
    case _ => log.info("unknown action")
  }
}

object CounterActor {
  val props: Props = Props(classOf[CounterActor])
}
