package com.actors.part2.try6.actor

import akka.actor.{Actor, ActorLogging, Props}
import com.actors.part2.try6.messages.{Decrement, Increment, PrintValue}

class CounterActor extends Actor with ActorLogging {

  override def receive: Receive = counterReceiver(0)

  def counterReceiver(counter: Long): Receive = {
    case Increment => log.info("Incrementing counter from {}", counter)
      context.become(counterReceiver(counter + 1L))
    case Decrement => log.info("Decrementing counter from {}", counter)
      context.become(counterReceiver(counter - 1L))
    case PrintValue => log.info("Current counter state: {}", counter)
  }
}

object CounterActor {
  val props: Props = Props(classOf[CounterActor])
}
