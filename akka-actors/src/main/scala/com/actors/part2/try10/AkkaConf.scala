package com.actors.part2.try10

import akka.actor.{ActorSystem, Props}
import com.actors.part2.try10.actor.SimpleActor
import com.typesafe.config.ConfigFactory

object AkkaConf extends App {
val conf =
  """
    | akka {
    | loglevel = "ERROR"
    | }
    |""".stripMargin

  val config = ConfigFactory.parseString(conf)
  val system = ActorSystem("conf_system", ConfigFactory.load(config))
  val actor = system.actorOf(Props[SimpleActor])
  actor ! "cos tam"
  Thread.sleep(500)
  system.terminate()
}
