package com.actors.part2.try10

import akka.actor.{ActorSystem, Props}
import com.actors.part2.try10.actor.SimpleActor
import com.typesafe.config.ConfigFactory

object NewSystem extends App {
  val system = ActorSystem("Simple-ActorSystem", ConfigFactory.load.getConfig("specialConf"))
  val actor = system.actorOf(Props[SimpleActor], "die-actoren")
  actor ! "malkontent"

  Thread.sleep(450)
  system.stop(actor)
  system.terminate()
}
