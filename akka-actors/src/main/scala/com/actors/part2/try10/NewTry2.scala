package com.actors.part2.try10

import akka.actor.{ActorSystem, Props}
import com.actors.part2.try10.actor.SimpleActor
import com.typesafe.config.ConfigFactory

object NewTry2 extends App {
  val system = ActorSystem("die-actoren", ConfigFactory.load("diren/cos.conf"))
  val actor = system.actorOf(Props[SimpleActor], "actor")
  actor ! "cos tam"

  Thread.sleep(500)
  system.stop(actor)
  system.terminate()
}
