package com.actors.part2.try10

import akka.actor.{ActorSystem, Props}
import com.actors.part2.try10.actor.SimpleActor
import com.typesafe.config.ConfigFactory

object Config3FromJSON extends App {
  val config = ConfigFactory.load("json/config.json")
  val system = ActorSystem("Systememm", config)
  val actor = system.actorOf(Props[SimpleActor], "actoren")

  actor ! "cos tam dalej"
  println(s"${config.getString("jakas")}")
  Thread.sleep(500)
  system.stop(actor)
  system.terminate()
}