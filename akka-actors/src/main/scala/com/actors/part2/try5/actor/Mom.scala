package com.actors.part2.try5.actor

import akka.actor.{Actor, ActorLogging, ActorRef}

class Mom extends Actor with ActorLogging {
  import Mom._
  import FussyKid._
  override def receive: Receive = {
    case MomStart(kid) =>
      kid ! Ask("Do you want to play?")
      kid ! Food(VEGETABLES)
      kid ! Ask("Do you want to play")
    case Accept => log.info("Yay, Kid has agreed")
    case Reject => log.info("Damn, Kid has rejected")
  }
}

object Mom {
  case class MomStart(kidRef: ActorRef)
  case class Food(food: String)
  case class Ask(message: String)
  val VEGETABLES = "veggies"
  val CHOCOLATE = "chocolate"
}
