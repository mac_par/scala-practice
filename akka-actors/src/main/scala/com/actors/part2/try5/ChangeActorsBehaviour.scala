package com.actors.part2.try5

import akka.actor.{ActorSystem, Props}
import com.actors.part2.try5.actor.Mom.MomStart
import com.actors.part2.try5.actor.{FussyKid, Mom, StatelessFussyKid}

object ChangeActorsBehaviour {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("Mom-Kid-System")
    val kid = system.actorOf(Props[FussyKid])
    val statelessKid = system.actorOf(Props[StatelessFussyKid])
    val mom = system.actorOf(Props[Mom])

    mom ! MomStart(kid)
    mom ! MomStart(statelessKid)
    Thread.sleep(2000)

    system.stop(kid)
    system.stop(mom)
    system.terminate()
  }
}
