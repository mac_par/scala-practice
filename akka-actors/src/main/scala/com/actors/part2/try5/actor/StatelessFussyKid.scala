package com.actors.part2.try5.actor

import akka.actor.{Actor, ActorLogging}

class StatelessFussyKid extends Actor with ActorLogging {
  import Mom._
  import FussyKid._

  override def receive: Receive = happyReceive

  def happyReceive: Receive = {
    case Food(VEGETABLES) => context.become(sadReceive, discardOld = false)
    case Food(CHOCOLATE) => context.unbecome()
      log.info("Happy, nothing changes")
    case Ask(_) => sender() ! Accept
  }
  def sadReceive: Receive = {
    case Food(VEGETABLES) => context.become(sadReceive, discardOld = false)
      log.info("Sad, nothing changes")
    case Food(CHOCOLATE) => context.unbecome()
    case Ask(_) => sender() ! Reject
  }

}
