package com.actors.part2.try5.actor

import akka.actor.{Actor, ActorLogging}

class FussyKid extends Actor with ActorLogging {
  import FussyKid._
  import Mom._
  var state = HAPPY
  override def receive: Receive = {
    case Food(food) => if (food == VEGETABLES) state = SAD else state = HAPPY
    case Ask(_) => if (state == HAPPY) sender() ! Accept else sender() ! Reject
  }
}

object FussyKid {
  case object Accept
  case object Reject
  val HAPPY = "happy"
  val SAD = "sad"
}
