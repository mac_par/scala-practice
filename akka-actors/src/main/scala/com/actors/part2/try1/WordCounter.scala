package com.actors.part2.try1

import akka.actor.{Actor, ActorLogging, Props}

class WordCounter(private val owner: String) extends Actor with ActorLogging {
  var totalWords = 0

  override def receive: Receive = {
    case message: String => totalWords += message.split("\\W+").length
      log.info("Current count: {} words", totalWords)
    case 15L => log.info("Responding")
      sender() ! "matko makumber zachowaj"
    case cos => log.info(s"Unknown message: $cos")
  }

  def count: Int = totalWords
}

object WordCounter {
  def props(name: String): Props = Props(new WordCounter(name))
}
