package com.actors.part2.try1

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.pattern._
import akka.util.Timeout

import scala.util.{Failure, Success}

object ActorsIntro {

  def main(args: Array[String]): Unit = {
    val actorSystem = ActorSystem("My-first-actor-system")
    val wordCountActor = actorSystem.actorOf(WordCounter.props("Marco Polo"), "word-counter-1")

    implicit val timeout = Timeout(2, TimeUnit.SECONDS)
    implicit val dispatcher = actorSystem.dispatcher
    wordCountActor ! "matko boska makumbe zachowaj"
    wordCountActor ! 1
    wordCountActor ! "i am learning akka"
    wordCountActor ? 15L onComplete {
      case Success(value) => println(s"pobrano wartosc '$value'")
      case Failure(exception) => println("cos poszlo nie tak "+ exception.getMessage)
    }
//    wait(2500)
    actorSystem.stop(wordCountActor)
    actorSystem.terminate()
  }
}
