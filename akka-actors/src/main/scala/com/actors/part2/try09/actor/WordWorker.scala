package com.actors.part2.try09.actor

import akka.actor.{Actor, ActorLogging}

class WordWorker extends Actor with ActorLogging {
  import WordCounterMaster._
  override def receive: Receive = {
    case WordCountTask(actor, text) => sender() ! WordCountReply(actor, text.split("\\W+").length)
  }
}
