package com.actors.part2.try09.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Props}


class WordCounterMaster extends Actor with ActorLogging {

  import WordCounterMaster._

  override def receive: Receive = {
    case Initialize(children) => log.info("Creating {} workers", children)
      (0 until children).map { item => context.actorOf(Props[WordWorker], s"Worker$item") }
      context.become(wordProcessor(0, children))
    case _ => log.info("Unknown action")
  }

  def wordProcessor(round: Int, workers: Int): Receive = {
    case WordCountTask(actor, text) => val counter = round % workers
      log.info("Giving worker {} to count words in {}", counter, text)
      context.actorSelection(s"Worker$counter") ! WordCountTask(sender(),text)
      context.become(wordProcessor(round +1, workers))
    case WordCountReply(actor, words) => log.info("Word count {}", words)
      actor ! words
      context.become(wordProcessor(round, workers))
  }

  override def postStop(): Unit = context.children.foreach(context.stop)
}

object WordCounterMaster {

  case class Initialize(children: Int)

  case class WordCountTask(actor: ActorRef, text: String)

  case class WordCountReply(actor: ActorRef, words: Int)

}
