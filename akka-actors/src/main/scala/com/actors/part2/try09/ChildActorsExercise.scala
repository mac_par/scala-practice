package com.actors.part2.try09

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, Props}
import akka.pattern._
import akka.util.Timeout
import com.actors.part2.try09.actor.WordCounterMaster
import com.actors.part2.try09.actor.WordCounterMaster.{Initialize, WordCountTask}

import scala.util.{Failure, Success}

object ChildActorsExercise extends App {
  implicit val timeout = Timeout(2, TimeUnit.SECONDS)
  implicit val executor = scala.concurrent.ExecutionContext.global
  val system = ActorSystem("WordCounter")
  val master = system.actorOf(Props[WordCounterMaster], "master-counter")
  master ! Initialize(5)
  val list = List("Dawno temu w odlegej galaktyce, byla sobie fanta lite", "Tak albo nie, oto jest pytanie", "Buraka kuraka bum")
  val result = for {cos <- 1 to 10} yield master ? WordCountTask(master, list(cos % 3))

  Thread.sleep(2500)
  result foreach { item =>
    item onComplete {
      case Success(value) => println(s"Result $value")
      case Failure(exception) => println("sth went wrong")
    }
  }

  Thread.sleep(5000)
  system.terminate()
}
