package com.actors.part2.try4.event

case class Deposit(accountId: Long, amount: BigDecimal)
