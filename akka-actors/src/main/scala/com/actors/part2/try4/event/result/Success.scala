package com.actors.part2.try4.event.result

case class Success(accountId: Long, newBalance: BigDecimal)
