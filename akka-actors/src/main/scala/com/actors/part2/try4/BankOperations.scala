package com.actors.part2.try4

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.pattern._
import akka.util.Timeout
import com.actors.part2.try4.actor.{BankActor, BankTellerActor}
import com.actors.part2.try4.event._

import scala.util.{Failure, Success}

object BankOperations extends App {
  implicit val system = ActorSystem("BankSystem")
  implicit val timeout = Timeout(5, TimeUnit.SECONDS)
  implicit val dispatcher = system.dispatcher

  val bankActor = BankActor.getActor("Main-Branch")
  val tellers = (for {value <- 1 to 5} yield BankTellerActor.getActor(bankActor, s"Teller-$value")).toList

  val initialBalance = BigDecimal("1000.00")
  val list = scala.collection.mutable.ListBuffer[AccountResult]()
  for {round <- tellers.indices} yield tellers(round) ? InitAccount(initialBalance) onComplete {
    case Success(value) => list += value.asInstanceOf[AccountResult]
    case Failure(ex) => None
  }

  Thread.sleep(2500)
  println(s"Size : ${list.size}")
  val amount = BigDecimal("10.0")
  for {times <- 0 to 100} {
    val actor = tellers(times % tellers.size)
    val account = list(times % list.size)
    new Thread(() => {
      Thread.sleep(100)
      actor ? Deposit(account.accountId, amount) onComplete {
        case Success(value) => val inst = value.asInstanceOf[com.actors.part2.try4.event.result.Success]
          println(s"Deposit: Success - $inst")
        case Failure(value) => println(s"Deposit: Failure $account")
      }
      Thread.sleep(100)
    }).start()
    new Thread(() => {
      Thread.sleep(100)
      actor ? Widthdraw(account.accountId, amount) onComplete {
        case Success(value) => val inst = value.asInstanceOf[com.actors.part2.try4.event.result.Success]
          println(s"Widthdraw: Success - $inst")
        case Failure(value) => println(s"Widthdraw: Failure $account")
      }
      Thread.sleep(100)
    }).start()
  }

  Thread.sleep(7000)


  println("Results")
  for {idx <- list.indices} {
    val teller = tellers(idx)
    val account = list(idx)
    teller ? Statement(account.accountId) onComplete {
      case Success(value) => val inst = value.asInstanceOf[com.actors.part2.try4.event.result.Success]
        println(s"Statement: Success - $inst")
      case Failure(value) => println(s"Statement: Failure $account")
    }
  }

  Thread.sleep(5700)
  system.stop(bankActor)
  for {teller <- tellers} {
    system.stop(teller)
  }

  system.terminate()
}

