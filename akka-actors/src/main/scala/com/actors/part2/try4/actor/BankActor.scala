package com.actors.part2.try4.actor

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import com.actors.part2.try4.event.result.{Failure, Success}
import com.actors.part2.try4.event._

import scala.collection.mutable.{Map => Mapen}

class BankActor extends Actor with ActorLogging {
  val ZERO = BigDecimal("0.0")
  val accountsMap = Mapen[Long, BigDecimal]()
  var accountCounter = 0L;

  override def receive: Receive = {
    case InitAccount(balance) => log.info("initializing account with balance {}", balance)
      val responseTo = sender()
      accountCounter += 1
      accountsMap.put(accountCounter, balance)
      responseTo ! AccountResult(accountCounter)
    case Deposit(accountId, amount) => log.info("Receiving deposit request for account {} with amount {}", accountId, amount)
      val responseTo = sender()
      if (amount != null && amount.compare(ZERO) <= 0) {
        responseTo ! Failure("Balance Below Zero")
      } else {
        if (!accountsMap.contains(accountId)) responseTo ! Failure("Account does not exist")
        val newBalance = accountsMap(accountId) + amount
        accountsMap.put(accountId, newBalance)
        responseTo ! Success(accountId, newBalance)
      }

    case Widthdraw(accountId, amount) => log.info("An attempt to widthdraw from {} an amount of {}", accountId, amount)
      val responseTo = sender()
      if (accountsMap.contains(accountId)) {
        val currentBalance = accountsMap(accountId)
        if (currentBalance.compare(amount) < 0) responseTo ! Failure("Widthdraw cannot exceed current balance")
        val newBalance = currentBalance - amount
        accountsMap.put(accountId, newBalance)
        log.info("New Balance for account {}: {}", accountId, newBalance)
        responseTo ! Success(accountId, newBalance)
      } else {
        responseTo ! Failure("Account does not exists")
      }
    case Statement(accountId) => log.info("Getting current account state for accountId {}", accountId)
      val responseTo = sender()
      if (accountsMap.contains(accountId)) {
        val currentBalance = accountsMap(accountId)
        log.info("Current Balance for account {}: {}", accountId, currentBalance)
        responseTo ! Success(accountId, currentBalance)
      } else {
        responseTo ! Failure("Account does not exists")
      }
    case _ => log.info("Unknown action")
  }
}

object BankActor {
  def getActor(name: String)(implicit system: ActorSystem): ActorRef = system.actorOf(Props(classOf[BankActor]), name)
}
