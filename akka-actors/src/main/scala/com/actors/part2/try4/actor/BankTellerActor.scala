package com.actors.part2.try4.actor

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.pattern._
import akka.util.Timeout
import com.actors.part2.try4.event.{Deposit, InitAccount, Statement, Widthdraw}

import scala.util.{Failure, Success}

class BankTellerActor(val bankActor: ActorRef) extends Actor with ActorLogging {
  implicit val timeout = Timeout(2, TimeUnit.SECONDS)
  implicit val dispatcher = context.dispatcher

  override def receive: Receive = {
    case request: InitAccount => log.info("Creating Account")
      initAccount(sender(), request)

    case request: Deposit => log.info("An attempt to deposit amount of {} to account {}", request.amount, request.accountId)
      depositRequest(sender(), request)
    case request: Widthdraw => log.info("An attempt to widthdraw amount of {} from account {}", request.amount, request.accountId)
      widthdrawRequest(sender(), request)
    case request: Statement => log.info("An attempt to get current balance for account {}", request.accountId)
      checkBalance(sender(), request)
    case _ => log.info("Unknown action")
  }

  def initAccount(requester: ActorRef, request: InitAccount): Unit = {
    bankActor ? request onComplete {
      case Success(value) => requester ! value
      case Failure(exception) => requester ! exception
    }
  }

  def depositRequest(requester: ActorRef, request: Deposit): Unit = {
    bankActor ? request onComplete {
      case Success(value) => requester ! value
      case Failure(exception) => requester ! exception
    }
  }

  def widthdrawRequest(requester: ActorRef, request: Widthdraw): Unit = {
    bankActor ? request onComplete {
      case Success(value) => requester ! value
      case Failure(exception) => requester ! exception
    }
  }

  def checkBalance(requester: ActorRef, request: Statement): Unit = {
    bankActor ? request onComplete {
      case Success(value) => requester ! value
      case Failure(exception) => requester ! exception
    }
  }
}

object BankTellerActor {
  def getActor(bankActor: ActorRef, name: String)(implicit system: ActorSystem): ActorRef = system.actorOf(Props(new BankTellerActor(bankActor)), name)
}

