package com.actors.part2.try4.event.result

case class Failure(content: String)
