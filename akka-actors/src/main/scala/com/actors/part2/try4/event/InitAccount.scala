package com.actors.part2.try4.event

case class InitAccount(balance: BigDecimal)
