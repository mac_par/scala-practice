package com.actors.part2.try08.actor

import akka.actor.{ActorSystem, Props}
import com.actors.part2.try08.actor.Parent.{ChooseChild, CreateChild, TellChild}

object ChildrenPlayground extends App {
  val system = ActorSystem("Playground")
  val parent = system.actorOf(Props[Parent], "Parent")

  parent ! CreateChild("Gizmo")
  parent ! CreateChild("Bruno")
  parent ! TellChild("There is no place for you in hollywood;/")
  Thread.sleep(2500)
  parent ! ChooseChild("Bruno", "Bruno i am your parent")
  Thread.sleep(1000)
  println("Killing parent")
  system.stop(parent)
  system.terminate()
}
