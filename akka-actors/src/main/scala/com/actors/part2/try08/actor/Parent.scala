package com.actors.part2.try08.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

class Parent extends Actor with ActorLogging {

  import Parent._

  override def receive: Receive = {
    case CreateChild(name) =>
      log.info("{} creating child", self.path)
      val child = context.actorOf(Props[Child], name)
      context.become(receiveChildren(Set(child)))
    case _ => log.info("Unsupported action")
  }

  def receiveChildren(children: Set[ActorRef]): Receive = {
    case CreateChild(name) => log.info("{} creating child", self.path)
      val child = context.actorOf(Props[Child], name)
      context.become(receiveChildren(children + child))
    case TellChild(message) => log.info("Tell {} to children children of Akka")
      children.foreach(child => child ! message)
    case ChooseChild(name,message) => log.info("Choosing child {}, telling him {}", name, message)
      val selection = context.actorSelection(name)
      selection ! message

  }

  override def postStop(): Unit = {
    log.info("killing children")
    context.children.foreach(child => context.stop(child))
  }
}

object Parent {

  case class CreateChild(name: String)

  case class TellChild(message: String)
  case class ChooseChild(name: String, message: String)

}
