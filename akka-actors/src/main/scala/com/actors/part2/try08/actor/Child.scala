package com.actors.part2.try08.actor

import akka.actor.{Actor, ActorLogging}

class Child extends Actor with ActorLogging {
  override def receive: Receive = {
    case message: String => log.info("{} received {}", self.path, message)
    case _ => log.info("Unknown action")
  }
}
