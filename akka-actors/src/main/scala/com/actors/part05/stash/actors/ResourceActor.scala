package com.actors.part05.stash.actors

import akka.actor.{Actor, ActorLogging, Stash}
import com.actors.part05.stash.actions.{Close, Open, Read, Write}

//close -> open -> read/write -> close
//read/write actions are postponed
class ResourceActor extends Actor with ActorLogging with Stash {
  override def receive: Receive = closed(List.empty[String])

  def closed(list: List[String]): Receive = {
    case Open =>
      log.info("Opening")
      unstashAll()
      context.become(open(list))
    case message =>
      log.info("Message {} will be stashed", message)
      stash()
  }

  def open(list: List[String]): Receive = {
    case Close =>
      log.info("Closing a resource")
      unstashAll()
      context.become(closed(list))
    case Read => log.info("Reading: {}", list)
    case Write(msg) => log.info("Writing to resource")
      context.become(open(list :+ msg))
    case message => log.info("Un supported message: Stashing")
      stash()

  }
}
