package com.actors.part05.stash

import akka.actor.{ActorSystem, Props}
import com.actors.part05.stash.actors.ResourceActor
import com.actors.part05.stash.actions._

object StashingDemo extends App {
  val system = ActorSystem("stash-it")
  val actor = system.actorOf(Props[ResourceActor], "resource-actor")

  actor ! Read
  actor ! Open
  actor ! Open
  actor ! Write("hello")
  actor ! Close
  actor ! Read
  actor ! Close

  system.terminate()
}
