package com.actors.part05.routing

import akka.actor.{ActorSystem, Props}
import akka.routing.RoundRobinPool
import com.actors.part05.routing.actors.Slave

object PoolRouterRun extends App {
  val actorSystem = ActorSystem("PoolRouting")
  val masterPool = actorSystem.actorOf(RoundRobinPool(5).props(Props[Slave]), "master-pool")

  for {i <- 1 to 10} {
    masterPool ! s"Sending from world[$i]"
  }
  Thread.sleep(5000)
  actorSystem.terminate()
}
