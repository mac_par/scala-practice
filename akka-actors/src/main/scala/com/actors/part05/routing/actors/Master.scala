package com.actors.part05.routing.actors

import akka.actor.{Actor, Props, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}

/**
 * Manual routing
 */
class Master extends Actor {
  //1. define routees
  private val slaves = for {_ <- 1 to 5} yield {
    val slave = context.actorOf(Props[Slave])
    context.watch(slave)
    ActorRefRoutee(slave)
  }

  //2. define router
  private var router = Router(RoundRobinRoutingLogic(), slaves)

  override def receive: Receive = {
    //on terminated slave, just replace it
    case Terminated(ref) => router = router.removeRoutee(ref)
      val newSlave = context.actorOf(Props[Slave])
      context.watch(newSlave)
      router = router.addRoutee(newSlave)
    case message => router.route(message, sender()) //slave can directly respond to sender
  }
}
