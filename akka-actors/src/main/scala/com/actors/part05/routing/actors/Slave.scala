package com.actors.part05.routing.actors

import akka.actor.{Actor, ActorLogging}

class Slave extends Actor with ActorLogging {
  override def receive: Receive = {
    case msg => log.info(msg.toString)
  }
}
