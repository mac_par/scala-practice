package com.actors.part05.routing

import akka.actor.{ActorSystem, Props}
import com.actors.part05.routing.actors.Master

object RouterApp extends App {
  val actorSystem = ActorSystem("dieActorenSystem")
  val master = actorSystem.actorOf(Props[Master], "master-actor")

  for {bu <- 1 to 10} {
    master ! s"Hello there: $bu"
  }

  Thread.sleep(5000)

  actorSystem.terminate()
}
