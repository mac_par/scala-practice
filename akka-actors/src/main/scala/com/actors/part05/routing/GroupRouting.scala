package com.actors.part05.routing

import akka.actor.{ActorSystem, Props}
import akka.routing.{Broadcast, RoundRobinGroup}
import com.actors.part05.routing.actors.Slave
import com.typesafe.config.ConfigFactory

object GroupRouting extends App {
  val actorSystem = ActorSystem("gropSystem", ConfigFactory.load("application.conf").getConfig("routingDemo"))

  val slaves = for {i <- 1 to 5} yield {
    actorSystem.actorOf(Props[Slave], s"slave_$i")
  }
  val slavePaths = slaves.map{_.path.toString}
  val masterGroup = actorSystem.actorOf(RoundRobinGroup(slavePaths).props())

  for {i <- 1 to 10} {
    masterGroup ! s"Hello there $i"
  }
  masterGroup ! Broadcast("Witam was")

  Thread.sleep(5000)
  actorSystem.terminate()
}
