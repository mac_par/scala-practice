package com.actors.part05.routing

import akka.actor.{ActorSystem, Props}
import akka.routing.FromConfig
import com.actors.part05.routing.actors.Slave
import com.typesafe.config.ConfigFactory

object GroupMasterFromConfig extends App {
  val actorSystem = ActorSystem("actorenz", ConfigFactory.load("application.conf").getConfig("routingDemo"))
  val groupMaster = actorSystem.actorOf(FromConfig.props(Props[Slave]), "groupMaster")

  for {i <- 1 to 10} {
    groupMaster ! s"Hello there $i"
  }
  Thread.sleep(5000)
  actorSystem.terminate()
}
