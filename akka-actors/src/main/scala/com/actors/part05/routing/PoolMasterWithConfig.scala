package com.actors.part05.routing

import akka.actor.{ActorSystem, Props}
import akka.routing.FromConfig
import com.actors.part05.routing.actors.Slave
import com.typesafe.config.ConfigFactory

object PoolMasterWithConfig extends App {
  val actorSystem = ActorSystem("actorSystem", ConfigFactory.load("application.conf").getConfig("routingDemo"))
  val master = actorSystem.actorOf(FromConfig.props(Props[Slave]), "poolMaster")

  for {i <- 1 to 10} {
    master ! s"Hello there [$i]"
  }
  Thread.sleep(5000)
  actorSystem.terminate()
}
