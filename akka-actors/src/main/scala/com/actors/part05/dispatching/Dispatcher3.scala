package com.actors.part05.dispatching

import akka.actor.{ActorSystem, Props}
import com.actors.part05.dispatching.actors.DBActor

object Dispatcher3 extends App {
  val actorSystem = ActorSystem("dispatcher3")
  val actor = actorSystem.actorOf(Props[DBActor], "dieActoren")
  actor ! "die Seil ist kraft"
  Thread.sleep(7000)
  actorSystem.terminate()
}
