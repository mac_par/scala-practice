package com.actors.part05.dispatching.actors

import akka.actor.{Actor, ActorLogging}

class Counter extends Actor with ActorLogging {
  override def receive: Receive = countHandler()

  private def countHandler(counter: Int = 0): Receive = {
    case message => val newCounter = counter +1
      log.info(s"Message: $message - [$newCounter]")
      context.become(countHandler(newCounter))
  }
}
