package com.actors.part05.dispatching

import akka.actor.{ActorSystem, Props}
import com.actors.part05.dispatching.actors.Counter

object Dispatchers2 extends App {
  val actorSystem = ActorSystem("dieSystemem")

  val actor = actorSystem.actorOf(Props[Counter], "jvmpower")

  for {i <- 1 to 1000} {
    actor ! i
  }
  Thread.sleep(5000)
  actorSystem.terminate()
}
