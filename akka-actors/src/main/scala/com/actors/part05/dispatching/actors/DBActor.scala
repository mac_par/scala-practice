package com.actors.part05.dispatching.actors

import akka.actor.{Actor, ActorLogging}

import scala.concurrent.{ExecutionContext, Future}

class DBActor extends Actor with ActorLogging{
  private implicit val executionContext: ExecutionContext = context.dispatcher
  override def receive: Receive = {
    case message => Future {
      Thread.sleep(5000)
      log.info(s"Message: $message")
    }
  }
}
