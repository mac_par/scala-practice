package com.actors.part05.dispatching

import akka.actor.{ActorSystem, Props}
import com.actors.part05.dispatching.actors.Counter

import scala.util.Random
//programatic approach
object Dispatchers extends App {
  val actorSystem = ActorSystem("actorSystem") //, ConfigFactory.load("application.conf").getConfig("dispatchDemo"))
  val actors = for {i <- 1 to 10} yield actorSystem.actorOf(Props[Counter].withDispatcher("my-dispatcher"), s"counter_$i")

  val random = new Random()
  for {i <- 1 to 1000} {
    actors(random.nextInt(actors.size)) ! i
  }

  Thread.sleep(5000)
  actorSystem.terminate()
}
