package com.actors.part05.try01.actor

import akka.actor.{Actor, ActorLogging, Cancellable}
import com.actors.part05.try01.actor.WindowedActor.{Message, Timeout}

import scala.concurrent.duration._

class WindowedActor extends Actor with ActorLogging {

  override def receive: Receive = window(getHandler)

  def window(handler: Cancellable): Receive = {
    case Message(message) =>
      handler.cancel()
      log.info("Received: {}", message)
      context.become(window(getHandler), discardOld = true)
    case Timeout =>
      log.warning("Window slide passed")
      context.stop(self)
  }

  def getHandler: Cancellable = context.system.scheduler.scheduleOnce(1 second) {
    self ! Timeout
  }(context.system.dispatcher)
}

object WindowedActor {

  case class Message(message: String)

  case object Timeout

}
