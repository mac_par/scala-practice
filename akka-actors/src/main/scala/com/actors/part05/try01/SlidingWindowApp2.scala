package com.actors.part05.try01

import java.time.LocalDateTime

import akka.actor.{ActorSystem, Props}
import com.actors.part05.try01.actor.WindowedActor.Message
import com.actors.part05.try01.actor.WindowedActor2
import com.actors.part05.try01.actor.WindowedActor2.Stop

import scala.concurrent.duration._

object SlidingWindowApp2 extends App {
  val system = ActorSystem("sliding2")
  val actor = system.actorOf(Props[WindowedActor2], "windowedActor")

  Thread.sleep(8700)
  actor ! Stop
  Thread.sleep(4000)
  system.terminate()
}
