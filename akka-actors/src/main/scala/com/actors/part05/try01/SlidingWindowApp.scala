package com.actors.part05.try01

import java.time.LocalDateTime

import akka.actor.{ActorSystem, Props}
import com.actors.part05.try01.actor.WindowedActor
import com.actors.part05.try01.actor.WindowedActor.Message

import scala.concurrent.duration._

object SlidingWindowApp extends App {
  val system = ActorSystem("sliding")
  val actor = system.actorOf(Props[WindowedActor], "windowedActor")

  system.log.info("Starting...")

  val handler = system.scheduler.scheduleAtFixedRate(0 seconds, 700 millis) {
    () => actor ! Message(LocalDateTime.now().toString)
  }(system.dispatcher)

  Thread.sleep(8700)
  system.log.info("Cancelling")
  handler.cancel()

  Thread.sleep(8700)
  system.log.info("Stopping...")
  system.terminate()
}
