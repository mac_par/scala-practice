package com.actors.part05.try01

import akka.actor.{ActorSystem, Props}
import com.actors.part05.try01.actor.SimpleActor
import scala.concurrent.duration._

object Timers extends App {
  val system = ActorSystem("Schedulers")
  val actor = system.actorOf(Props[SimpleActor], "simpleActor")

  system.log.info("Scheduling")
  system.scheduler.scheduleOnce(1 second){
    actor ! SimpleActor.Message("miarka")
  }(system.dispatcher)

  Thread.sleep(15000)
  system.terminate()
}
