package com.actors.part05.try01.actor

import akka.actor.{Actor, ActorLogging, Timers}
import com.actors.part05.try01.actor.WindowedActor2.{Reminder, Start, Stop, TimerKey}

import scala.concurrent.duration._

class WindowedActor2 extends Actor with ActorLogging with Timers {
  timers.startSingleTimer(TimerKey, Start, 1 second)

  override def receive: Receive = {
    case Start =>
      log.info("standing periodical timer")
      timers.startTimerAtFixedRate(TimerKey, Reminder, 500 millis)
    case Reminder =>
      log.info("Hello there!...")
    case Stop =>
      log.info("Stopping all")
      timers.cancel(TimerKey)
      context.stop(self)
  }
}

object WindowedActor2 {

  case object TimerKey

  case object Start

  case object Reminder

  case object Stop

}


