package com.actors.part05.try01.actor

import akka.actor.{Actor, ActorLogging}
import com.actors.part05.try01.actor.SimpleActor.Message

class SimpleActor extends Actor with ActorLogging {
  override def receive: Receive = {
    case Message(message) => log.info("Received: {}", message)
  }
}

object SimpleActor {
  case class Message(message: String)
}
