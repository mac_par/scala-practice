package com.actors.part05.try01

import akka.actor.{ActorSystem, Cancellable, Props}
import com.actors.part05.try01.actor.SimpleActor
import com.actors.part05.try01.actor.SimpleActor.Message

import scala.concurrent.duration._
import scala.sys.Prop

object RepeatedActions extends App {
  val system = ActorSystem("repeated-system")
  val actor = system.actorOf(Props[SimpleActor], "costam")

  system.log.info("Jazda")
  val handler: Cancellable = system.scheduler.scheduleAtFixedRate(2 seconds, 2 seconds) (
    () => actor ! Message("kolejny")
  )(system.dispatcher)
  Thread.sleep(15000)
  handler.cancel()

  Thread.sleep(6000)
  system.terminate()
}
