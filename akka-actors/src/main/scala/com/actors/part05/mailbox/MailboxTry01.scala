package com.actors.part05.mailbox

import akka.actor.{ActorSystem, Props}
import com.actors.part05.mailbox.actors.SimpleActor01

object MailboxTry01 extends App {
  val system = ActorSystem("mailboxes")
  val actor = system.actorOf(Props[SimpleActor01].withDispatcher("my-next-try"))

  actor ! "[P3] nice to have"
  actor ! "[P2] you may forget"
  actor ! "[P0] do it now"
  actor ! "[P1] please do it"
  actor ! "sth not important"

  system.terminate()
}
