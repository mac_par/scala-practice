package com.actors.part05.mailbox.actors

import akka.actor.{Actor, ActorLogging}

class SimpleActor01 extends Actor with ActorLogging{
  override def receive: Receive = {
    case message => log.info(message.toString)
  }
}
