package com.actors.part05.mailbox.config

import akka.actor.ActorSystem
import akka.dispatch.{PriorityGenerator, UnboundedPriorityMailbox}
import com.typesafe.config.Config

class MyPrioritizedMailbox(settings: ActorSystem.Settings, config: Config) extends UnboundedPriorityMailbox(PriorityGenerator {
  case message: String if message.startsWith("[P0]") => 0
  case message: String if message.startsWith("[P1]") => 1
  case message: String if message.startsWith("[P2]") => 2
  case message: String if message.startsWith("[P3]") => 3
  case _ => 4
}){

}
