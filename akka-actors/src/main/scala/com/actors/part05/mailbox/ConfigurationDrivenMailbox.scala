package com.actors.part05.mailbox

import akka.actor.{ActorSystem, Props}
import com.actors.part05.mailbox.ControlAwareMailboxTest.actor
import com.actors.part05.mailbox.actions.ControlAwareMessage
import com.actors.part05.mailbox.actors.SimpleActor01
import com.typesafe.config.ConfigFactory

object ConfigurationDrivenMailbox extends App {
  val system = ActorSystem("dfsdf-dfsdf",ConfigFactory.load("application.conf").getConfig("mailboxesDemo"))
  val actor = system.actorOf(Props[SimpleActor01], "omatko")
  actor ! "[P3] nice to have"
  actor ! "[P2] you may forget"
  actor ! new ControlAwareMessage()
  actor ! "[P1] please do it"
  actor ! "sth not important"
  system.terminate()
}
