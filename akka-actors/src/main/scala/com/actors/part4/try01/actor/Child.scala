package com.actors.part4.try01.actor

import akka.actor.{Actor, ActorLogging}

class Child extends Actor with ActorLogging {
  override def receive: Receive = {
    case message => log.info("Received: {}", message)
  }

  override def preStart(): Unit = log.info("I started to exist {}", context.self.path)

  override def postStop(): Unit = log.info("Stopping {}", self.path)
}
