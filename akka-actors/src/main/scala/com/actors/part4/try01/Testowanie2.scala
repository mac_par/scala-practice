package com.actors.part4.try01

import akka.actor.{ActorSystem, PoisonPill, Props}
import com.actors.part4.try01.actor.CosTam
import com.actors.part4.try01.actor.Parent.StartChild

object Testowanie2 extends App {
  val system = ActorSystem("Testowanie2")
  val actor = system.actorOf(Props[CosTam], "domoslaw")
  actor ! StartChild("Marko-Polo")
  Thread.sleep(200)
  actor ! PoisonPill
  Thread.sleep(2500)
  system.terminate()
}
