package com.actors.part4.try01.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

class Parent extends Actor with ActorLogging {
  override def receive: Receive = withChildren(Map())

  import Parent._

  def withChildren(map: Map[String, ActorRef]): Receive = {
    case StartChild(name) => log.info(s"Creating child $name")
      context.become(withChildren(map + (name -> context.actorOf(Props[Child], name))))
    case StopChild(name) =>
      val child = map.get(name)
      child.foreach(childRef => {
        log.info("Stopping child {}", childRef.path)
        context.stop(childRef)

      })
    case Stop => log.info("Stoping myself {}", self.path)
      context.stop(self)
  }
}

object Parent {

  case class StartChild(name: String)

  case class StopChild(name: String)

  case object Stop

}
