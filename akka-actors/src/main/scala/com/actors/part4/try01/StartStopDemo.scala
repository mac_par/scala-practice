package com.actors.part4.try01

import akka.actor.{ActorSystem, Kill, PoisonPill, Props}
import com.actors.part4.try01.actor.{Child, Parent}
import com.actors.part4.try01.actor.Parent.{StartChild, Stop, StopChild}

object StartStopDemo extends App {
  val system = ActorSystem("startStopSystem")
  val parent = system.actorOf(Props[Parent], "parent")

  parent ! StartChild("child1")
  parent ! StartChild("child2")
  parent ! StartChild("child3")
  val child = system.actorSelection("/user/parent/child1")
  child ! "hi kid!"

  parent ! StopChild("child1")
  parent ! Stop
//  system.stop(parent)
  val child3 = system.actorOf(Props[Child], "childUnknown")
  val suicidicalChild = system.actorOf(Props[Child], "suicide")
  Thread.sleep(5000)
  child3 ! PoisonPill
  suicidicalChild ! Kill
  Thread.sleep(5000)
  system.terminate()
}
