package com.actors.part4.try01.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import com.actors.part4.try01.actor.Parent.StartChild

class CosTam extends Actor with ActorLogging {
  override def receive: Receive =  {
    case StartChild(name) => log.info(s"Creating child $name")
      val child = context.actorOf(Props[Child], name)
      context.watch(child)
    case Terminated(reference) =>
      log.info("Watched Actor {} was terminated", reference.path)
  }
}


