package com.actors.part4.try04.actor

import java.io.File

import akka.actor.{Actor, ActorLogging}

import scala.io.Source

class FBP2 extends Actor with ActorLogging {
  override def preStart(): Unit = {
    log.info("Sunrise....")
    val source = Source.fromFile(new File("pliczek.jakis"))
    log.info("Mam cię: {}", source.getLines().toList)
  }

  override def receive: Receive = {
    case FileBasePersistentActor.ReadFile => log.info("Hello Franky!")
  }

  override def postStop(): Unit = log.warning("Sunset......")
}
