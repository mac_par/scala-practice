package com.actors.part4.try04

import akka.actor.{ActorSystem, Props}
import akka.pattern.{BackoffOpts, BackoffSupervisor}
import com.actors.part4.try04.actor.FileBasePersistentActor
import scala.concurrent.duration._

object BackoffSupervisorPattern extends App {
    val system = ActorSystem("BackoffActor-system")
  val simpleBackoffPolicy = BackoffSupervisor.props(BackoffOpts.onFailure(Props[FileBasePersistentActor], "supervised-actor", 3 seconds, 30 seconds, 0.2))
  val actor = system.actorOf(simpleBackoffPolicy, "SimpleActor")
  actor ! FileBasePersistentActor.ReadFile

  Thread.sleep(14000)
  system.terminate()
}
