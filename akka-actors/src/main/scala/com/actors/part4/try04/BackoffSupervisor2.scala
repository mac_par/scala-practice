package com.actors.part4.try04

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{ActorSystem, OneForOneStrategy, Props}
import akka.pattern.{BackoffOpts, BackoffSupervisor}
import com.actors.part4.try04.actor.FileBasePersistentActor
import com.actors.part4.try04.actor.FileBasePersistentActor.ReadFile

import scala.concurrent.duration._

object BackoffSupervisor2 extends App {
  val system = ActorSystem("system")
  val stopSupervisorProps = BackoffSupervisor.props(BackoffOpts.onStop(Props[FileBasePersistentActor], "child", 3 seconds, 30 seconds, 0.2)
    .withSupervisorStrategy(OneForOneStrategy() {
      case _ => Stop
    }))

  val actor = system.actorOf(stopSupervisorProps, "supervisored")
  actor ! ReadFile

  Thread.sleep(15000)
  system.terminate()
}
