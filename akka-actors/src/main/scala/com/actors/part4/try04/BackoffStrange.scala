package com.actors.part4.try04

import akka.actor.SupervisorStrategy.{Restart, Stop}
import akka.actor.{ActorSystem, OneForOneStrategy, Props}
import akka.pattern.{BackoffOpts, BackoffSupervisor}
import com.actors.part4.try04.actor.FBP2

import scala.concurrent.duration._

object BackoffStrange extends App {
  val system = ActorSystem("system")
  val repeatedSupervisor = BackoffSupervisor.props(BackoffOpts.onStop(Props[FBP2], "repeated-child", 3 seconds, 30 seconds, 0.2))
  val actor = system.actorOf(repeatedSupervisor, "toEager")


  Thread.sleep(15000)
  system.terminate()
}
