package com.actors.part4.try04.actor

import java.io.File

import akka.actor.{Actor, ActorLogging}
import com.actors.part4.try04.actor.FileBasePersistentActor.ReadFile

import scala.io.Source

class FileBasePersistentActor extends Actor with ActorLogging {
  var dataSource: Source = null

  override def receive: Receive = {
    case ReadFile =>
      if (dataSource == null) {
        dataSource = Source.fromFile(new File("testfiles/nimportant.txt"))
        log.info("I've just read sth: " + dataSource.getLines().toList)
      }
  }

  override def preStart(): Unit = log.info("Persistent Actor has started")

  override def postStop(): Unit = log.warning("Persistent actor has stopped")

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = log.warning("Persistent actor restarting: reason: {}, message: {}", reason, message)

}

object FileBasePersistentActor {

  case object ReadFile

}
