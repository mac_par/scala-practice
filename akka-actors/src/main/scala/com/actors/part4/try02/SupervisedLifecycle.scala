package com.actors.part4.try02

import akka.actor.{ActorSystem, Props}
import com.actors.part4.try02.actor.Parent
import com.actors.part4.try02.message.{CheckChild, FailChild}

object SupervisedLifecycle extends App {
  val system = ActorSystem("systemen")
  val parent = system.actorOf(Props[Parent], "parent")
  parent ! CheckChild
  parent ! FailChild
  parent ! CheckChild
  parent ! CheckChild
  parent ! CheckChild

  Thread.sleep(250)
  system.stop(parent)
  system.terminate()
}
