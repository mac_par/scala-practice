package com.actors.part4.try02.actor

import akka.actor.{Actor, ActorLogging, Props}

class LifecycleActor extends Actor with ActorLogging {

  import com.actors.part4.try02.message._

  override def receive: Receive = {
    case StartChild => context.actorOf(Props[LifecycleActor], "child")
  }

  //at this stage an actor cannot process a thing
  override def preStart(): Unit = log.info("I am starting")

  //koniec instancji
  override def postStop(): Unit = log.info("I was stopped")
}
