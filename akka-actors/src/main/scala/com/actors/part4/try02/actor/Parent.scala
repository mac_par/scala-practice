package com.actors.part4.try02.actor

import akka.actor.{Actor, ActorLogging, Props}
import com.actors.part4.try02.message.{Check, CheckChild, Fail, FailChild}

class Parent extends Actor with ActorLogging {
  private val child = context.actorOf(Props[Child], "supervisedChild")

  override def receive: Receive = {
    case FailChild => log.info("Child will fail")
      child ! Fail
    case CheckChild => child ! Check
    case message => log.info("{} received {}", self.path, message.toString)
  }


  override def postStop(): Unit = super.postStop()

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = super.preRestart(reason, message)

  override def postRestart(reason: Throwable): Unit = super.postRestart(reason)
}
