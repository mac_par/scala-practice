package com.actors.part4.try02.actor

import akka.actor.{Actor, ActorLogging}
import com.actors.part4.try02.message.{Check, Fail}

class Child extends Actor with ActorLogging {
  override def receive: Receive = {
    case Fail => log.info("{} will throw an exception", self.path)
      throw new RuntimeException("Boom")
    case Check => log.info("{} alive and kicking", self.path)
  }

  override def preStart(): Unit = log.info("{} is starting", self.path)

  override def postStop(): Unit = log.info("{} was stopped", self.path)

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = log.info("{} restarting due to {}", self.path, reason.getMessage)

  override def postRestart(reason: Throwable): Unit = log.info("{} restarted", self.path)
}
