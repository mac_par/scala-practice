package com.actors.part4.try02

import akka.actor.{ActorSystem, PoisonPill, Props}
import com.actors.part4.try02.message.StartChild
import com.actors.part4.try02.actor.LifecycleActor

object LifecycleDisplay extends App {
  val system = ActorSystem("actorSystem")
  val parent = system.actorOf(Props[LifecycleActor], "parent")

  parent ! StartChild
  parent ! PoisonPill

  Thread.sleep(250)

  system.terminate()
}
