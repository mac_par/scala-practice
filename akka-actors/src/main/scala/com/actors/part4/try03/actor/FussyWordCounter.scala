package com.actors.part4.try03.actor

import akka.actor.{Actor, ActorLogging}
import com.actors.part4.try03.message.Report

class FussyWordCounter extends Actor with ActorLogging {
  var words = 0
  override def receive: Receive = {
    case Report => sender() ! words
    case message:String => if (message.isEmpty) throw new NullPointerException("Message is empty")
      if (message.length > 20) throw new RuntimeException("sentence is to long")
      else if (!Character.isUpperCase(message(0))) throw new IllegalArgumentException("Sentence must start with capital letter")
      else words += message.split("\\s+").length
    case _ => throw new Exception("Only strings are allowed")
  }
}
