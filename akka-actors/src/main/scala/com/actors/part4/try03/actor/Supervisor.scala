package com.actors.part4.try03.actor

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, AllForOneStrategy, Props, SupervisorStrategy}

class Supervisor extends Actor {
  override def receive: Receive = {
    case props: Props => val childRef = context.actorOf(props)
      sender() ! childRef
  }

  override def supervisorStrategy: SupervisorStrategy = AllForOneStrategy() {
    case _: NullPointerException => Restart
    case _: IllegalArgumentException => Stop
    case _: RuntimeException => Resume
    case _: Exception => Escalate
  }
}
