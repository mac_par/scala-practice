package com.actors.part06.messages

case class Initialize(inventory: Map[String, Int], prices: Map[String, Int])
