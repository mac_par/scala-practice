package com.actors.part06.messages

case class VendingError(reason: String)
