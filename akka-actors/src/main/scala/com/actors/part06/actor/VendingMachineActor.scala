package com.actors.part06.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable}
import com.actors.part06.messages._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

class VendingMachineActor extends Actor with ActorLogging {
  implicit val executionContext: ExecutionContext = context.dispatcher

  override def receive: Receive = idle

  private def idle: Receive = {
    case Initialize(inventory, prices) => log.info("Initializing")
      context.become(operational(inventory, prices))
    case _ => sender() ! VendingError("Machine not initialized")
  }

  private def operational(inventory: Map[String, Int], prices: Map[String, Int]): Receive = {
    case RequestProduct(product) => log.info("Requesting product: {}", product)
      val requester = sender()
      inventory.get(product) match {
        case Some(_) =>
          requester ! Instruction(s"Please insert: ${prices(product)} PLN")
          context.become(waitForMoney(inventory, prices, product, 0, startReceiveMoney, requester))
        case None | Some(0) => sender() ! VendingError("ProductNotAvailable")
      }
  }

  private def waitForMoney(inventory: Map[String, Int], prices: Map[String, Int], product: String, currentAmount: Int, moneyTimeout: Cancellable, requester: ActorRef): Receive = {
    case ReceiveMoney(amount) => moneyTimeout.cancel()
      val price = prices(product)
      val currentBalance = amount + currentAmount
      if (currentBalance >= price) {
        requester ! Deliver(product)
        if ((currentBalance - price) > 0) requester ! GiveBackChange(currentBalance - price)
        val newStock = inventory(product) - 1
        context.become(operational(inventory + (product -> newStock), prices))
      } else {
        val remainingMoney = price - currentBalance
        requester ! Instruction(s"Remains $remainingMoney PLN")
        context.become(waitForMoney(inventory, prices, product, currentBalance, startReceiveMoney, requester))
      }
    case ReceiveMoneyTimeout => requester ! VendingError("Timeout")
      if (currentAmount > 0) requester ! GiveBackChange(currentAmount)
      context.become(operational(inventory, prices))
  }

  private def startReceiveMoney = context.system.scheduler.scheduleOnce(3 second) {
    self ! ReceiveMoneyTimeout
  }
}
