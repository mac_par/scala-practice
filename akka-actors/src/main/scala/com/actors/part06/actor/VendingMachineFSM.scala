package com.actors.part06.actor

import akka.actor.{ActorRef, FSM}
import com.actors.part06.data.{InitializedData, UninitializedData, VendingData, WaitForMoneyData}
import com.actors.part06.messages._
import com.actors.part06.states.{IdleState, Operational, VendingState, WaitForMoney}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

class VendingMachineFSM extends FSM[VendingState, VendingData] {
  implicit val executionContext: ExecutionContext = context.dispatcher
  startWith(IdleState, UninitializedData)

  when(IdleState) {
    case Event(Initialize(inventory, prices), UninitializedData) => goto(Operational) using (InitializedData(inventory, prices))
    case _ => sender() ! VendingError("Machine not initialized")
      stay()
  }

  when(Operational) {
    case Event(RequestProduct(product), InitializedData(inventory, prices)) => log.info("Requesting product: {}", product)
      val requester = sender()
      inventory.get(product) match {
        case Some(_) =>
          requester ! Instruction(s"Please insert: ${prices(product)} PLN")
          goto(WaitForMoney) using WaitForMoneyData(inventory, prices, product, 0, requester)
        case None | Some(0) => sender() ! VendingError("ProductNotAvailable")
          stay()
      }
  }

  when(WaitForMoney, 3 seconds) {
    case Event(ReceiveMoney(amount), WaitForMoneyData(inventory: Map[String, Int], prices: Map[String, Int], product: String, currentAmount: Int,
    requester: ActorRef)) =>
      val price = prices(product)
      val currentBalance = amount + currentAmount
      if (currentBalance >= price) {
        requester ! Deliver(product)
        if ((currentBalance - price) > 0) requester ! GiveBackChange(currentBalance - price)
        val newStock = inventory(product) - 1
        goto(Operational) using InitializedData(inventory + (product -> newStock), prices)
      } else {
        val remainingMoney = price - currentBalance
        requester ! Instruction(s"Remains $remainingMoney PLN")
        stay() using WaitForMoneyData(inventory, prices, product, currentBalance, requester)
      }
    case Event(StateTimeout, WaitForMoneyData(inventory: Map[String, Int], prices: Map[String, Int], product: String, currentAmount: Int,
    requester: ActorRef)) => requester ! VendingError("Timeout")
      if (currentAmount > 0) requester ! GiveBackChange(currentAmount)
      goto(Operational) using InitializedData(inventory, prices)
  }

  whenUnhandled {
    case Event(_, _) => sender() ! VendingError("CommandUnknown")
      stay()
  }

  onTransition {
    case stateA -> stateB => log.info(s"Transition from $stateA into $stateB")
  }

  initialize()
}
