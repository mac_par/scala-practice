package com.actors.part06.data

case class InitializedData(inventory: Map[String, Int], prices: Map[String, Int]) extends VendingData
