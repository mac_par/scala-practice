package com.actors.part06.data

import akka.actor.ActorRef

case class WaitForMoneyData(inventory: Map[String, Int], prices: Map[String, Int], product: String, currentAmount: Int, requester: ActorRef)
  extends VendingData
