package com.actors.part06

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.actors.part06.actor.{VendingMachineActor, VendingMachineFSM}
import com.actors.part06.messages._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.duration._
import scala.language.postfixOps

class FSMSpec extends TestKit(ActorSystem("VendingMachine")) with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "A vending machine" should {
    "Return vendingError when not initialized" in {
      val actor = system.actorOf(Props[VendingMachineActor])
      actor ! RequestProduct("coke")
      expectMsg(VendingError("Machine not initialized"))
    }

    "inform about not available product" in {
      val actor = system.actorOf(Props[VendingMachineActor])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("cocaine")
      expectMsg(VendingError("ProductNotAvailable"))
    }

    "returns partial money on timeout" in {
      val actor = system.actorOf(Props[VendingMachineActor])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction(s"Please insert: 3 PLN"))
      actor ! ReceiveMoney(1)
      expectMsg(Instruction("Remains 2 PLN"))
      within(3.5 seconds) {
        expectMsg(VendingError("Timeout"))
        expectMsg(GiveBackChange(1))
      }
    }

    "returns product" in {
      val actor = system.actorOf(Props[VendingMachineActor])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction(s"Please insert: 3 PLN"))
      actor ! ReceiveMoney(3)
      expectMsg(Deliver("coke"))
    }

    "after inserting remaining money product and frata is returned" in {
      val actor = system.actorOf(Props[VendingMachineActor])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction("Please insert: 3 PLN"))
      actor ! ReceiveMoney(1)
      expectMsg(Instruction("Remains 2 PLN"))
      actor ! ReceiveMoney(4)
      expectMsg(Deliver("coke"))
      expectMsg(GiveBackChange(2))
    }

    "money were not received withing a sec - timeout" in {
      val actor = system.actorOf(Props[VendingMachineActor])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction("Please insert: 3 PLN"))
      within(3.5 seconds) {
        expectMsg(VendingError("Timeout"))
      }
    }
  }


  "A FSM vending machine" should {
    "Return vendingError when not initialized" in {
      val actor = system.actorOf(Props[VendingMachineFSM])
      actor ! RequestProduct("coke")
      expectMsg(VendingError("Machine not initialized"))
    }

    "inform about not available product" in {
      val actor = system.actorOf(Props[VendingMachineFSM])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("cocaine")
      expectMsg(VendingError("ProductNotAvailable"))
    }

    "returns partial money on timeout" in {
      val actor = system.actorOf(Props[VendingMachineFSM])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction(s"Please insert: 3 PLN"))
      actor ! ReceiveMoney(1)
      expectMsg(Instruction("Remains 2 PLN"))
      within(3.5 seconds) {
        expectMsg(VendingError("Timeout"))
        expectMsg(GiveBackChange(1))
      }
    }

    "returns product" in {
      val actor = system.actorOf(Props[VendingMachineFSM])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction(s"Please insert: 3 PLN"))
      actor ! ReceiveMoney(3)
      expectMsg(Deliver("coke"))
    }

    "after inserting remaining money product and frata is returned" in {
      val actor = system.actorOf(Props[VendingMachineFSM])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction("Please insert: 3 PLN"))
      actor ! ReceiveMoney(1)
      expectMsg(Instruction("Remains 2 PLN"))
      actor ! ReceiveMoney(4)
      expectMsg(Deliver("coke"))
      expectMsg(GiveBackChange(2))
    }

    "money were not received withing a sec - timeout" in {
      val actor = system.actorOf(Props[VendingMachineFSM])
      actor ! Initialize(Map(("coke" -> 15)), Map(("coke" -> 3)))
      actor ! RequestProduct("coke")
      expectMsg(Instruction("Please insert: 3 PLN"))
      within(3.5 seconds) {
        expectMsg(VendingError("Timeout"))
      }
    }
  }
}
