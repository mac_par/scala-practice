package com.actors.part3.try02

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

class TestProbeSpec extends TestKit(ActorSystem("TestKit")) with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  import com.actors.part3.try02.actor._
  import com.actors.part3.try02.message._

  "A master actor" should {
    "register a value" in {
      val master = system.actorOf(Props[Master], "Master")
      val slave = TestProbe("slave")
      master ! Register(slave.ref)
      expectMsg(RegistrationAck)
      system.stop(master)
    }

    "send SlaveWork with payload" in {
      val master = system.actorOf(Props[Master], "Master")
      val slave = TestProbe("slave")
      master ! Register(slave.ref)
      expectMsg(RegistrationAck)
      val text = "Jakis tam tekst, cos tam dalej"
      master ! Work(text)

      slave.expectMsg(SlaveWork(text, testActor))
      slave.reply(WorkCompleted(6, testActor))
      expectMsg(Report(6))
      system.stop(master)
    }

    "aggregate results correctly" in {
      val master = system.actorOf(Props[Master], "Master")
      val slave = TestProbe("slave")
      master ! Register(slave.ref)
      expectMsg(RegistrationAck)
      val text = "cos tam cos tam"
      master ! Work(text)
      master ! Work(text)
      slave.receiveWhile(){
        case SlaveWork(`text`, `testActor`) => slave.reply(WorkCompleted(4, testActor))
      }
      expectMsg(Report(4))
      expectMsg(Report(8))
      system.stop(master)
    }
  }
}

object TestProbeSpec {

}
