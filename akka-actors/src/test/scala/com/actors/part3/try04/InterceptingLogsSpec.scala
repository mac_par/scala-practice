package com.actors.part3.try04

import akka.actor.{ActorSystem, Props}
import akka.testkit.{EventFilter, ImplicitSender, TestKit}
import com.actors.part3.try04.actor.CheckoutActor
import com.actors.part3.try04.message.Checkout
import com.typesafe.config.ConfigFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

class InterceptingLogsSpec extends TestKit(ActorSystem("Intercepting-Logs-Spec", ConfigFactory.load("configen.json").getConfig("interceptor")))
  with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "A checkout flow" should {
    val checkoutRef = system.actorOf(Props[CheckoutActor])
    "correctly logout the dispatch of an order" in {
      val item = "Rock da Casbah"
      val creditCard = "123-456-789"
      EventFilter.info(pattern = s"Order [0-9]+ for item $item has been dispatched", occurrences = 1).intercept {
        checkoutRef ! Checkout(item, creditCard)
      }
    }
    "freak out on denied payment" in {
      val item = "Rock da Casbah"
      val creditCard = "023-456-789"
      EventFilter[RuntimeException](occurrences = 1, message = "I cannot handle it!") intercept {
        checkoutRef ! Checkout(item, creditCard)
      }
    }
  }
}
