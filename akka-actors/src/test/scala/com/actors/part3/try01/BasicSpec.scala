package com.actors.part3.try01

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.actors.part3.try01.actor.{BlackHoleActor, LabTestActor, SimpleBasicActor}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

class BasicSpec extends TestKit(ActorSystem("BasicSpecSystem")) with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Simple Actor" should {
    "should send back the message" in {
      val actor = system.actorOf(Props[SimpleBasicActor], "SimpleActor")
      val message = "Hello test"
      actor ! message
      expectMsg(message)
    }
  }

  "A Black Hole Actor" should {
    "send back message" in {
      val blackHole = system.actorOf(Props[BlackHoleActor], "BlackHoleActor")
      val message = "got it"
      blackHole ! message
      expectNoMessage()
    }
  }

  "LabTestActor" should {
    val labTestActor = system.actorOf(Props[LabTestActor], "LabTestActor")
    "change string into upper string" in {
      val message = "i hate myself"
      labTestActor ! message
      val reply = expectMsgType[String]
      assert(reply == message.toUpperCase)
    }

    "for greeting returns random message" in {
      val message = "greeting"
      labTestActor ! message
      expectMsgAnyOf("Hi!", "Hello")
    }

    "reply with favorite tech" in {
      labTestActor ! "favoriteTech"
      expectMsgAllOf("Scala", "Akka")
    }

    "reply with favorite tech in other wat" in {
      labTestActor ! "favoriteTech"
      val messages = receiveN(2)
      expectMsgAllOf("Scala", "Akka")
    }

    "reply in fancier way" in {
      labTestActor ! "favoriteTech"
      expectMsgPF(){
        case "Scala" =>
        case "Akka" =>
      }
    }
  }
}

object BasicSpec {

}
