package com.actors.part3.tr

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{CallingThreadDispatcher, TestActorRef, TestProbe}
import com.actors.part3.try05.actor.Counter
import com.actors.part3.try05.message.{Inc, Read}
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

class SynchroniousSpec extends AnyWordSpecLike with BeforeAndAfterAll with BeforeAndAfterEach {
  implicit val system = ActorSystem("System")


  override protected def afterAll(): Unit = system.terminate()

  "A counter" should {
    "synchronously increas its counter" in {
      val counter = TestActorRef[Counter](Props[Counter])
      counter ! Inc
      counter.receive(1)
    }

    "calling on the calling thread dispatcher" in {
      val counter = system.actorOf(Props[Counter].withDispatcher(CallingThreadDispatcher.Id))
      val probe = TestProbe()
      probe.send(counter, Read)
      probe.expectMsg(0)
    }
  }
}
