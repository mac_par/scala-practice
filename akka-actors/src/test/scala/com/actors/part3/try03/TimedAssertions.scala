package com.actors.part3.try03

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.actors.part3.try03.actor.WorkerActor
import com.actors.part3.try03.message._
import com.typesafe.config.ConfigFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

import scala.concurrent.duration._

class TimedAssertions extends TestKit(ActorSystem("TimedAssertions", ConfigFactory.load("configen.json").getConfig("custom-config")))
  with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  "WorkerActor" should {
    val workerActor = system.actorOf(Props[WorkerActor], "testWorkerActor")

    "reply in timely manner" in {
      within(500 millis, 1 second) {
        workerActor ! LongWork
        expectMsg(WorkResult(42))
      }
    }

    "reply work at reasonable cadence" in {
      within(1 second) {
        workerActor ! ShortWork

        val results = receiveWhile[Int](max = 2 seconds, idle = 500 millis, messages = 10) {
          case WorkResult(result) => result
        }

        assert(results.sum >= 10)
      }
    }

    "reply to a test prove in a timely manner" in {
      within(500 millis, 1 second) {
      val probe = TestProbe("testProben")
      probe.send(workerActor, LongWork)
      probe.expectMsg(WorkResult(42))
      }
    }
  }
}
