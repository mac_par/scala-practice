package com.actors.part04.messages.bank

case class PasswordChangeRequest(accountNumber: String, password: String, newPasswd: String)
