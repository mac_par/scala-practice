package com.actors.part04.messages.bank

case class WithdrawalRequest(accountNumber: String, password: String, amount: Double)
