package com.actors.part04.messages.bank

case class DepositRequest(accountNumber: String, password: String, amount: Double)