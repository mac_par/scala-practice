package com.actors.part04.messages

case class AuthenticateUser(username: String, password: String)
