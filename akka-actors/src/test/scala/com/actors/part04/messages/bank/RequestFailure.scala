package com.actors.part04.messages.bank

case class RequestFailure(reason: String)
