package com.actors.part04.messages.bank

case class CheckBalanceRequest(accountNumber: String, password: String)
