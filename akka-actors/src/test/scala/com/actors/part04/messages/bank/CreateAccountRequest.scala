package com.actors.part04.messages.bank

case class CreateAccountRequest(accountNumber: String, password:String, initialBalance: Double = 0)
