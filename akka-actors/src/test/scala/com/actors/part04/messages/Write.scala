package com.actors.part04.messages

case class Write(key: String, value: String)
