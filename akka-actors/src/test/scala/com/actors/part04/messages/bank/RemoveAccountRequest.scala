package com.actors.part04.messages.bank

case class RemoveAccountRequest(accountNumber: String, password: String)
