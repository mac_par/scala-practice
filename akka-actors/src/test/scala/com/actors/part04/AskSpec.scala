package com.actors.part04

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.actors.part04.actors.AuthManager
import com.actors.part04.messages.{AuthFailure, AuthenticateUser, RegisterUser, Write}
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

class AskSpec extends TestKit(ActorSystem("Asking")) with ImplicitSender with AnyWordSpecLike with BeforeAndAfterAll {
  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)

  import AskSpec._
  import AuthManager._
  "authenticator" should {

    "fail on unregistered user" in {
      val actor = system.actorOf(Props[AuthManager])
      actor ! AuthenticateUser("non-existent", "password")
      expectMsg(AuthFailure(AUTH_FAILURE))
    }

    "fail on registered user but wrong password" in {
      val actor = system.actorOf(Props[AuthManager])
      val username= "test-user"
      val password = "test-password"
      actor ! RegisterUser(username, password)
      actor ! AuthenticateUser(username, "gowno-nie-haslo")

      expectMsg(AuthFailure(s"User $username does not exist"))
    }
  }
 }

object AskSpec {

}
