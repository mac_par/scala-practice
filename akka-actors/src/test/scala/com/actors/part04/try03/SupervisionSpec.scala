package com.actors.part04.try03

import akka.actor.{ActorRef, ActorSystem, Props, Terminated}
import akka.testkit.{EventFilter, ImplicitSender, TestKit}
import com.actors.part4.try03.actor.{FussyWordCounter, Supervisor}
import com.actors.part4.try03.message.Report
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpecLike

class SupervisionSpec extends TestKit(ActorSystem("Jakis-spec")) with AnyWordSpecLike with ImplicitSender with BeforeAndAfterAll {

  "A Supervisor" should {
    "resume it's child in a minor fail" in {
      val supervisor = system.actorOf(Props[Supervisor])
      supervisor ! Props[FussyWordCounter]
      val child = expectMsgType[ActorRef]

      child ! "I love Akka"
      child ! Report
      expectMsg(3)

      child ! "dfsfdfdsfsdfs dsdf d dsfd dfsdfsdfsd sdfdf dfsf dfsdf dfsdf fsdfdf dfs d"
      child ! Report
      expectMsg(3)
    }

    "A child on medium fail " should {
      "restart with kept state" in {
        val supervisor = system.actorOf(Props[Supervisor])
        supervisor ! Props[FussyWordCounter]
        val child = expectMsgType[ActorRef]
        child ! "Ale jaja tu się"
        child ! Report
        expectMsg(4)

        child ! ""
        child ! Report
        expectMsg(0)
      }
    }

    "A supervisor " should {
      "stop child in case of major error" in {
        val supervisor = system.actorOf(Props[Supervisor])
        supervisor ! Props[FussyWordCounter]
        val child = expectMsgType[ActorRef]

        watch(child)
        child ! "akka is nice"
        val terminatedMessage = expectMsgType[Terminated]
        assert(terminatedMessage.actor == child)
      }
    }

    "A supervisor" should {
      "escalate the failure in case of not knowing what to do" in {
        val supervisor = system.actorOf(Props[Supervisor])
        supervisor ! Props[FussyWordCounter]
        val child = expectMsgType[ActorRef]
        supervisor ! Props[FussyWordCounter]
        val sndChild = expectMsgType[ActorRef]

        child ! "A b c"
        child ! Report
        expectMsg(3)

        EventFilter[NullPointerException]() intercept{
          sndChild ! ""
        }

        Thread.sleep(200)
        child ! Report
        expectMsg(0)
      }
    }
  }

  override protected def afterAll(): Unit = TestKit.shutdownActorSystem(system)
}
