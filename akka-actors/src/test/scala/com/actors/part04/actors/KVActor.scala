package com.actors.part04.actors

import akka.actor.{Actor, ActorLogging}
import com.actors.part04.messages.{Read, Write}

class KVActor extends Actor with ActorLogging {
  override def receive: Receive = online(Map())

  def online(kv: Map[String, String]): Receive = {
    case Read(key) => log.info(s"Reading: $key")
      log.info(s"Values: ${kv}")
      sender() ! kv.get(key)
    case Write(key, value) => log.info("Writing [{}, {}]", key, value)
      val newMap = kv + (key -> value)
      log.info("Mapen {}", newMap)
      context.become(online(kv + (key -> value)))
  }
}
