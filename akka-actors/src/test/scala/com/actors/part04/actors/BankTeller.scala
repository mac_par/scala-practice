package com.actors.part04.actors

import akka.actor.{Actor, ActorLogging}
import com.actors.part04.messages.bank._

class BankTeller extends Actor with ActorLogging {

  import BankTeller._

  private val map = Map.empty[(String, String), Double]

  override def receive: Receive = handler()

  private def handler(implicit accounts: Map[String, Account] = Map.empty[String, Account]): Receive = {
    case CreateAccountRequest(number, password, balance) => createAccount(number, password, balance)
    case CheckBalanceRequest(accountNumber, password) => checkBalance(accountNumber, password)
    //TODO: add remaining messages
  }

  private def createAccount(accountNumber: String, password: String, balance: Double)(implicit accounts: Map[String, Account]): Unit = {
    log.info("creating account for {} with balance of {}", accountNumber, balance)
    val requestOrigin = sender()
    if (accounts.contains(accountNumber)) {
      log.error("Create: Account exists")
      requestOrigin ! RequestFailure(ACCOUNT_EXISTS)
    } else {
      log.info("CreateAccount: Success for Account Number: {}", accountNumber)
      requestOrigin ! RequestSuccess
      context.become(handler(accounts + (accountNumber -> Account(accountNumber, password, balance))))
    }
  }

  private def checkBalance(accountNumber: String, password: String)(implicit accounts: Map[String, Account]): Unit = {
    log.info("Checking balance for account: {}", accountNumber)
    val requestOrigin = sender()
    if (accounts.contains(accountNumber)) {
      accounts(accountNumber).accessAccount(password) match {
        case Some(account) => requestOrigin ! Some(account.balance)
        case None => requestOrigin ! RequestFailure(PASSWORD_MISMATCH)
      }
    } else {
      log.error("Account does not exist")
      requestOrigin ! RequestFailure(ACCOUNT_DOES_NOT_EXIST)
    }
  }
}

object BankTeller {
  val ACCOUNT_EXISTS = "Account already exists"
  val PASSWORD_MISMATCH = "Password does not match"
  val ACCOUNT_DOES_NOT_EXIST = "Account does not exist"

  case class Account(number: String, val password: String, balance: Double) {
    @throws(clazz = classOf[IllegalArgumentException])
    def withdraw(amount: Double): Account = {
      if (balance >= amount) {
        Account(number, password, balance - amount)
      } else {
        throw new IllegalArgumentException("Unsufficient funds!")
      }
    }

    def deposit(amount: Double): Account = {
      Account(number, password, balance + amount)
    }

    @throws(clazz = classOf[IllegalArgumentException])
    def changePassword(newPassword: String): Account = {
      require(newPassword == null || newPassword.trim.isEmpty, "Password must be provided")
      Account(number, newPassword, balance)
    }

    def accessAccount(passwd: String): Option[Account] = {
      if (passwd == password) Some(this) else None
    }
  }

}
