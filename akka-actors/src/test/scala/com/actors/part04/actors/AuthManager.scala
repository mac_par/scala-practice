package com.actors.part04.actors

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import akka.pattern.pipe
import com.actors.part04.messages.{Read, Write, AuthenticateUser, RegisterUser, AuthFailure, AuthSuccess}

class AuthManager extends Actor with ActorLogging {

  import AuthManager._

  private implicit val executionContext: ExecutionContext = context.dispatcher
  private implicit val timeout: Timeout = Timeout(10, TimeUnit.SECONDS)

  private val authDB: ActorRef = context.actorOf(Props[KVActor], "authDB-Actor")

  override def receive: Receive = {
    case AuthenticateUser(username, password) => authenticateUser(username, password)
    case RegisterUser(username, password) => log.info(s"Registering user: $username")
      authDB ! Write(username, password)
  }

  private def authenticateUser(username:String, password: String): Unit = {
    log.info(s"Trying authenticate user $username")
    val originSender: ActorRef = sender()
    val future = authDB ? Read(username)
    val response = future.mapTo[Option[String]].map{
      case None => AuthFailure(AUTH_FAILURE)
      case Some(dbPassword) => if (dbPassword == password) AuthSuccess else AuthFailure(s"User $username does not exist")
    }
    response.pipeTo(originSender)
  }
}

object AuthManager {
  val AUTH_FAILURE = "System failure"
  val WRONG_PASSWORD = "Wrong password"
}
