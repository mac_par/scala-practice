package com.functional.chpt05.stream

import org.scalatest.funspec.AnyFunSpec

class StreamTest extends AnyFunSpec {
  def checkStreams[A](s1: => Stream[A], s2: => Stream[A]): Boolean = (s1, s2) match {
    case (Empty, Empty) => true
    case (Cons(h1, t1), Cons(h2, t2)) if h1() == h2() => checkStreams(t1(), t2())
    case (Cons(h1, t1), Empty) => false
    case (Empty, Cons(h2, t2)) => false
  }

  def checkStreamStreams[A](s1: => Stream[Stream[A]], s2: => Stream[Stream[A]]): Boolean = (s1, s2) match {
    case (Empty, Empty) => true
    case (Cons(h1, t1), Cons(h2, t2)) if checkStreams(h1(), h2()) => checkStreamStreams(t1(), t2())
    case (Cons(h1, t1), Empty) => false
    case (Empty, Cons(h2, t2)) => false
  }

  describe("toList") {
    it("empty stream returns an empty list") {
      val stream = Stream.empty[Int]
      val expected = Nil
      assert(expected == stream.toList)
    }

    it("populated stream returns a listsum") {
      val stream = Stream.apply(1, 2, 3)
      val expected = List(1, 2, 3)
      assert(expected == stream.toList)
    }
  }

  describe("take") {
    it("for empty returns an empty stream") {
      val items = 5
      val stream = Stream.empty[Int]
      val expected = Stream.empty[Int]
      assert(checkStreams(expected, stream.take(items)))
    }

    it("for populated but with less items, all are retrieved") {
      val stream = Stream(1, 2, 3)
      val items = 5
      val expected = Stream(1, 2, 3)
      assert(checkStreams(expected, stream.take(items)))
    }

    it("for populated but more items than required, subset is retrieved") {
      val stream = Stream((0 to 10).toArray: _*)
      val items = 5
      val expected = Stream((0 until items).toArray: _*)
      val value = stream.take(items)
      assert(checkStreams(expected, value))
    }

  }

  describe("drop") {
    it("for empty returns an empty stream") {
      val items = 5
      val stream = Stream.empty[Int]
      val expected = Stream.empty[Int]
      assert(checkStreams(expected, stream.drop(items)))
    }

    it("for populated but with amount equal to size, the empty stream is returned") {
      val stream = Stream(1, 2, 3)
      val items = 3
      val expected = Stream.empty
      assert(checkStreams(expected, stream.drop(items)))
    }

    it("for populated but more items than required, remaining subset is retrieved") {
      val stream = Stream((0 to 10).toArray: _*)
      val items = 5
      val expected = Stream((items to 10).toArray: _*)
      assert(checkStreams(expected, stream.drop(items)))
    }
  }

  describe("takeWhile") {
    it("for empty returns an empty stream") {
      val pred = (i: Int) => i < 5
      val stream = Stream.empty[Int]
      val expected = Stream.empty[Int]
      assert(checkStreams(expected, stream.takeWhile(pred)))
    }

    it("for populated but not matching the predicate, the empty stream is returned") {
      val stream = Stream(1, 2, 3)
      val pred = (i: Int) => i > 5
      val expected = Stream.empty
      assert(checkStreams(expected, stream.takeWhile(pred)))
    }

    it("for populated but subset of items meet the predicate, remaining subset is retrieved") {
      val stream = Stream((0 to 10).toArray: _*)
      val items = 5
      val pred = (i: Int) => i < items
      val expected = Stream((0 until items).toArray: _*)
      assert(checkStreams(expected, stream.takeWhile(pred)))
    }

  }

  describe("forAll") {
    it("for empty stream, true is returned") {
      val stream = Stream.empty[Int]
      assert(stream.forAll(_ > 0))
    }

    it("for few initial elements meeting predicate, false is returned") {
      val stream = Stream(1, 2, 3, 4, 5, 6, 7, 8)
      assert(!stream.forAll(_ < 5))
    }

    it("for all elements meeting the requirement, true is returned") {
      val stream = Stream(1, 2, 3, 4, 5, 6, 7, 8)
      assert(stream.forAll(_ < 9))
    }
  }

  describe("flatMap") {
    it("empty stream returns empty stream") {
      val stream = Stream.empty[Int]
      val trans = (i: Int) => Stream(i.toString)
      val expected = Stream.empty[String]
      assert(checkStreams(expected, stream.flatMap(trans)))
    }

    it("populated stream returns transformed stream") {
      val stream = Stream((1 to 10).toArray: _*)
      val trans = (i: Int) => Stream(i.toString)
      val expected = Stream((1 to 10).map {
        _.toString
      }.toArray: _*)
      assert(checkStreams(expected, stream.flatMap(trans)))
    }
  }

  describe("filter") {
    it("for empty stream and empty stream is returned") {
      val stream = Stream.empty[Int]
      val expected = stream
      val pred = (i: Int) => i % 2 == 0
      assert(checkStreams(expected, stream.filter(pred)))
    }

    it("for populated stream and empty stream is returned when predicate is not met") {
      val stream = Stream((1 to 10).toArray: _*)
      val expected = Stream.empty[Int]
      val pred = (i: Int) => i > 10
      assert(checkStreams(expected, stream.filter(pred)))
    }

    it("for populated stream a populated stream is returned with elements meeting condition") {
      val stream = Stream((1 to 10).toArray: _*)
      val pred = (i: Int) => i % 2 == 0
      val expected = Stream((1 to 10).filter(pred).toArray: _*)
      assert(checkStreams(expected, stream.filter(pred)))
    }
  }

  describe("append") {
    it("for empty stream, appended stream is being returned") {
      val stream = Stream.empty[Int]
      val appendStream = Stream(4, 5, 6)
      val expected = appendStream
      assert(checkStreams(expected, stream.append(appendStream)))
    }

    it("for populated stream and empty appender, an original stream is returned") {
      val stream = Stream(1, 2, 3)
      val appendStream = Stream.empty[Int]
      val expected = stream
      assert(checkStreams(expected, stream.append(appendStream)))
    }

    it("for populated streams a new stream is returned") {
      val stream = Stream(1, 2, 3)
      val appendStream = Stream(4, 5, 6)
      val expected = Stream((1 to 6).toArray: _*)
      assert(checkStreams(expected, stream.append(appendStream)))
    }
  }


  describe("zipWith") {
    it("empty with empty returns empty stream") {
      val empty1 = Stream.empty[Int]
      val empty2 = Stream.empty[Int]
      val expected = Stream.empty[Int]
      assert(checkStreams(expected, empty1.zipWith(empty2) {
        _ + _
      }))
    }

    it("populated with empty returns empty stream") {
      val empty1 = Stream.from(0).take(5)
      val empty2 = Stream.empty[Int]
      val expected = Stream.empty[Int]
      assert(checkStreams(expected, empty1.zipWith(empty2) {
        _ + _
      }))
    }

    it("populated with populated returns stream up to amount of second stream") {
      val empty1 = Stream.from(0).take(5)
      val empty2 = Stream.from(1).take(3)
      val expected = Stream(1, 3, 5)
      assert(checkStreams(expected, empty1.zipWith(empty2) {
        _ + _
      }))
    }
  }
  describe("zipWithAll") {

    val fn = (opt1: Option[Int], opt2: Option[Int]) => (opt1, opt2) match {
      case (Some(value1), Some(value2)) => value1 + value2
      case (Some(value), None) => value
      case (None, Some(value)) => value
    }

    it("empty with empty returns empty stream") {
      val empty1 = Stream.empty[Int]
      val empty2 = Stream.empty[Int]
      val expected = Stream.empty[Int]
      assert(checkStreams(expected, empty1.zipWithAll(empty2) {
        fn
      }))
    }

    it("populated with empty returns first stream") {
      val empty1 = Stream.from(0).take(5)
      val empty2 = Stream.empty[Int]
      val expected = Stream.from(0).take(5)
      assert(checkStreams(expected, empty1.zipWithAll(empty2) {
        fn
      }))
    }

    it("empty with populated returns populated stream") {
      val empty1 = Stream.empty[Int]
      val empty2 = Stream.from(0).take(5)
      val expected = Stream.from(0).take(5)
      assert(checkStreams(expected, empty1.zipWithAll(empty2) {
        fn
      }))
    }

    it("populated with populated returns stream up to amount of second stream") {
      val empty1 = Stream.from(0).take(5) //0, 1, 2, 3, 4
      val empty2 = Stream.from(1).take(3) //1, 2, 3
      val expected = Stream(1, 3, 5, 3, 4)
      val result = empty1.zipWithAll(empty2) {
        fn
      }
      assert(checkStreams(expected, result))
    }
    it("populated with populated of equal size returns new stream") {
      val empty1 = Stream.from(0).take(5) //0, 1, 2, 3, 4
      val empty2 = Stream.from(1).take(5) //1, 2, 3, 4, 5
      val expected = Stream(1, 3, 5, 7, 9)
      assert(checkStreams(expected, empty1.zipWithAll(empty2) {
        fn
      }))
    }
  }

  describe("startsWith") {
    it("empty list returns false") {
      val stream1 = Stream.empty[Int]
      val stream2 = Stream.from(1).take(2)
      assert(!stream1.startsWith(stream2))
    }
    it("second stream is not included, return false") {
      val stream1 = Stream.from(1).take(5)
      val stream2 = Stream.from(3).take(2)
      assert(!stream1.startsWith(stream2))
    }
    it("second stream is included, then return true") {
      val stream1 = Stream.from(1).take(5)
      val stream2 = Stream.from(1).take(2)
      assert(stream1.startsWith(stream2))
    }

    it("second stream has subset but is longer, then return false") {
      val stream1 = Stream.from(1).take(5)
      val stream2 = Stream.from(1).take(8)
      assert(!stream1.startsWith(stream2))
    }
  }

  describe("tails") {
    it("for empty stream, epmty stream is returned") {
      val stream = Stream.empty[Int]
      val expected = Stream.empty[Stream[Int]]
      assert(expected == stream.tails)
    }

    it("for populated stream, stream with subsequent tails is returned") {
      val stream = Stream.from(0).take(5)
      val expected: Stream[Stream[Int]] = Stream(Stream.from(0).take(5), Stream.from(1).take(4), Stream.from(2).take(3), Stream.from(3).take(2), Stream.from(4).take(1))
      assert(checkStreamStreams(expected, stream.tails))
    }
  }
}
