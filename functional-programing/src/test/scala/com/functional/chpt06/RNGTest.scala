package com.functional.chpt06

import com.functional.chpt06.RNGTest.{validateDouble, validateInstance, validateInt}
import org.scalatest.funspec.AnyFunSpec

class RNGTest extends AnyFunSpec {

  import RNG._
  import RNGTest.exampleSeed

  describe("SimpleRNG") {
    it("nonnegative returns a non-negative value") {
      val rng = SimpleRNG(exampleSeed)
      val (nextInt, _) = nonNegativeInt(rng)
      assert(validateInstance[Int](nextInt))
      assert(validateInt(nextInt))
    }

    it("double returns a double of [0,1.0)") {
      val rng = SimpleRNG(exampleSeed)
      val (nextDouble, _) = double(rng)
      assert(validateInstance[Double](nextDouble))
      assert(validateDouble(nextDouble))
    }

    it("intDouble return a pair of int and double with rng") {
      val rng = SimpleRNG(exampleSeed)
      val ((int, double), _) = intDouble(rng)
      assert(validateInstance[Int](int))
      assert(validateInt(int))
      assert(validateInstance[Double](double))
      assert(validateDouble(double))
    }

    it("doubleInt return a pair of double and int with rng") {
      val rng = SimpleRNG(exampleSeed)
      val ((value1, value2), _) = doubleInt(rng)
      assert(validateInstance[Double](value1))
      assert(validateDouble(value1))
      assert(validateInstance[Int](value2))
      assert(validateInt(value2))
    }

    it("double3 return a pair of triple double and rng") {
      val rng = SimpleRNG(exampleSeed)
      val (d1, rng1) = double(rng)
      val (d2, rng2) = double(rng1)
      val (d3, nextRng) = double(rng2)
      val result = double3(rng)
      val expectedResult = ((d1, d2, d3), nextRng)
      assert(expectedResult == result)
    }

    it("ints with zero returns an empty list with initial rng") {
      val rng = SimpleRNG(exampleSeed)
      val (list, nextRng) = ints(0)(rng)
      assert(list.isEmpty)
      assert(rng == nextRng)
    }

    it("ints with 5 returns an list of 5 ints with next rng") {
      val amount = 5
      val rng = SimpleRNG(exampleSeed)
      val (i1, r1) = rng.nextInt
      val (i2, r2) = r1.nextInt
      val (i3, r3) = r2.nextInt
      val (i4, r4) = r3.nextInt
      val (i5, r5) = r4.nextInt
      val expected = List(i1, i2, i3, i4, i5)
      val (list, nextRng) = ints(amount)(rng)
      assert(list.size == amount)
      assert(expected == list)
      assert(r5 == nextRng)
    }
  }
  describe("sequence") {
    it("For empty list rand with empty list is returned") {
      val rng = SimpleRNG(exampleSeed)
      val input = List()
      val expectedResult = unit(Nil: List[Int])
      val result = sequence(input)
      assert(expectedResult(rng) == result(rng))
    }
    it("For list of rands returns a rand of list with elements") {
      val rng = SimpleRNG(exampleSeed)
      val input = List(unit(1), unit(2), unit(3), unit(4), unit(5))
      val expectedResult = unit((1 to 5).toList)
      val result = sequence(input)
      assert(expectedResult(rng) == result(rng))
    }
  }

  describe("flatMap") {
    it("rand is transformed to other rand") {
      val initRand = unit(5)
      val rng = SimpleRNG(System.currentTimeMillis())
      val expectedRand = unit("5")
      val fn = (s: Int) => unit(s.toString)
      val result = flatMap(initRand)(fn)
      assert(expectedRand(rng) == result(rng))
    }
  }
}

object RNGTest {
  val exampleSeed = 42

  def validateInstance[A](value: Any): Boolean = value.isInstanceOf[A]

  def validateInt(value: Int): Boolean = value > 0 && value < Int.MaxValue

  def validateDouble(value: Double): Boolean = value >= 0 && value < 1.0
}
