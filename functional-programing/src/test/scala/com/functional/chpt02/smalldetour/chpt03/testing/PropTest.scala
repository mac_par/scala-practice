package com.functional.chpt02.smalldetour.chpt03.testing

import com.functional.chpt02.smalldetour.chpt02.repeat.Par
import org.scalatest.funspec.AnyFunSpec

import java.util.concurrent.Executors

class PropTest extends AnyFunSpec {
  describe("testing Par") {
    it("1 + 1 = 2") {
      val es = Executors.newCachedThreadPool
      val p2 = Prop.check{
        val p = Par.map(Par.unit(1))(_ + 1)
        val cond = Par.unit(2)
        Par.equal(p, cond)(es).get()
      }
      println(p2)
    }

    it("strange and i dont get it") {
      val s = Gen.weighted(Gen.choose(1,4).map(Executors.newFixedThreadPool) -> .75,
        Gen.unit(Executors.newCachedThreadPool) -> .25)
    }
  }
}
