package com.functional.chpt02.smalldetour.chpt02.repeat.actor

import com.functional.chpt02.smalldetour.chpt02.repeat.actor.NonBlocking.{Par, parMap, unit}
import org.scalatest.funspec.AnyFunSpec

import java.util.concurrent.{ExecutorService, Executors}

class NonBlockingTest extends AnyFunSpec {
  describe("testing") {
    it("latch should be closed") {
      val es: ExecutorService = Executors.newFixedThreadPool(1)
      val dummyPar: Par[Int] = es => (k: Int => Unit) => {
        throw new Exception("tak")
      }
      assert(0 == NonBlocking.run(es)(dummyPar))
    }

    it("run should get value") {
      val es: ExecutorService = Executors.newFixedThreadPool(1)
      val p: Par[Int] = unit(1)
      assert(1 == (NonBlocking.run(es)(p)))
    }

    it("parMap") {
      val es: ExecutorService = Executors.newFixedThreadPool(2)
      val p: Par[List[Double]] = parMap(List.range(1, 1000))(math.sqrt(_))
      println(NonBlocking.run(es)(p))
    }
  }
}
