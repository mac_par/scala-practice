package com.functional.chpt04

import org.scalatest.funspec.AnyFunSpec

class EitherTest extends AnyFunSpec {
  describe("map") {
    it("transform value returning right") {
      val right = Right(2)
      val trans = (i: Int) => i * 3
      val expected = Right(6)
      val result = right.map(trans)
      assert(expected == result)
    }

    it("left returns left") {
      val value = "dffs"
      val left = Left(value)
      val trans = (i: Int) => i * 3
      val expected = Left(value)
      val result = left.map(trans)
      assert(expected == result)
    }
  }

  describe("sequence") {
    it("list of eithers is transformed into either of list items") {
      val list = List(Right(1), Right(2), Right(3), Right(4), Right(5))
      val expected = Right((1 to 5).toList)
      val result = Either.sequence(list)
      assert(expected == result)
    }

    it("list of eithers with a left is transformed into left1") {
      val expected = Left("it failed")
      val list = List(Right(1), Right(2), Right(3), expected, Right(4), Right(5))
      val result = Either.sequence(list)
      assert(expected == result)
    }

    it("list of eithers with a left is transformed into left2") {
      val expected = Left("it failed")
      val list = List(Right(1), Right(2), Right(3), Right(4), Right(5), expected)
      val result = Either.sequence(list)
      assert(expected == result)
    }
  }

  describe("traverse") {
    it("list of eithers is transformed into either of list items") {
      val list = List(1, 2, 3, 4, 5)
      val fn = (i: Int) => Right(i.toString())
      val expected = Right((1 to 5).map(_.toString).toList)
      val result = Either.traverse(list)(fn)
      assert(expected == result)
    }

    it("list of eithers with a left is transformed into left1") {
      val list = List(1, 2, 3, 4, 5)
      val expected = Left("failure")
      val fn = (i: Int) => if (i < 4) Right(i.toString) else expected
      val result = Either.traverse(list)(fn)
      assert(expected == result)
    }

    it("list of eithers with a left is transformed into left2") {
      val list = List(1, 2, 3, 4, 5)
      val expected = Left("failure")
      val fn = (i: Int) => if (i != 5) Right(i.toString) else expected
      val result = Either.traverse(list)(fn)
      assert(expected == result)
    }
  }
}
