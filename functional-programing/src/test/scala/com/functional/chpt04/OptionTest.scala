package com.functional.chpt04

import org.scalatest.funspec.AnyFunSpec

class OptionTest extends AnyFunSpec {
  describe("sequence") {
    it("list of non-empty options is transformed into option with list of those elements") {
      val input: List[Option[Int]] = List(Some(1), Some(2), Some(3), Some(4))
      val expected = Some(List(1, 2, 3, 4))
      assert(expected == Option.sequence(input))
    }

    it("list of at least single empty option is transformed into None") {
      val input: List[Option[Int]] = List(Some(1), None, Some(3), Some(4))
      val expected = None
      assert(expected == Option.sequence(input))
    }

    it("empty list is transformed into Some(List())") {
      val input: List[Option[Int]] = List()
      val expected = Some(Nil)
      assert(expected == Option.sequence(input))
    }
  }
}
