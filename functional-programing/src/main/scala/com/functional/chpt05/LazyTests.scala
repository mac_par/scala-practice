package com.functional.chpt05

import com.functional.chpt05.stream.Stream

object LazyTests {
  def maybeTwice(pred: => Boolean, i: => Int): Int = {
    lazy val value = i
    if (pred) value + value else 0
  }

  def main(args: Array[String]): Unit = {
    println(maybeTwice(false, {
      println("Hi there!");
      41 + 1
    }))
    println(maybeTwice(true, {
      println("Hi there!");
      41 + 1
    }))

    val ones: Stream[Int] = Stream.constant(1)
    println(ones.take(12).toList)
    println(Stream.from(1).take(22).toList)
    println(Stream.fibs().take(22).toList)
    println(Stream.unfold[Int, Int](0) { s => Some((s, s + 1)) }.take(22).toList)
  }
}
