package com.functional.chpt05.stream

import com.functional.chpt05.stream.Stream.{cons, empty, unfold}

sealed trait Stream[+A] {
  def headOption: Option[A] = foldRight[Option[A]](None) { (item, _) => Some(item)
  } /*this match {
    case Empty => None
    case Cons(head, _) => Some(head())
  }*/

  def toList: List[A] = foldRight[List[A]](Nil) {
    _ :: _
  }
  /*this match {
    case Empty => List.empty[A]
    case Cons(head, tail) => head() :: tail().toList
  }*/

  def take(n: Int): Stream[A] = unfold((this, n)) {
    case (Cons(h, t), cnt) if cnt > 0 => Option((h(), (t(), cnt - 1)))
    case _ => None
  } /*this match {
    case Cons(h, t) if n > 0 => cons(h(), t().take(n - 1))
    case _ => empty
  }*/
  /*{
     def goAndTake(counter: Int, stream: => Stream[A]): Stream[A] = if (counter == n) empty else stream match {
       case Empty => empty
       case Cons(h, t) => Cons(h, () => goAndTake(counter + 1, t()));
     }

     goAndTake(0, this)
   }*/

  def takeWhile(pred: A => Boolean): Stream[A] = unfold(this) {
    case Cons(h, t) if (pred(h())) => Option((h(), t()))
    case _ => None
  }
  /*foldRight[Stream[A]](empty) { (item, comb) =>
    if (pred(item)) cons(item, comb) else comb
  }*/

  /*this match {
    case Cons(h, t) if (pred(h())) => cons(h(), t().takeWhile(pred))
    case _ => empty
  }
*/
  def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if n > 0 => t().drop(n - 1)
    case Cons(_, _) => this
    case Empty => empty
  } /*{
    def goAndDrop(counter: Int, stream: => Stream[A]): Stream[A] = if (counter == n) stream else stream match {
      case Empty => empty
      case Cons(_, t) => goAndDrop(counter + 1, t())
    }

    goAndDrop(0, this)
  }*/

  def exists(pred: A => Boolean): Boolean = foldRight(false) { (a, b) =>
    pred(a) || b
  }

  def foldRight[B](z: => B)(fn: (A, => B) => B): B = this match {
    case Cons(h, t) => fn(h(), t().foldRight(z)(fn))
    case _ => z
  }

  def forAll(pred: A => Boolean): Boolean = foldRight(true) { (a, b) => pred(a) && b }

  def map[B](fn: A => B): Stream[B] = unfold[B, Stream[A]](this) {
    case Cons(h, t) => Some((fn(h()), t()))
    case Empty => None
  }
  /*foldRight[Stream[B]](empty) { (item, comb) => cons(fn(item), comb) }*/

  def flatMap[B](fn: A => Stream[B]): Stream[B] = foldRight[Stream[B]](empty) { (item, comb) => fn(item) append (comb) }

  def filter(pred: A => Boolean): Stream[A] = foldRight[Stream[A]](empty[A]) { (item, combiner) =>
    if (pred(item)) cons(item, combiner) else combiner
  }

  def append[A2 >: A](that: => Stream[A2]): Stream[A2] = foldRight[Stream[A2]](that) { (item, combiner) => cons(item, combiner) }

  def find(pred: A => Boolean): Option[A] = filter(pred).headOption

  def zipAll[B](s2: Stream[B]): Stream[(Option[A], Option[B])] =
    zipWithAll(s2)((_, _))

  def zipWith[B, C](s2: Stream[B])(fn: (A, B) => C): Stream[C] = unfold((this, s2)) {
    case (Cons(h1, t1), Cons(h2, t2)) => Option((fn(h1(), h2()), (t1(), t2())))
    case _ => None
  }

  def zipWithAll[B, C](s2: Stream[B])(fn: (Option[A], Option[B]) => C): Stream[C] = unfold((this, s2)) {
    case (Empty, Empty) => None
    case (Cons(h1, t1), Cons(h2, t2)) => Some(fn(Some(h1()), Some(h2())) -> (t1(), t2()))
    case (Cons(h1, t1), Empty) => Some(fn(Some(h1()), None) -> (t1(), empty))
    case (Empty, Cons(h2, t2)) => Some(fn(None, Some(h2())) -> (empty, t2()))
  }

  def startsWith[A](s: Stream[A]): Boolean = zipAll(s).takeWhile(_._2.isDefined).forAll { case (h1, h2) => h1 == h2 }

  def tails: Stream[Stream[A]] = unfold(this) {
    case Cons(h, t) => Option((cons(h(), t()), t()))
    case Empty => None
  }

  def scanRight[B](seed: B)(fn: (A, B) => B): Stream[B] = foldRight((seed, Stream(seed))) {
    case (a, s0) =>
      lazy val s1 = s0
      val b2 = fn(a, s1._1)
      (b2, cons(b2, s1._2))
  }._2

}

object Stream {
  def empty[A]: Stream[A] = Empty

  def cons[A](hd: => A, td: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = td
    Cons(() => head, () => tail)
  }

  def constant[A](a: A): Stream[A] = unfold(a) { _ => Some(a, a) }
  /*{
    lazy val tail: Stream[A] = Cons(() => a, () => tail)
    tail
  }*/

  def from(n: Int): Stream[Int] = cons(n, from(n + 1))

  def fibs(): Stream[Int] = unfold((0, 1)) { case (f0, f1) => Some(f0, (f1, f0 + f1)) } /*{
    def go(f0: Int, f1: Int): Stream[Int] = {
      cons(f0, go(f1, f0 + f1))
    }

    go(0, 1)
  }*/

  def unfold[A, S](z: S)(fn: S => Option[(A, S)]): Stream[A] = fn(z) match {
    case Some((item, seed)) => cons(item, unfold(seed)(fn))
    case _ => empty[A]
  }

  def apply[A](items: A*): Stream[A] = if (items.isEmpty) empty else cons(items.head, apply(items.tail: _*))
}

case object Empty extends Stream[Nothing]

case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

