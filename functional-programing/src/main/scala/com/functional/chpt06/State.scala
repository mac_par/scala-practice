package com.functional.chpt06

case class State[S, +A](run: S => (A, S)) {

  import State.unit

  def map[B](fn: A => B): State[S, B] = flatMap {
    fn andThen unit
  }

  def map2[B, C](bs: State[S, B])(fn: (A, B) => C): State[S, C] = flatMap { a => bs.map { b => fn(a, b) } }

  def flatMap[B](fn: A => State[S, B]): State[S, B] = State({ s: S =>
    val (a, state) = run(s)
    fn(a).run(state)
  })
}

object State {
  type Rand[A] = State[RNG, A]
  //  type State[S, +A] = S => (A, S)

  def unit[S, A](a: A): State[S, A] = State(s => (a, s))

//  def sequence[S, A](xs: List[State[S, A]]): State[S, List[A]] = traverse[S,A, List[A]](xs)(_)

  def traverse[S, A, B](xs: List[A])(fn: A => State[S, B]): State[S, List[B]] =
    xs.foldRight[State[S, List[B]]](unit(Nil: List[B])) { (a, state) =>
      fn(a).map2(state) {
        _ :: _
      }
    }

  def get[S]: State[S, S] = State({ s: S => (s, s) })

  def set[S](s: S): State[S, Unit] = State({ _: S => ((), s) })
}
