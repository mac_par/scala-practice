package com.functional.chpt06.dispenser

sealed trait Input

case object Coin extends Input

case object Turn extends Input
