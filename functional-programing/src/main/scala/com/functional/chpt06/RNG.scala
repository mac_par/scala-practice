package com.functional.chpt06

sealed trait RNG {
  def nextInt: (Int, RNG)
}

case class SimpleRNG(seed: Long) extends RNG {
  override def nextInt: (Int, RNG) = {
    val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
    val nextRNG = SimpleRNG(newSeed)
    val n = (newSeed >>> 16).toInt
    (n, nextRNG)
  }
}

object RNG {
  type Rand[+A] = RNG => (A, RNG)

  val int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] = rng => (a, rng)

  def map[A, B](r: Rand[A])(fn: A => B): Rand[B] = flatMap(r) { a => unit(fn(a)) }
  /*def map[A, B](r: Rand[A])(fn: A => B): Rand[B] = rng => {
    val (value, rng2) = r(rng)
    (fn(value), rng2)
  }*/

  def map2[A, B, C](ra: Rand[A])(rb: Rand[B])(fn: (A, B) => C): Rand[C] = flatMap(ra) { aValue => map(rb) { valueB => fn(aValue, valueB) } }
  /*def map2[A, B, C](ra: Rand[A])(rb: Rand[B])(fn: (A, B) => C): Rand[C] = rng => {
    val (va, ran) = ra(rng)
    val (vb, rbn) = rb(ran)
    (fn(va, vb), rbn)
  }*/

  def both[A, B](ra: Rand[A], rb: Rand[B]): Rand[(A, B)] = map2(ra)(rb) {
    (_, _)
  }

  def nonNegative: Rand[Int] = map(nonNegativeInt)(i => i - 1 % 2)

  def nonNegativeInt(rng: RNG): (Int, RNG) = rng.nextInt match {
    case (value, nextRng) => if (value < 0) (-(value + 1), nextRng) else (value, nextRng)
  }

  def nonNegativeLessThan(than: Int): Rand[Int] = flatMap(nonNegative) { i =>
    val mod = i % than
    if (i + (than -1) - mod >= 0) unit(mod) else nonNegativeLessThan(than)
  }

  def double: Rand[Double] = map(nonNegative) {
    _ / (Int.MaxValue.toDouble + 1)
  }

  /*{
    val (nextValue, nextRng) = nonNegativeInt(rng)
    (nextValue / Int.MaxValue.toDouble, nextRng)
  }*/
  def intDouble: Rand[(Int, Double)] = both(nonNegative, double)
  /*(rng: RNG): ((Int, Double), RNG) =
  {
    val (i, nr) = rng.nextInt
    val (d, _) = double(rng)
    ((i, d), nr)
  }*/

  def doubleInt: Rand[(Double, Int)] = both(double, nonNegative)
  /*(rng: RNG): ((Double, Int), RNG) = {
    val ((nextInt, nextDouble), nextRNG) = intDouble(rng)
    ((nextDouble, nextInt), nextRNG)
  }*/

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, rng1) = double(rng)
    val (d2, rng2) = double(rng1)
    val (d3, rng3) = double(rng2)
    ((d1, d2, d3), rng3)
  }

  def ints(count: Int): Rand[List[Int]] = sequence(List.fill(count)(int))

  /*def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
    if (count <= 0) {
      (List(), rng)
    } else {
      val (value, nr) = rng.nextInt
      val (list, r) = ints(count - 1)(nr)
      (value :: list, r)
    }
  }*/

  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = fs.foldRight(unit(Nil: List[A]))((rand, acc) => map2(rand)(acc) {
    _ :: _
  })

  def flatMap[A, B](r: Rand[A])(fn: A => Rand[B]): Rand[B] = rng => {
    val (a, rng1) = r(rng)
    fn(a)(rng1)
  }

  def rollDice: Rand[Int] = nonNegativeLessThan(6)
}
