package com.functional.chpt08

import com.functional.chpt06.{RNG, State}

object Gen {
  def listOf[A](a: Gen[A]): Gen[List[A]] = ???

  def listOfN[A](size: Int, a: Gen[A]): Gen[List[A]] = Gen(State(RNG.sequence(List.fill(size) {
    a.state.run
  })))

  def forAll[A](gen: Gen[A])(fn: A => Boolean): Prop = Prop.forAll(gen)(fn)

  def choose(start: Int, stopExclusive: Int): Gen[Int] = Gen(State(RNG.map(RNG.nonNegativeLessThan(stopExclusive - start)) {
    _ + start
  }))

  def unit[A](a: => A): Gen[A] = Gen(State(RNG.unit(a)))

  def boolean: Gen[Boolean] = Gen(State(RNG.map(RNG.nonNegativeLessThan(2)) {
    _ == 1
  }))

  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] = boolean.flatMap {
    if (_) g1 else g2
  }

  def weighted[A](g1: (Gen[A], Double), g2: (Gen[A], Double)): Gen[A] = choose(0, (g1._2 + g2._2).toInt) flatMap { w =>
    if (w < g1._2) g1._1 else g2._1
  }
}

case class Gen[A](state: State[RNG, A]) {
  def flatMap[B](f: A => Gen[B]): Gen[B] = Gen(State(RNG.flatMap(state.run)(f(_).state.run)))

  def listOfN(size: Int): Gen[List[A]] = Gen.listOfN(size, this)
}
