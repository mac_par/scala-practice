package com.functional.chpt08

import com.functional.chpt05.stream.Stream
import com.functional.chpt06.RNG
import com.functional.chpt08.Prop._

import scala.{Stream => _}


object Prop {
  type TestCases = Int
  type SuccessCount = Int
  type FailedCase = String

  sealed trait Result {
    def isFalsified: Boolean
  }

  case object Passed extends Result {
    override def isFalsified: Boolean = false
  }

  case class Failed(failure: FailedCase, successes: SuccessCount) extends Result {
    override def isFalsified: Boolean = true
  }

  def randomStream[A](g: Gen[A])(rng: RNG): Stream[A] = Stream.unfold(rng) { v => Some(g.state.run(v)) }

  def forAll[A](as: Gen[A])(f: A => Boolean): Prop = Prop { (size, rng) =>
    randomStream(as)(rng).zipWith(Stream.from(0))((_, _)).take(size).map {
      case (a, i) => try {
        if (f(a)) Passed else Failed(a.toString, i)
      } catch {
        case e: Exception => Failed(buildMessage(a, e), i)
      }
    }.find(_.isFalsified).getOrElse(Passed)
  }

  private def buildMessage[A](a: A, e: Exception): String = s"test case: $a\n" +
    s"generated an exception: ${e.getMessage}\n" +
    s"stack trace:\n ${e.getStackTrace.mkString("\n")}"


}

case class Prop(run: (TestCases, RNG) => Result) {

  def &&(prop: Prop): Prop = ???

  def check: Result = ???
}