package com.functional.chpt02.smalldetour.chpt03.testing

import com.functional.chpt02.smalldetour.chpt03.t4.improved.{RNG, State}

case class Gen[+A](sample: State[RNG, A]) {
  def flatMap[B](f: A => Gen[B]): Gen[B] = Gen(sample.flatMap(f(_).sample))

  def listOfN(size: Gen[Int]): Gen[List[A]] = size flatMap {
    listOfN
  }

  def listOfN(size: Int): Gen[List[A]] = Gen.listOfN(size, this)

  def unsized: SGen[A] = SGen(n => this)

  def listOf: SGen[List[A]] = Gen.listOf(this)

  def map[B](fn: A => B): Gen[B] = Gen(sample.map(fn))

  def map2[B, C](g: Gen[B])(fn: (A, B) => C): Gen[C] = Gen(sample.map2(g.sample)(fn))

  def **[B](g: Gen[B]): Gen[(A,B)] = map2(g)((_,_))
}

object Gen {
  def unit[A](a: => A): Gen[A] = Gen(State(RNG.unit(a)))

  def boolean: Gen[Boolean] = Gen(State(RNG.boolean))

  def listOf[A](gen: Gen[A]): SGen[List[A]] = SGen(n => gen.listOfN(n))

  def listOfN[A](n: Int, a: Gen[A]): Gen[List[A]] = Gen(State.sequence(List.fill(n)(a.sample)))

  def forAll[A](a: Gen[A])(pred: A => Boolean): Prop = Prop { (m, n, rng) =>
    randomStream(a)(rng).zip(Stream.from(0))
      .take(n).map {
      case (a, i) => try {
        if (pred(a)) Passed else Failed(a.toString, i)
      } catch {
        case e: Exception => Failed(buildMsg(a, e), i)
      }
    }.find(_.isFalsified).getOrElse(Passed)
  }

  def forAll[A](g: Int => Gen[A])(pred: A => Boolean): Prop = Prop { (max, t, rng) => {
    val casesPerSize = (t + (max + 1)) / max
    val props: Stream[Prop] = Stream.from(0).take((t min max) + 1).map { i =>
      forAll(g(i))(pred)
    }
    val prop: Prop = props.map(p => Prop { (max, _, rng) => p.run(max, casesPerSize, rng) }).toList.reduce(_ && _)
    prop.run(max, casesPerSize, rng)
  }
  }

  def buildMsg[A](a: A, exception: Exception): String =
    s"""test case: $a
       |generated and exception: ${exception.getMessage}
       |stacktrace: ${exception.getStackTrace.mkString("\n")}""".stripMargin

  def randomStream[A](as: Gen[A])(rng: RNG): Stream[A] = Stream.unfold(rng) { rng => Some(as.sample.run(rng)) }

  def choose(start: Int, stopExclusive: Int): Gen[Int] = Gen(State(RNG.nonNegativeInt).map(n => start + n % (stopExclusive - start)))

  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] = boolean.flatMap {
    if (_) g1 else g2
  }

  def weighted[A](g1: (Gen[A], Double), g2: (Gen[A], Double)): Gen[A] = {
    //propability
    val threshold = g1._2.abs / (g1._2.abs + g2._2.abs)
    Gen(State(RNG._double).flatMap(d => if (d < threshold) g1._1.sample else g2._1.sample))
  }

  def listOf1[A](g: Gen[A]): SGen[List[A]] = SGen(n => g.listOfN(n max 1))
}

object ** {
  def unapply[A,B](p:(A,B)): Option[(A,B)] = Some(p)
}
