package com.functional.chpt02.smalldetour.chpt03.testing

import com.functional.chpt02.smalldetour.chpt03.t4.improved.RNG
import com.functional.chpt02.smalldetour.chpt03.testing.Prop._

case class Prop(run: (MaxSize, TestCases, RNG) => Result) {
  def &&(p: Prop): Prop = Prop((m, t, rng) => run(m, t, rng) match {
    case Passed | Proved => p.run(m, t, rng)
    case x => x
  })

  def ||(p: Prop): Prop = Prop { (m, t, rng) =>
    run(m, t, rng) match {
      case Failed(msg, _) => p.tag(msg).run(m, t, rng)
      case x => x
    }
  }

  def tag(msg: String): Prop = Prop { (m, t, rng) =>
    run(m, t, rng) match {
      case Failed(failed, successes) => Failed(s"$msg\n$failed", successes)
      case x => x
    }
  }

  def check: Result = ???
}

object Prop {
  type SuccessCount = Int
  type FailedCase = String
  type TestCases = Int
  type MaxSize = Int

  def run(p: Prop, maxSize: Int = 100, testCases: Int = 100, rng: RNG = RNG.Simple(System.currentTimeMillis())): Unit = {
    p.run(maxSize, testCases, rng) match {
      case Failed(msg, successes) => println(s"! Failed after $successes passed tests: \n$msg")
      case Passed => println(s"+ OK, passed $testCases tests.")
      case Proved => println(s"+ OK, proved property.")
    }
  }

  def check(p: => Boolean): Prop = Prop { (_, _, _) => if (p) Passed else Failed("()", 0) }
}
