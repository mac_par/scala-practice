package com.functional.chpt02.smalldetour.chpt03.except

object Laziness extends App {
  def doIt(cond: Boolean, i: => Int): Int = if (cond) i + i else 0

  println(doIt(false, {println("h1"); 41 +1}))
  println(doIt(true, {println("h1"); 41 +1}))
}
