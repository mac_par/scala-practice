package com.functional.chpt02.smalldetour.tree

sealed trait Tree[+A]

case class Leaf[+A](value: A) extends Tree[A]

case class Branch[+A](value: A, left: Tree[A], right: Tree[A]) extends Tree[A]