package com.functional.chpt02.smalldetour.chpt02.repeat

import java.util.concurrent.{Callable, ExecutorService, Future, TimeUnit}
import scala.language.implicitConversions

object Par {

  type Par[A] = ExecutorService => Future[A]

  def run[A](es: ExecutorService)(a: Par[A]): Future[A] = a(es)

  def unit[A](a: A): Par[A] = es => UnitFuture(a)

  def lazyUnit[A](a: => A): Par[A] = fork(unit(a))

  private case class UnitFuture[A](get: A) extends Future[A] {
    override def isDone: Boolean = true

    override def cancel(mayInterruptIfRunning: Boolean): Boolean = false

    override def isCancelled: Boolean = false

    override def get(timeout: Long, unit: TimeUnit): A = get
  }

  def map2[A, B, C](a: Par[A], b: Par[B])(fn: (A, B) => C): Par[C] = (es: ExecutorService) => UnitFuture(fn(a(es).get, b(es).get))

  def fork[A](a: => Par[A]): Par[A] = es => es.submit(() => a(es).get)

  def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  def sortPar[A <: Comparable[A]](parList: Par[List[A]]): Par[List[A]] = map(parList)(_.sorted)

  def map[A, B](par: Par[A])(fn: A => B): Par[B] = map2(par, unit(()))((a, _) => fn(a))

  def parMap[A, B](ps: List[A])(p: A => B): Par[List[B]] = sequence(ps.map(asyncF(p)))

  def sequence[A](ps: List[Par[A]]): Par[List[A]] = ps.foldRight[Par[List[A]]](unit(List()))((l, p) => map2(l, p)(_ :: _))

  def equal[A](p1: Par[A], p2: Par[A]): Par[Boolean] = map2(p1, p2)(_ == _)

//  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = as map (asyncF(a => if (f(a)) List(a) else List.empty)) map (sequence)
}
