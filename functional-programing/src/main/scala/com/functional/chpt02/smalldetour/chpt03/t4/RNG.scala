package com.functional.chpt02.smalldetour.chpt03.t4

trait RNG {
  def nextInt: (Int, RNG)
}
