package com.functional.chpt02.smalldetour.chpt03.except.t2

object Namen extends App {
  def mkName(name: String): Either[String, Name] =
    if (name == "" || name == null) Left("Name is empty.")
    else Right(new Name(name))

  def mkAge(age: Int): Either[String, Age] =
    if (age < 0) Left("Age is out of range.")
    else Right(new Age(age))

 /* def mkPerson(name: String, age: Int): Either[String, Person] =
    mkName(name).map2(mkAge(age)){
      case
    }*/

//  println(mkPerson("", -1))
}

case class Person(name: Name, age: Age)

sealed class Name(val value: String)

sealed class Age(val value: Int)
