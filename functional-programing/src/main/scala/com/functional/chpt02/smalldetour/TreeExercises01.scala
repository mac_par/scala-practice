package com.functional.chpt02.smalldetour

import com.functional.chpt02.smalldetour.tree.{Branch, Leaf, Tree}

object TreeExercises01 extends App {
  def size[A](root: Tree[A]): Int = root match {
    case Leaf(value) => 1
    case Branch(_, left, right) => 1 + size(left) + size(right)
    case _ => 0
  }

  def max[A](root: Tree[A])(implicit ord: Ordering[A]): Option[A] = root match {
    case Leaf(value) => Some(value)
    case Branch(value, left, right) => compareValues(compareValues(Some(value), max(left)), max(right))
    case _ => None
  }

  def compareValues[A](opt1: Option[A], opt2: Option[A])(implicit ord: Ordering[A]): Option[A] = (opt1, opt2) match {
    case (None, Some(v)) => Some(v)
    case (Some(v), None) => Some(v)
    case (Some(v1), Some(v2)) => Some(ord.max(v1, v2))
  }

  def depth[_](root: Tree[_]): Int = root match {
    case Leaf(_) => 1
    case Branch(_, left, right) => 1 + math.max(depth(left), depth(right))
    case _ => 0
  }

  def map[A, B](root: Tree[A])(fn: A => B): Tree[B] = root match {
    case Leaf(value) => Leaf(fn(value))
    case Branch(value, left, right) => Branch(fn(value), map(left)(fn), map(right)(fn))
    case _ => null
  }

  def fold[A](base: A)(root: Tree[A])(fn: (A, A) => A): A = root match {
    case Leaf(value) => value
    case Branch(value, left, right) => fn(value, fn(fold(base)(left)(fn), fold(base)(right)(fn)))
    case _ => base
  }

  val leafA = Leaf('A')
  val leafB = Leaf('B')
  val branchAB = Branch('E', leafA, leafB)
  val leafC = Leaf('C')
  val branchC = Branch('D', leafC, null)
  val root = Branch('A', branchAB, branchC)

  println(size(root))
  println(size(null))
  println(size(Leaf(''')))
  println(size(Branch('O', null, null)))
  println(max(Branch('O', null, null)))
  println(max(root))

  val leaf1 = Leaf(1)
  val leaf2 = Leaf(2)
  val branch0 = Branch(0, leaf1, leaf2)
  val leaf4 = Leaf(4)
  val leaf3 = Leaf(5)
  val branch3 = Branch(3, leaf3, leaf4)
  val root2 = Branch(-1, branch0, branch3)

  println(max(root2))
  println("depth")
  println(3)
  println(depth(root2))
  val leafa1 = Leaf(1)
  val branchab1 = Branch(3, Leaf(3), Branch(3, Leaf(2), null))
  val brancha1 = Branch(2, leafa1, branchab1)
  val rootab = Branch(6, Leaf(5), brancha1)
  println(5)
  println(depth(rootab))

  println("zie mapen die tree")
  println(map(root)(x => if (x != null) x.toInt else null))
  println(map(rootab)(x => if (x != null) x * 2 else 0))
  println("fold it")
  println(s"${fold(0)(rootab)(_ + _)} = 25")
  println(s"${fold(0)(root2)(_ + _)} = 14")
}
