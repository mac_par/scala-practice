package com.functional.chpt02.smalldetour.t3

import com.functional.chpt02.smalldetour.t3.Par.{Par, fork}

object Splitting extends App {
  def sum1(s: Seq[Int]): Int = s.foldLeft(0)(_ + _)

  def sum2(s: IndexedSeq[Int]): Par[Int] = {
    val size = s.size
    if (size <= 1) {
      Par.unit(s.headOption.getOrElse(0))
    } else {
      val mid = size / 2
      val (part1, part2) = s.splitAt(mid)
      Par.map2(Par.fork(sum2(part1)), fork(sum2(part2)))(_ + _)
    }
  }
}
