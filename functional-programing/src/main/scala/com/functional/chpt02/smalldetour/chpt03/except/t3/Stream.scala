package com.functional.chpt02.smalldetour.chpt03.except.t3

import com.functional.chpt02.smalldetour.chpt03.except.t1.{None, Option, Some}

import scala.{None => _, Option => _, Some => _, Stream => _}

sealed trait Stream[+A] {

  import Stream._

  def headOption: Option[A] = this match {
    case Empty => None
    case Cons(h, _) => Some(h())
  }

  def headOption2: Option[A] = foldRight(None: Option[A])((a, _) => Some(a))

  def toList: List[A] = this match {
    case Empty => Nil
    case Cons(h, t) => h() :: t().toList
  }

  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 1 => cons(h(), t().take(n - 1))
    case Cons(h, _) if n == 1 => cons(h(), empty)
    case Empty => this
  }

  def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if n > 0 => t().drop(n - 1)
    case _ => this
  }

  def exists(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) || t().exists(p)
    case _ => false
  }

  def exists2(p: A => Boolean): Boolean = foldRight(false)((a, b) => p(a) || b)

  def foldRight[B](z: => B)(f: (A, B) => B): B = this match {
    case Cons(h, t) => f(h(), t().foldRight(z)(f))
    case _ => z
  }

  def forAll(p: A => Boolean): Boolean = foldRight(true)((a, b) => p(a) && b)

  def takeWhile(p: A => Boolean): Stream[A] = foldRight(empty[A])((a, b) => if (p(a)) cons(a, b) else b)

  def map[B](p: A => B): Stream[B] = foldRight(Empty: Stream[B])((a, b) => cons(p(a), b))

  def append[B >: A](p: => Stream[B]): Stream[B] = foldRight(p)((a, b) => cons(a, b))

  def flatMap[B >: A](p: A => Stream[B]): Stream[B] = foldRight(Empty: Stream[B])((a, b) => p(a) append (b))


}

case object Empty extends Stream[Nothing]

case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]


object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] = if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))

  def constant[A](a: A): Stream[A] = {
//    lazy val tail = cons(a, tail)
//    tail
    null
  }

  def from(n: Int): Stream[Int] = cons(n, from(n + 1))

  def fibs(): Stream[Int] = {
    def go_to(i: Int, j: Int): Stream[Int] = cons(i, go_to(j, i + j))

    go_to(0, 1)
  }

  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case Some(b) => cons(b._1, unfold(b._2)(f))
    case None => empty
  }

  def fibs2(): Stream[Int] = unfold((0, 1))(x => Some(x._1, (x._2, x._1 + x._2)))
}