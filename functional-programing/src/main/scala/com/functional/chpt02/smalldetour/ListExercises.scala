package com.functional.chpt02.smalldetour

object ListExercises extends App {
  def tail[A](list: List[A]): List[A] = list match {
    case Nil => Nil
    case head :: tail => tail
  }

  def setHead[A](list: List[A], newHead: A): List[A] = list match {
    case head :: tail => newHead :: tail
  }

  def drop[A](l: List[A], n: Int): List[A] = {
    def loop(l: List[A], n: Int): List[A] = l match {
      case Nil => Nil
      case _ :: tail if n > 0 => loop(tail, n - 1)
      case _ => l
    }

    loop(l, n)
  }

  def dropWhile[A](l: List[A], pred: A => Boolean): List[A] = {
    def loop(l: List[A], pred: A => Boolean): List[A] = l match {
      case Nil => Nil
      case head:: tail if pred(head) => loop(tail, pred)
      case _ => l
    }

    loop(l, pred)
  }

  def append[A](l1: List[A], l2: List[A]): List[A] = l1 match {
    case Nil=> l2
    case head::tail => head :: append(tail, l2)
  }

  def init[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case head:: Nil => Nil
    case head:: tail => head :: init(tail)
  }

  println(init(List(1,2,3,4,5,6)))
  println(dropWhile(List(1,2,3,4,5,6,7), (v: Int) => v < 6))
}
