package com.functional.chpt02.smalldetour

object ListFunctions2 extends App {
  def incrementEachElement(list: List[Int]): List[Int] = list match {
    case Nil => Nil
    case head :: tail => List(head + 1) ++ incrementEachElement(tail)
  }

  def map[A, B](list: List[A])(fn: A => B): List[B] = list match {
    case Nil => Nil
    case head :: tail => List(fn(head)) ++ map(tail)(fn)
  }

  def filter[A](list: List[A])(pred: A => Boolean): List[A] = list match {
    case Nil => Nil
    case head :: tail if pred(head) => List(head) ++ filter(tail)(pred)
    case _ :: tail => filter(tail)(pred)
  }

  def flatMap[A, B](list: List[A])(fn: A => List[B]): List[B] = list match {
    case Nil => Nil
    case head :: tail => fn(head) ++ flatMap(tail)(fn)
  }

  def mix(l1: List[Int], l2: List[Int]): List[Int] = (l1, l2) match {
    case (_, Nil) => Nil
    case (Nil, _) => Nil
    case (h1 :: t1, h2 :: t2) => List(h1 + h2) ++ mix(t1, t2)
  }

  def zipWith[A, B](l1: List[A], l2: List[B]): List[(A, B)] = (l1, l2) match {
    case (_, Nil) => Nil
    case (Nil, _) => Nil
    case (h1 :: t1, h2 :: t2) => List((h1, h2)) ++ zipWith(t1, t2)
  }

  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = sup match {
    case Nil => false
    case _ if startsWith(sup, sub) => true
    case h::t => hasSubsequence(t, sub)
  }

  def startsWith[A](l1: List[A], l2: List[A]): Boolean = (l1, l2) match {
    case (_, Nil) => true
    case (h1 :: t1, h2 :: t2) if h1 == h2 => startsWith(t1, t2)
    case _ => false
  }

  println(incrementEachElement(List(1, 5, 10, 2, 13, 31)))

  println(map(List("dupa", "tak", "nie", "moze"))(_.length))
  println(filter((1 to 15).toList)(_ % 2 == 1))
  println(flatMap((1 to 5).toList)(x => (x to x + 5).toList))
  println(flatMap(List("dupa", "tak", "nie", "moze"))(_.toCharArray.toList))
  println(flatMap((1 to 15).toList)(x => if (x % 2 == 1) List(x) else Nil))
  println(mix(List(1, 3, 5), List(2, 4, 6)))
  println(zipWith(List(1, 3, 5), List(2, 4, 6)))
  println(zipWith(List(1, 3, 5), List("entliczek", "pentliczek", "czerwony", "stoliczek")))
  println(hasSubsequence(List(1,2,4,5,2,3,8), List(4,5,2)))
  println(hasSubsequence(List(1,2,4,5,2,3,8), List(3,5,2)))
}
