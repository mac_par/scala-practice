package com.functional.chpt02.smalldetour.chpt03.t4

object RngTest extends App {
  val ZERO = 0

  def randomPair(rng: RNG): (Int, Int) = {
    val (v1, _) = rng.nextInt
    val (v2, _) = rng.nextInt
    (v1, v2)
  }

  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (value, rnG) = rng.nextInt
    (if (value < 0) -(value + 1) else value, rnG)
  }

  def double(rng: RNG): (Double, RNG) = {
    val (i, r) = rng.nextInt
    (i / (Int.MaxValue.toDouble + 1) , r)
  }

  def intDouble(rng: RNG): ((Int,Double), RNG) = {
    val (intVal, _) = nonNegativeInt(rng)
    val (doubleVal, r) = double(rng)
    ((intVal, doubleVal), r)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val ((intVal, doubleVal), r) = intDouble(rng)
    ((doubleVal, intVal), r)
  }

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (doubleVal, r) = double(rng)
    val (doubleVal1, r1) = double(r)
    val (doubleVal2, r2) = double(r1)
    ((doubleVal, doubleVal1, doubleVal2), r2)
  }

  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
   def gotTo(count: Int, list: List[Int],rng: RNG): (List[Int],RNG) = {
     if (count <= 0)
       (list, rng)
     else {
       val (intVal, r) = rng.nextInt
       gotTo(count -1, intVal :: list, r)
     }
   }

    gotTo(count, Nil, rng)
  }

  val rng = SimpleRNG(42)
  val (nbr, newRng) = rng.nextInt
  println(nbr)
  val (nbr2, newRng2) = newRng.nextInt
  println(nbr2)
  val (nbr3, newRng3) = newRng2.nextInt
  println(nbr3)

  println(randomPair(newRng3))
  println(ints(5)(newRng3))
  println(ints(0)(newRng3))
}
