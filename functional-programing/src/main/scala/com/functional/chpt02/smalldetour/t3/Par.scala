package com.functional.chpt02.smalldetour.t3

import java.util.concurrent._

object Par {
  type Par[A] = ExecutorService => Future[A]

  def run[A](s: ExecutorService)(a: Par[A]): Future[A] = a(s)

  def unit[A](a: A): Par[A] = es => UnitFuture(a)

  def lazyUnit[A](a: A): Par[A] = fork(unit(a))

  private case class UnitFuture[A](get: A) extends Future[A] {
    override def isDone: Boolean = true

    override def cancel(mayInterruptIfRunning: Boolean): Boolean = false

    override def isCancelled: Boolean = false

    override def get(timeout: Long, unit: TimeUnit): A = get
  }

  def fork[A](a: => Par[A]): Par[A] = es => a(es)

  def map[A, B](pa: Par[A])(f: A => B): Par[B] = map2(pa, unit(()))((a, _) => f(a))

  def map2[A, B, C](p1: Par[A], p2: Par[B])(f: (A, B) => C): Par[C] = (s: ExecutorService) => {
    UnitFuture(f(p1(s).get(), p2(s).get()))
  }

  def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  def sortPar(parList: Par[List[Int]]): Par[List[Int]] = map(parList)(_.sorted)

  def parMap[A, B](ps: List[A])(f: A => B): Par[List[B]] = sequence(ps.map(asyncF(f)))

  def sequence[A](ps: List[Par[A]]): Par[List[A]] = ps.foldRight[Par[List[A]]](unit(List.empty))((h, t) => map2(h, t)(_ :: _))

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = sequence(as.filter(f).map(asyncF(x => x)))

  def choiceN[A](n: Par[Int])(choices: List[Par[A]]): Par[A] = es => {
    val idx = run(es)(n).get()
    run(es)(choices(idx))
  }

  def choice[A](cond: Par[Boolean])(t: Par[A], r: Par[A]): Par[A] = es => if (run(es)(cond).get()) t(es) else r(es)

  def choiceMap[K, V](key: Par[K])(choices: Map[K, Par[V]]): Par[V] = es => run(es)(choices(run(es)(key).get()))

  def chooser[A,B](pa: Par[A])(choices: A => Par[B]): Par[B] = es => run(es)(choices(run(es)(pa).get()))

  def flatMap[A, B](a: Par[A])(f: A=> Par[B]): Par[B] = chooser(a)(f)
}
