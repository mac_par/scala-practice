package com.functional.chpt02.smalldetour.chpt03.except.t2

import scala.{Either => _, Left => _, Right => _}

object EitherTests extends App {
  def mean(xs: Seq[Double]): Either[String, Double] = {
    if (xs == null || xs.isEmpty) Left("List is empty!")
    else
      Right(xs.sum / xs.length)
  }

  def safeDiv(x: Int, y: Int): Either[Exception, Int] = try Right(x / y) catch {
    case e: Exception => Left(e)
  }

  def Try[A](a: => A): Either[Exception, A] =
    try Right(a)
    catch {
      case e: Exception => Left(e)
    }

  def sequence[E,A](es: List[Either[E,A]]): Either[E, List[A]] = traverse(es)(x => x)

  def traverse[E,A,B](as: List[A])(f: A=> Either[E, B]): Either[E, List[B]] = as match {
    case Nil => Right(Nil)
    case head :: tail => (f(head) map2 traverse(tail)(f))(_ :: _)
  }

  println(safeDiv(2, 0))

  val sequence = List(Right(5), Right(10), Right(20))
  val sequence2 = List(Right(5), Right(10), Left(new NullPointerException("dupa")), Right(20))
  println(sequence(sequence))
  println(sequence(sequence2))
}
