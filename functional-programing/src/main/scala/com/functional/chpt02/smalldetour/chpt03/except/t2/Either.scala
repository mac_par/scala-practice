package com.functional.chpt02.smalldetour.chpt03.except.t2

import scala.{Either => _, Right => _, Left => _}

sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E,B] = this match {
    case Right(value) => try Right(f(value)) catch {
      case e: E => Left(e)
  }
    case Left(value) => Left(value)
  }

  def flatMap[EE >: E, B](fn: A => Either[EE, B]): Either[EE, B] = this match {
    case Right(value) => fn(value)
    case Left(value) => Left(value)
  }

  def orElse[EE >:E,B >:A](b: Either[EE,B]): Either[EE, B] = this match {
    case Right(_) => this
    case Left(_) => b
  }

  def map2[EE >:E, B, C](b: Either[EE,B])(fn: (A,B) => C): Either[EE, C] = for {
    a <- this
    be <- b
  } yield fn(a,be)
}

case class Left[+E](value: E) extends Either[E, Nothing]

case class Right[+A](value: A) extends Either[Nothing, A]
