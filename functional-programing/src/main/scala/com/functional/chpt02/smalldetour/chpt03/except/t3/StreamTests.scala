package com.functional.chpt02.smalldetour.chpt03.except.t3

import scala.{Stream => _}

object StreamTests extends App {
  val s1 = Stream(1,2,3,4,5)

  println(s1)
  println(s1.takeWhile(_ < 4).toList)
  println(s1.headOption)
  println(s1.headOption2)
  println(Stream().headOption2)
  println(s1.map(_.toString + ":").toList)
}
