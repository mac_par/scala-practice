package com.functional.chpt02.smalldetour

object Funny extends App {
  def format(name: String, n: Int, fn: Int => Int): String = {
   s"$name of $n - result: ${fn(n)}"
  }

  def factorial(value: Int): Int = {
    def go(n: Int, m: Int): Int = {
      if (m < 1) {
        return n
      }

      go(n * m, m - 1)
    }

    go(1, value)
  }

  def fib(n: Int): Int = {
    def loop(f: Int, s: Int, iter: Int): Int = {
      if (iter <= 0) {
        return f
      }
      loop(s, f + s, iter - 1)
    }

    loop(0, 1, n)
  }


  println(format("factorial", 3, factorial))
  println(format("fibonacci", 0, fib))
  println(format("fibonacci", 1, fib))
  println(format("fibonacci", 7, fib))
}
