package com.functional.chpt02.smalldetour.chpt02.repeat

import com.functional.chpt02.smalldetour.chpt02.repeat.Par.Par

object InitialTry extends App {
  def sum(ints: Seq[Int]): Par[Int] = {
    if (ints.length <= 1) {
      Par.unit(ints.headOption getOrElse 0)
    } else {
      val (l, r) = ints.splitAt(ints.length / 2)
      val sumL = Par.unit(sum(l))
      val sumR = Par.unit(sum(r))
      Par.map2(Par.fork(sum(l)), Par.fork(sum(r)))(_ + _)
    }
  }
}
