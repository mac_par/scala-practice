package com.functional.chpt02.smalldetour

import scala.::
import scala.annotation.tailrec

object ListExercises2 extends App {
  def tail[A](list: List[A]): List[A] = list match {
    case Nil => Nil
    case head :: tail => tail
  }

  def setHead[A](list: List[A], newHead: A): List[A] = list match {
    case head :: tail => newHead :: tail
  }

  def drop[A](l: List[A], n: Int): List[A] = {
    def loop(l: List[A], n: Int): List[A] = l match {
      case Nil => Nil
      case _ :: tail if n > 0 => loop(tail, n - 1)
      case _ => l
    }

    loop(l, n)
  }

  def dropWhile[A](l: List[A])(pred: A => Boolean): List[A] = l match {
    case head :: tail if pred(head) => dropWhile(tail)(pred)
    case _ => l
  }

  def append[A](l1: List[A], l2: List[A]): List[A] = l1 match {
    case Nil => l2
    case head :: tail => head :: append(tail, l2)
  }

  def init[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case head :: Nil => Nil
    case head :: tail => head :: init(tail)
  }

  def sum(l: List[Int]): Int = l match {
    case Nil => 0
    case head :: tail => head + sum(tail)
  }

  def product(l: List[Double]): Double = l match {
    case Nil => 1.0
    case head :: tail => head * product(tail)
  }

  def foldLeft[T](base: T)(l: List[T])(fn: (T, T) => T): T = l match {
    case Nil => base
    case head :: tail => fn(head, foldLeft(base)(tail)(fn))
  }

  def foldLeft2[T, R](base: R)(l: List[T])(fn: (T, R) => R): R = l match {
    case Nil => base
    case head :: tail => fn(head, foldLeft2(base)(tail)(fn))
  }

  @tailrec
  def foldLeft3[T, R](l: List[T], base: R)(fn: (T, R) => R): R = l match {
    case Nil => base
    case head :: tail => foldLeft3(tail, fn(head, base))(fn)
  }

  def foldleft4[T, R](l: List[T], base: R)(fn: (T, R) => R): R = l match {
    case Nil => base
    case head :: tail => foldleft4(tail, fn(head, base))(fn)
  }

  println(dropWhile(List(1, 2, 3, 4, 5, 6, 7))(x => x < 6))
  println(foldLeft(0)(List(1, 2, 3, 4, 5, 6, 7))(_ + _))
  println(foldLeft(1.0)(List(1, 2, 3, 4, 5, 6, 7))(_ * _))
  println(foldLeft2(Nil: List[Int])(List(1, 2, 0, 4, 5, 6, 7))(_ :: _))
  println(foldLeft2(0)(List(1, 2, 0, 4, 5, 6, 7))((_, counter) => counter + 1))
  println(foldleft4(List(1, 2, 0, 4, 5, 6, 7), Nil: List[Int])(_ :: _))
}
