package com.functional.chpt02.smalldetour.chpt03.testing

case class SGen[+A](forSize: Int => Gen[A])
