package com.functional.chpt02.smalldetour.chpt03.except.t1

import scala.util.{Try => _}
import scala.{Either => _, Option => _}

object IMeanIt extends App {

  def lift[A, B](fn: A => B): Option[A] => Option[B] = _ map fn

  def lift2[A, B, C](opt1: Option[A], opt2: Option[B])(fn: (A, B) => C): Option[C] = (opt1, opt2) match {
    case (Some(v1), Some(v2)) => Some(fn(v1, v2))
    case _ => None
  }

  def mean(seq: Seq[Double]): Option[Double] = {
    if (seq.isEmpty) None
    Some(seq.sum / seq.size)
  }

  def variance(xs: Seq[Double]): Option[Double] = mean(xs) flatMap (meanValue => mean(
    xs.map(x => math.pow(x - meanValue, 2))))

  def Try[A](a: => A): Option[A] = try {
    Some(a)
  } catch {
    case e: Exception => None
  }

  def sequence[A](l: List[Option[A]]): Option[List[A]] = l match {
    case Nil => Some(Nil)
    case h :: t => h flatMap (head => sequence(t) map (head :: _))
  }

  def parseInts(a: List[Option[String]]): Option[List[Int]] = sequence(a map (i => i flatMap (s => Try(s.toInt))))

  def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = sequence(a map (f(_)))

  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = (a, b) match {
    case (Some(v1), Some(v2)) => Some(f(v1, v2))
    case _ => None
  }

  def map2for[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = for {
    v1 <- a
    v2 <- b
  } yield f(v1, v2)


  println(mean(Seq(3.0, 15, 2, 1.3)))
  println(mean(Seq.empty[Double]))
  val sequence = (1 to 15).map(_.toDouble)
  println(Some(sequence).flatMap(mean))
  println(parseInts(List(Some("12"), Some("2"), Some("df"), Some("3"))))
  println(traverse(List("12", "2", "4", "3"))(i => Try(i.toInt)))
}
