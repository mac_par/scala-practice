package com.functional.chpt02.smalldetour.chpt02.repeat.actor

import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.{Callable, CountDownLatch, ExecutorService}

object NonBlocking {

  //sealed was removed to test it
  trait Future[A] {
    private[actor] def apply(k: A => Unit): Unit
  }

  type Par[A] = ExecutorService => Future[A]

  def run[A](es: ExecutorService)(p: Par[A]): A = {
    val ref = new AtomicReference[A]
    val latch = new CountDownLatch(1)
    val actor = Actor[A](es) { a => ref.set(a); latch.countDown() }
    try {
      p(es) { value => actor ! value }
    } catch {
      case ex: Throwable => println(ex); latch.countDown()
    }
    latch.await()
    ref.get()
  }

  def unit[A](a: A): Par[A] = es => new Future[A] {
    override private[actor] def apply(k: A => Unit): Unit = k(a)
  }

  def fork[A](p: => Par[A]): Par[A] = es => new Future[A] {
    override private[actor] def apply(k: A => Unit): Unit = eval(es)(p(es)(k))
  }

  def dummy[A](p: () => Par[A]): Par[A] = es => new Future[A] {
    override private[actor] def apply(k: A => Unit): Unit = p()(es)(k)
  }

  private[actor] def eval(es: => ExecutorService)(r: => Unit): Unit = es.submit(new Callable[Unit] {
    override def call(): Unit = r
  })

  def map2[A, B, C](p1: Par[A], p2: Par[B])(fn: (A, B) => C): Par[C] = es => new Future[C] {
    override private[actor] def apply(k: C => Unit): Unit = {
      var ar: Option[A] = None
      var br: Option[B] = None

      val combiner = Actor[Either[A, B]](es) {
        case Left(a) => br match {
          case None => ar = Some(a)
          case Some(b) => eval(es)(k(fn(a, b)))
        }

        case Right(b) => ar match {
          case None => br = Some(b)
          case Some(a) => eval(es)(k(fn(a, b)))
        }
      }

      p1(es) {
        a => combiner ! Left(a)
      }

      p2(es) {
        b => combiner ! Right(b)
      }
    }
  }

  def lazyUnit[A](a: => A): Par[A] = fork(unit(a))

  def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  def sortPar[A <: Comparable[A]](parList: Par[List[A]]): Par[List[A]] = map(parList)(_.sorted)

  def map[A, B](par: Par[A])(fn: A => B): Par[B] = map2(par, unit(()))((a, _) => fn(a))

  def parMap[A, B](ps: List[A])(p: A => B): Par[List[B]] = sequence(ps.map(asyncF(p)))

  def sequence[A](ps: List[Par[A]]): Par[List[A]] = ps.foldRight[Par[List[A]]](unit(List()))((l, p) => map2(l, p)(_ :: _))

  def choice[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] = es => if (run[Boolean](es)(cond)) t(es) else f(es)

  def choiceN[A](n: Par[Int])(choices: List[Par[A]]): Par[A] = es => (value: A => Unit) => n(es) { idx => choices(idx)(es)(value) }

  def choiceMap[K, V](key: Par[K])(choices: Map[K, Par[V]]): Par[V] = es => (k: V => Unit) => key(es) { keyVal => choices(keyVal)(es)(k) }

  def chooser[A, B](pa: Par[A])(choices: A => Par[B]): Par[B] = flatMap(pa)(choices)

  def flatMap[A, B](p: Par[A])(f: A => Par[B]): Par[B] = es => (b: B => Unit) => p(es)(f(_)(es)(b))
}
