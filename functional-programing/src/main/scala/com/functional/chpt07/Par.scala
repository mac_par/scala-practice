package com.functional.chpt07


import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.{Callable, CountDownLatch, ExecutorService, Future => _}

//Nonblocking
object Par {
  sealed trait Future[A] {
    private[chpt07] def apply(f: A => Unit): Unit
  }

  type Par[A] = ExecutorService => Future[A]

  private case class UnitFuture[A](get: A) extends Future[A] {
    override private[chpt07] def apply(f: A => Unit): Unit = f(get)
  }

  def unit[A](a: => A): Par[A] = es => UnitFuture(a)

  def map2[A, B, C](p1: Par[A], p2: Par[B])(fn: (A, B) => C): Par[C] = es => {
    UnitFuture(fn(run(es)(p1), run(es)(p2)))
  }

  def map[A, B](par: Par[A])(fn: A => B): Par[B] = map2(par, unit(())) { (value, _) => fn(value) }

  def fork[A](p: Par[A]): Par[A] = es => new Future[A] {
    override private[chpt07] def apply(f: A => Unit): Unit = eval(es)(p(es)(f))
  }

  def lazyUnit[A](a: => A): Par[A] = fork(unit(a))

  def run[A](es: ExecutorService)(a: Par[A]): A = {
    val ref = new AtomicReference[A]()
    val latch = new CountDownLatch(1)
    a(es)(a => {
      ref.set(a);
      latch.countDown()
    })
    latch.await()
    ref.get()
  }

  def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  def sortPar(parList: Par[List[Int]]): Par[List[Int]] = map(parList) {
    _.sorted
  }

  def parMap[A, B](ps: List[A])(f: A => B): Par[List[B]] = fork(sequence(ps.map(asyncF(f))))

  def sequence[A](ps: List[Par[A]]): Par[List[A]] = ps.foldRight(unit(Nil: List[A])) {
    map2(_, _) {
      _ :: _
    }
  }

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = {
    map(sequence(as.map(asyncF(a => if (f(a)) List(a) else List.empty[A]))))(_.flatten)
  }

  def eval(es: ExecutorService)(r: => Unit): Unit = es.submit(new Callable[Unit] {
    def call = r
  })
}
