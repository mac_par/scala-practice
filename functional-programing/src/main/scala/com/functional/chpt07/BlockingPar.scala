package com.functional.chpt07

import java.util.concurrent.{Callable, ExecutorService, Future, TimeUnit}

object BlockingPar {
  object Par {
    type Par[A] = ExecutorService => Future[A]

    def unit[A](a: A): Par[A] = (es: ExecutorService) => UnitFuture(a)

    case class UnitFuture[A](get: A) extends Future[A] {
      def cancel(mayInterruptIfRunning: Boolean): Boolean = false

      def isCancelled: Boolean = false

      def isDone: Boolean = true

      def get(timeout: Long, unit: TimeUnit): A = get
    }

    def map[A, B](a: Par[A])(f: A => B): Par[B] = { (es: ExecutorService) =>
      val af = a(es)

      UnitFuture(f(af.get))
    }

    def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] = { (es: ExecutorService) =>
      val af = a(es)
      val bf = b(es)

      UnitFuture(f(af.get, bf.get))
    }

    def fork[A](a: Par[A]): Par[A] = es => es.submit(new Callable[A] {
      def call = a(es).get
    })

    def async[A](a: => A): Par[A] = fork(unit(a))

    def run[A](es: ExecutorService)(a: Par[A]): A = a(es).get

    def map2b[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] = { (es: ExecutorService) =>
      val af = a(es)
      val bf = b(es)

      new Future[C] {
        def cancel(mayInterruptIfRunning: Boolean): Boolean = true

        def isCancelled: Boolean = af.isCancelled || bf.isCancelled

        def isDone: Boolean = af.isDone && bf.isDone

        def get(): C = f(af.get, bf.get)

        def get(timeout: Long, unit: TimeUnit): C = {
          val started = System.currentTimeMillis

          val a = af.get(timeout, unit)
          val elapsed = System.currentTimeMillis - started
          val remaining = unit.toMillis(timeout) - elapsed
          val b = bf.get(remaining, unit)

          f(a, b)
        }
      }
    }

    def asyncF[A, B](f: A => B): A => Par[B] = a => async(f(a))

    def sequence[A](as: List[Par[A]]): Par[List[A]] = as match {
      case Nil => unit(Nil)
      case h :: t => map2(h, sequence(t))(_ :: _)
    }

    def parMap[A, B](as: List[A])(f: A => B): Par[List[B]] = fork {
      sequence(as.map(asyncF(f)))
    }

    def parFilter[A](l: List[A])(f: A => Boolean): Par[List[A]] = l match {
      case Nil => unit(Nil)
      case h :: t => map2(unit(f(h)), parFilter(t)(f)) { (matches, rest) =>
        if (matches) h :: rest else rest
      }
    }

    def reduce[A](as: IndexedSeq[A], zero: A)(f: (A, A) => A): Par[A] = {
      def iter(as: IndexedSeq[A]): Par[A] = {
        if (as.isEmpty) unit(zero)
        else {
          val left = fork(iter(as.take(as.length / 2)))
          val right = fork(iter(as.drop(as.length / 2)))

          map2(left, right)(f)
        }
      }

      iter(as)
    }

    def parallelMax[A](as: IndexedSeq[A])(max: (A, A) => A): Par[A] = reduce(as, as.head)(max)

    def map3[A, B, C, D](fa: Par[A], fb: Par[B], fc: Par[C])(f: (A, B, C) => D): Par[D] = {
      map2(map2(fa, fb)((a, b) => (c: C) => f(a, b, c)), fc)(_ (_))
    }

    def map4[A, B, C, D, E](fa: Par[A], fb: Par[B], fc: Par[C], fd: Par[D])(f: (A, B, C, D) => E): Par[E] = {
      map2(map2(map2(fa, fb)((a, b) => (c: C) => (d: D) => f(a, b, c, d)), fc)(_ (_)), fd)(_ (_))
    }

    def map5[A, B, C, D, E, F](fa: Par[A], fb: Par[B], fc: Par[C], fd: Par[D], fe: Par[E])(f: (A, B, C, D, E) => F): Par[F] = {
      map2(map2(map2(map2(fa, fb)((a, b) => (c: C) => (d: D) => (e: E) => f(a, b, c, d, e)), fc)(_ (_)), fd)(_ (_)), fe)(_ (_))
    }

    def deadlock[A](threadPoolSize: Int, a: A): Par[A] = {
      if (threadPoolSize <= 1) {
        async(a)
      } else {
        fork(deadlock(threadPoolSize - 1, a))
      }
    }


    def flatMap[A, B](a: Par[A])(f: A => Par[B]): Par[B] = join(map(a)(f))

    def join2[A](a: Par[Par[A]]): Par[A] = flatMap(a)(identity)

    // choice part
    def choice[A](pred: Par[Boolean])(p1: Par[A], p2: Par[A]): Par[A] = choiceN(flatMap(pred) {
      case true => unit(0)
      case _ => unit(1)
    })(List(p1, p2))

    def choiceN[A](n: Par[Int])(choices: List[Par[A]]): Par[A] = chooser(n)(choices.apply)

    def choiceMap[K, V](keyPar: Par[K])(choices: Map[K, Par[V]]): Par[V] = chooser(keyPar)(choices.apply)

    def chooser[A, B](keyPar: Par[A])(choicesFn: A => Par[B]): Par[B] = es => choicesFn(run(es)(keyPar))(es)

    def join[A](a: Par[Par[A]]): Par[A] = es => run(es)(a)(es)
  }
}