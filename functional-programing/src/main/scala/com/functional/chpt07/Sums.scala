package com.functional.chpt07

object Sums {

  import Par._

  def sum(seq: IndexedSeq[Int]): Par[Int] = {
    if (seq.size <= 1) {
      Par.unit(seq.headOption getOrElse 0)
    } else {
      val (l, r) = seq.splitAt(seq.size / 2)
      Par.map2(sum(l), sum(r)) {
        _ + _
      }
    }
  }

}
