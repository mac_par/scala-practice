package com.functional.chpt04.obj

import com.functional.chpt04.{None, Option, Some}

case class Employee(name: String, department: String)

case object Employee {
  private val employeesMap = collection.mutable.Map[String, Employee]()

  def lookupByName(name: String): Option[Employee] = if (employeesMap.contains(name)) Some(employeesMap.get(name).get) else None

  def addEmployee(employee: Employee): Unit = employeesMap += employee.name -> employee
}
