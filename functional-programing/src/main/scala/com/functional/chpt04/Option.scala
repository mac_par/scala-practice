package com.functional.chpt04

sealed trait Option[+A] {
  def map[B](fn: A => B): Option[B] = this match {
    case None => None
    case Some(value) => Some(fn(value))
  }

  def flatMap[B](fn: A => Option[B]): Option[B] = map(fn) getOrElse None

  def getOrElse[B >: A](default: => B): B = this match {
    case Some(value) => value
    case None => default
  }

  def orElse[B >: A](default: => Option[B]): Option[B] = map(Some(_)) getOrElse (default)

  def filter(pred: A => Boolean): Option[A] = flatMap(a => if (pred(a)) this else None)


}

case class Some[+A](value: A) extends Option[A]

case object None extends Option[Nothing]

case object Option {
  def map2[A, B, C](a: Option[A], b: Option[B])(fn: (A, B) => C): Option[C] = a flatMap (aa => b map (bb => fn(aa, bb)))

  /* def sequence[A](opts: List[Option[A]]): Option[List[A]] = opts match {
     case Nil => Some(Nil)
     case head::tail => head flatMap(hh => sequence(tail) map(hh :: _))
   }*/

  def lift[A, B](fn: A => B): Option[A] => Option[B] = _ map (fn)

  def sequence[A](opts: List[Option[A]]): Option[List[A]] = traverse(opts) { x => x }

  /*opts.foldRight[Option[List[A]]](Some(List())) { (opt, lOpt) =>
    map2(opt, lOpt) {
      _ :: _
    }
  }
*/
  def traverse[A, B](list: List[A])(fn: A => Option[B]): Option[List[B]] = list.foldRight[Option[List[B]]](Some(Nil)) { (a, list) =>
    map2(fn(a), list) {
      _ :: _
    }
  }
}