package com.functional.chpt04

sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B] = this match {
    case Left(value) => Left(value)
    case Right(value) => Right(f(value))
  }

  def flatMap[EE >: E, B](fn: A => Either[EE, B]): Either[EE, B] = this match {
    case Left(value) => Left(value)
    case Right(value) => fn(value)
  }

  def orElse[EE >: E, B >: A](fn: => Either[EE, B]): Either[EE, B] = this match {
    case Left(_) => fn
    case Right(_) => this
  }

  def map2[EE >: E, B >: A, C](b: Either[EE, B])(fn: (A, B) => C): Either[EE, C] = for {
    aa <- this
    bb <- b
  } yield fn(aa, bb)


}

object Either {
  def sequence[E, A](s: List[Either[E, A]]): Either[E, List[A]] = traverse(s) { i => i }

  def traverse[E, A, B](s: List[A])(fn: A => Either[E, B]): Either[E, List[B]] =
    s.foldRight[Either[E, List[B]]](Right(List.empty[B])) { (a, either) =>
      fn(a) flatMap { b =>
        either map {
          b :: _
        }
      }
    }
}

case class Left[+E](value: E) extends Either[E, Nothing]

case class Right[+A](value: A) extends Either[Nothing, A]