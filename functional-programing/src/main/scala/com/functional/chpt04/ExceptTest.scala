package com.functional.chpt04

import com.functional.chpt04.obj.Employee

object ExceptTest {
  def mean(l: Seq[Double]): Option[Double] = if (l.isEmpty) None else Some(l.sum / l.size)

  def mea2(l: Seq[Double]): Either[String, Double] = if (l.isEmpty) Left("empty list") else Right(l.sum / l.size)

  def Try2[A](a: => A): Either[Exception, A] = try {
    Right(a)
  } catch {
    case e: Exception => Left(e)
  }

  def safeDiv(x: Int, y: Int): Either[Exception, Int] = Try2(x / y)

  def variance(xs: Seq[Double]): Option[Double] = mean(xs) flatMap { mean =>
    ExceptTest.mean(xs.map(i => math.pow(i - mean, 2)))
  }

  def Try[A, B](fn: => A): Option[A] = {
    try {
      Some(fn)
    } catch {
      case _: Exception => None
    }
  }

  def insuranceRateQuote(age: Int, numberOfSpeedingTickets: Int): Double = {
    3
  }

  def parseInsuranceRateQuote(
                               age: String,
                               numberOfSpeedingTickets: String
                             ): Option[Double] = {
    val optAge = Try(age.toInt)
    val optSpeedingTickers = Try(numberOfSpeedingTickets.toInt)
    Option.map2(optAge, optSpeedingTickers) {
      insuranceRateQuote
    }
  }

  def main(args: Array[String]): Unit = {
    println(mean(Seq.empty[Double]))
    println(mean(Seq(1, 2, 3, 4, 5)))
    val name = "Marco"
    val employee = Employee(name, "Accountacy")
    println(Employee.lookupByName(name).map(_.department).getOrElse("Unknown dept."))
    Employee.addEmployee(employee)
    println(Employee.lookupByName(name).map(_.department).getOrElse("Unknown dept."))
    val abs0: Option[Double] => Option[Double] = Option.lift(math.abs)
    println(abs0(None))
    println(abs0(Some(-5.0)))
    println(parseInsuranceRateQuote("5", "15"))
    println(parseInsuranceRateQuote("5", "dupa"))
  }
}
