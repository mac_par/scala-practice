package com.data.p02

import breeze.linalg.{CSCMatrix, DenseMatrix}

object MatricesIntr extends App {
val matrix = DenseMatrix((1,2,3), (2,3,4), (3,4,5))
  println(matrix)

  val sars = CSCMatrix((1,2,3), (2,3,4), (3,4,5))
  println(sars)

  val matrixZero = DenseMatrix.zeros[Double](5,4)
  println(matrixZero)

  val compressedMatrixZero = CSCMatrix.zeros[Double](5,4)
  println(compressedMatrixZero)


  val genMet = DenseMatrix.tabulate[Double](5,4)((x,y)=> x*y)
  println(genMet)

  val identityMatrix = DenseMatrix.eye[Int](3)
  println(identityMatrix)

  val fromArray = new DenseMatrix(2,3, Array(1,1,2,3,5,8,13,21))
  println(fromArray)
}
