package com.data.p02

import breeze.linalg.DenseMatrix

object MatrixOps extends App {
  val m1 = DenseMatrix((1,2,3),(11,12,13),(21,22,23))
  val identity = DenseMatrix.eye[Int](3)
  //suma korespondujących obiektów
  println(s"Sum:\n${m1 + identity}" )

  println(s"Multiply:\n ${m1 * identity}")

  println("Getting column vectors")
  val result = m1(::, 0)

  println(result)

  val firstTwoRows = m1(0 to 1, ::)
  println(s"dwa rzędy:\n ${firstTwoRows}")
}
