package com.data.p02

import breeze.linalg.{DenseMatrix, convert}
import breeze.stats._

object MatrixCalc extends App {
  val matrix = DenseMatrix((1,0,2),(0,1,5),(0,0,1))
  val transition = matrix.t

  val meanVal = meanAndVariance(convert(matrix, Double))
  println(meanVal)

}
