package com.data.p01

import breeze.linalg.DenseVector

object ConcatVectors extends App {
  val vec1 = DenseVector.fill(5,1)
  val vec2 = DenseVector.range(0,5)

  println(DenseVector.vertcat(vec1, vec2))
  println(DenseVector.horzcat(vec1, vec2))

  //conversion
  println(breeze.linalg.convert(vec2, Double))
}
