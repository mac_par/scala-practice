package com.data.p01

import breeze.linalg._
import breeze.numerics.{log, sqrt}
import breeze.stats._

object Stats extends App {
  val vec = DenseVector.rangeD(0, 15)

  val meanVal = mean(vec)
  val meanVarianceVal = meanAndVariance(vec)

  println(s"mean $meanVal")
  println(s"mean and variance ${meanAndVariance}")

  val stdDev = stddev(vec)
  println(s"Standard deviation: $stdDev")

  println(s"max: ${max(vec)}")
  println(s"min: ${min(vec)}")
  println(s"sqrt: ${sqrt(vec)}")
  println(s"sum: ${sum(vec)}")
  println(s"sum: ${log(vec)}")
}
