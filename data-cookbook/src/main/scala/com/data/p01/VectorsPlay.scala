package com.data.p01

import breeze.linalg.{DenseVector, SparseVector, linspace}

object VectorsPlay extends App {
  val dense = DenseVector((1 to 5).toArray)
  println(dense)
  val sparse = SparseVector((1 to 5).toArray)
  println(sparse)

  //zero vectors
  val zero1 = DenseVector.zeros[Double](5)
  val zero2 = SparseVector.zeros[Double](5)
  println(zero1)
  println(zero2)

  //function-driven

  val tab1 = DenseVector.tabulate[Double](5)(x => x.toDouble % 2)

  println(tab1)

  val spaceVector = linspace(2, 9, 16)

  println(spaceVector)

  val rangeVector = DenseVector.range(5, 15)
  val rangeVector2 = DenseVector.rangeD(0.2, 7.4, 0.25)

  println(rangeVector)
  println(rangeVector2)

  val filledVector = DenseVector.fill[Int](5, 0)
  println(filledVector)

  //slicing
  val fullVector = DenseVector.rangeD(1, 15.0)
  val threeFirstElements = fullVector.slice(0, 3)
  val threeElementBySlide2 = fullVector.slice(0,5, 2)

  println(threeFirstElements)
  println(threeElementBySlide2)

  val fromScala = DenseVector(collection.immutable.Vector(5,6,3,4))

  println(fromScala)
}
