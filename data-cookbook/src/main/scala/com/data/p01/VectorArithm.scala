package com.data.p01

import breeze.linalg.DenseVector

object VectorArithm extends App {
  val vector = DenseVector.rangeD(0.0, 5.2,1.23)
  val vectorPlus033 = vector + 0.33

  println(vectorPlus033)

  val fix = DenseVector.range(0, 5)
  println(fix %2)
  println(fix *2)

  val twos = DenseVector.fill(10, 1)
  val nosTwos = DenseVector.range(0,20, 2)
  println(twos + nosTwos)
  println(twos.dot(nosTwos))

  //summation: both vectors of equal size
  val v1 = DenseVector.fill(5, 1)
  val v2 = DenseVector.range(20, 25)
  println(v1 + v2)
}
