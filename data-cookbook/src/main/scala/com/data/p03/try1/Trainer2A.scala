package com.data.p03.try1

import com.data.p03.{Animal, Cat, Dog, Lion, Snake, Tiger, Wolf}

case class Trainer2A[A <: Wolf](animal: A) {
  def accept (animal : A):Unit = println(animal)
}

object Trainer2A extends App {
  var trainer2 = Trainer2A[Wolf](new Wolf())

//  trainer2.accept(new Cat())
  trainer2.accept(new Dog())
//  trainer2.accept(new Animal())
//  trainer2.accept(new Lion())
  trainer2.accept(new Wolf())
//  trainer2.accept(new Snake())
//  trainer2.accept(new Tiger())

  var t3 = Trainer2[Animal](new Cat())
  //  t3.accept(new Animal())
  //  t3.accept(new Wolf())
  t3.accept(new Dog())
}
