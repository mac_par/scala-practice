package com.data.p03.try1

import com.data.p03.{Animal, Cat, Dog, Lion, Snake, Tiger, Wolf}

case class Trainer4[+A]() {
  def add[B >:A](element: B):Unit = println(element)
}

object Trainer4 extends App {
  var t: Trainer4[Wolf] = Trainer4[Dog]()
  t = Trainer4[Wolf]()
  t.add(new Dog())
  t.add(new Wolf())
//  t = Trainer4[Animal]()
  var t3: Trainer4[Animal] = Trainer4[Animal]()
  t3.add(new Snake())
  t3 = Trainer4[Wolf]()
//  val t4: Trainer4[Tiger] = Trainer4[Cat]()
  var t5: Trainer4[Cat] = Trainer4[Lion]()
  t5 = Trainer4[Tiger]()
  t5 = Trainer4[Cat]()
  var t6: Trainer4[Cat] = Trainer4[Tiger]()
  var t7 : Trainer4[Tiger] = Trainer4[Tiger]()
//  t7 = Trainer4[Lion]()
}
