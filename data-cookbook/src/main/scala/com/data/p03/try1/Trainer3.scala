package com.data.p03.try1

import com.data.p03._

case class Trainer3[-A]() {
}

object Trainer3 extends App {
  val trainer: Trainer3[Animal] = Trainer3[Animal]()

  var trainer2: Trainer3[Cat] = Trainer3[Animal]()
  trainer2 = Trainer3[Animal]()
  trainer2 = Trainer3[Cat]()
//  trainer2 = Trainer3[Tiger]()
//  trainer2 = Trainer3[Lion]()

  var t3: Trainer3[Lion] = Trainer3[Cat]()
  t3 = Trainer3[Lion]()
//  t3 = Trainer3[Tiger]()
  var t4 : Trainer3[Dog] = Trainer3[Wolf]()
  t4 = Trainer3[Animal]()
}
