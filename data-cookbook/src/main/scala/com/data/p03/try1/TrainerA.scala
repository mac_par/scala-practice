package com.data.p03.try1

import com.data.p03.{Animal, Cat, Dog, Lion, Snake, Tiger, Wolf}

case class TrainerA[A <: Cat](animal: A) {
  def accept(animal: A): Unit  = println(animal)
}

object TrainerA extends App {
  var trainer = TrainerA[Cat](new Cat())
  trainer.accept(new Lion())
  trainer.accept(new Cat())
//  trainer.accept(new Dog())
//  trainer.accept(new Animal())
  trainer.accept(new Lion())
//  trainer.accept(new Wolf())
//  trainer.accept(new Snake())
  trainer.accept(new Tiger())

  val trainer2 = TrainerA[Lion](new Tiger())
//  trainer2.accept(new Tiger())
//  trainer2.accept(new Cat())
}



