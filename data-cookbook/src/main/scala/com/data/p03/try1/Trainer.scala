package com.data.p03.try1

import com.data.p03.{Animal, Cat, Dog, Lion, Snake, Tiger, Wolf}

case class Trainer[A <: Animal]() {
  def accept(animal: A): Unit  = println(animal)
}

object Trainer extends App {
  var trainer = Trainer[Animal]()

  trainer.accept(new Cat())
  trainer.accept(new Dog())
  trainer.accept(new Animal())
  trainer.accept(new Lion())
  trainer.accept(new Wolf())
  trainer.accept(new Snake())
  trainer.accept(new Tiger())
}
